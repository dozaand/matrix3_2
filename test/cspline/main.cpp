#include <matrix3_2/inc/cspline_ext.h>
#include <matrix3_2/inc/tmatr.h>
#include <src/UnitTest++.h>
#include <iostream>
#include <rdtsc.h>

using namespace std;

double f(double x)
{
    return x*x;
}

double df(double x)
{
    return 2.*x;
}

double f3(double x)
{
    return 1+x+x*x+x*x*x;
}
double f3d1(double x)
{
    return 1 + 2*x + 3*x*x;
}

double f3d2(double x)
{
    return 2 + 6*x;
}
double f3d3(double x)
{
    return 6;
}

int ccc=0;


int test_time()
{
    using namespace std;
    const int n1=41,n2=201;
    const double x1=-1.,x2=1.,dx1=(x2-x1)/(n1-1),dx2=(x2-x1)/(n2-1);
    double ax1[n1],af1[n1];
    int i=0;
    for(double x=x1;x<=x2+0.00001;x+=dx1,i++)
    {
        ax1[i]=x;
        af1[i]=f(x);
    }
    spline3<double,spline_grid_v<double> > s0(ax1,af1,n1),s1(ax1,af1,n1),s2(ax1,af1,n1);
    unsigned long long t0,t1;
    double v;
    i=0;
    t0=rdtsc();
    for(double x=x1;x<=x2+0.00001;x+=dx2,i++)
    {
        v=s0(x);
        v=s1(x);
        v=s2(x);
        v=s2(x);
        v=s2(x);
    }
    t1=rdtsc();
    cout<< (t1-t0)/i<<v <<endl;
    return 0;

}

int test_time1()
{
    using namespace std;
    const int n1=41,n2=201;
    const double x1=-1.,x2=1.,dx1=(x2-x1)/(n1-1),dx2=(x2-x1)/(n2-1);
    double ax1[n1],af1[n1];
    int i=0;
    for(double x=x1;x<=x2+0.00001;x+=dx1,i++)
    {
        ax1[i]=x;
        af1[i]=f(x);
    }
    Tpoint_info<double> acc;
    spline3_multipoint<double,spline_grid_v<double> > s0(&acc,ax1,af1,n1),s1(&acc,ax1,af1,n1),s2(&acc,ax1,af1,n1);
    unsigned long long t0,t1;
    double v;
    i=0;
    t0=rdtsc();
    for(double x=x1;x<=x2+0.00001;x+=dx2,i++)
    {
        v=s0(x);
        v=s1(x);
        v=s2(x);
        v=s2(x);
        v=s2(x);
    }
    t1=rdtsc();
    cout<<(t1-t0)/i<<v<<endl;
    return 0;

}

template <class Tval>
struct Tcashed_interval
{
    bool inited;
    Tval x1,x2;
    int i;
    Tcashed_interval():inited(false){}
    /*inline*/ int index()const{return i;}
    /*inline*/ bool includes(Tval x)const{if(inited)return x>=x1 && x<=x2;else return false;}
    /*inline*/ void update(int i_,Tval x1_, Tval x2_)
	{
		x1=x1_;
		x2=x2_;
		i=i_;
		inited=true;
	}
};

int test_time2() 
{
    using namespace std;
    const int n1=41,n2=201;
    const double x1=-1.,x2=1.,dx1=(x2-x1)/(n1-1),dx2=(x2-x1)/(n2-1);
    double ax1[n1],af1[n1];
    int i=0;
    for(double x=x1;x<=x2+0.00001;x+=dx1,i++)
    {
        ax1[i]=x;
        af1[i]=f(x);
    }
    Tcashed_interval<double> acc;
    spline3<double,spline_grid_v_cashed_interval<double,Tcashed_interval<double> > > s0(ax1,af1,n1,acc),s1(ax1,af1,n1,acc),s2(ax1,af1,n1,acc);
    unsigned long long t0,t1;
    double v;
    i=0;
    t0=rdtsc();
    for(double x=x1;x<=x2+0.00001;x+=dx2,i++)
    {
        v=s0(x);
        v=s1(x);
        v=s2(x);
        v=s2(x);
        v=s2(x);
    }
    t1=rdtsc();
    cout<<(t1-t0)/i<<" "<<v<<endl;
    return 0;

}

int test_value()
{
    // using namespace std;
    // const int n1=41,n2=201;
    // const double x1=-1.,x2=1.,dx1=(x2-x1)/(n1-1),dx2=(x2-x1)/(n2-1);
    // double ax1[n1],af1[n1];
    // int i=0;
    // for(double x=x1;x<=x2+0.00001;x+=dx1,i++)
    // {
    //     ax1[i]=x;
    //     af1[i]=f(x);
    // }
    // spline3<double,spline_grid_h<double> > s(x1,x2,af1,n1);
    // for(double x=x1;x<=x2+0.00001;x+=dx2,i++)
    // {
    //     cout<<x<<'\t'<<s(x)<<'\t'<<f(x)<<'\t'<<s.d(x)<<'\t'<<df(x)<<endl;
    // }
    return 0;
}

class Tbc_fix
{
public:
    double x0=-0.1;
    double x1=1.5;
    int N=10;
    vector<double> f;
    spline3<double,spline_grid_h<double> > s;
    Tbc_fix():f(f2vector<double>(f3,x0,x1,N)),s(x0,x1,f.data(),N)
    { 
    }
    ~Tbc_fix() 
    { 
    }
};

TEST_FIXTURE(Tbc_fix, Test1node_points)
{
    // тестируем в узлах интерполяции
    const int N1=10;
    double x,dx=(x1-x0)/(N1-1);
	x = x0;
    for(int i=0;i<N1;++i,x+=dx)
    {
//        cout<<s(x)<<" "<<f3(x)<<" "<<s(x)-f3(x)<<endl;
        CHECK_CLOSE(s(x), f3(x),1e-12 );
    }
}
TEST_FIXTURE(Tbc_fix, inner_points)
{
    // тестируем межжу узлами
    const int N1=50;
    double x,dx=(x1-x0)/(N1-1);
    x = x0;
    for(int i=0;i<N1;++i,x+=dx)
    {
//        cout<<s(x)<<" "<<f3(x)<<" "<<s(x)-f3(x)<<endl;
        CHECK_CLOSE(s(x), f3(x),0.02 );
    }
}

TEST_FIXTURE(Tbc_fix, inner_d1)
{
    // тестируем производную межжу узлами
    const int N1=50;
    double x,dx=(x1-x0)/(N1-1);
    x = x0;
    for(int i=0;i<N1;++i,x+=dx)
    {
        cout<<s.d(x)<<" "<<f3d1(x)<<" "<<s.d(x)-f3d1(x)<<endl;
        CHECK_CLOSE(s.d(x), f3d1(x),0.6 );
    }
}

TEST_FIXTURE(Tbc_fix, inner_d2)
{
    // тестируем вторую производную межжу узлами
    const int N1=50;
    double x,dx=(x1-x0)/(N1-1);
    x = x0;
    for(int i=0;i<N1;++i,x+=dx)
    {
//        cout<<s.d2(x)<<" "<<f3d2(x)<<" "<<s.d2(x)-f3d2(x)<<endl;
        CHECK_CLOSE(s.d2(x), f3d2(x),5 );
    }
}

TEST_FIXTURE(Tbc_fix, inner_d3)
{
    // тестируем третью производную межжу узлами
    const int N1=50;
    double x,dx=(x1-x0)/(N1-1);
    x = x0;
    for(int i=0;i<N1;++i,x+=dx)
    {
//        cout<<s.d3(x)<<" "<<f3d3(x)<<" "<<s.d3(x)-f3d3(x)<<endl;
        CHECK_CLOSE(s.d3(x), f3d3(x),7 );
    }
}

TEST_FIXTURE(Tbc_fix, spl3_integral)
{
    // тестируем интеграл
    double ref= 25./12,itg;
    itg=integral(s,0,1);
//    cout<<itg<<" "<<ref<<endl;
    CHECK_CLOSE(itg,ref ,0.1 );
}

TEST_FIXTURE(Tbc_fix, min_prop)
{
    // тестируем интеграл
    double ref= 25./12,itg;
    itg=integral(s,0,1);
//    cout<<itg<<" "<<ref<<endl;
    CHECK_CLOSE(itg,ref ,0.1 );
}



int main1()
{
    //test_value();
    ccc=0;
    test_time();
    cout<<"ccc="<<ccc<<endl;
    ccc=0;
    test_time1();
    cout<<"ccc="<<ccc<<endl;
    ccc=0;
    test_time2();
    cout<<"ccc="<<ccc<<endl;
    return 0;
}

int main()
{
    int i=0;
    i+=1;
    return UnitTest::RunAllTests();
}