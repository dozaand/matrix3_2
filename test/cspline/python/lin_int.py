import pycspline as SPL
import numpy as np
import matplotlib.pyplot as plt

def f(x):
    return 1 - (x - 1)**2

def intf(x):
    return x**2*(1.-x/3.)

def intf2(x):
    return (x**3)*(4./3-x+0.2*x**2)

xdata = np.linspace(0,4.3,10)
ydata = f(xdata)

spl = SPL.cspline1_v(xdata,ydata,len(xdata))

tt = np.linspace(0,4.3,500)
yy = f(tt)
ll = np.array([spl(t) for t in tt])

iyy = intf(tt)
ill = np.array([spl.integral(0,t) for t in tt])

iyy2 = intf2(tt)
ill2 = np.array([spl.integral_pow2(0,t) for t in tt])

plt.plot(tt,yy);
plt.plot(tt,ll);
plt.show()

plt.plot(tt,iyy);
plt.plot(tt,ill);
plt.show()

plt.plot(tt,yy**2);
plt.plot(tt,ll**2);
plt.show()

plt.plot(tt,iyy2);
plt.plot(tt,ill2);
plt.show()
