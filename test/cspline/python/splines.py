import pycspline

class MinimalEnergySpline(object):
    def __init__(self,yvector,xvector=None,interval=None):
        self.spline=None
        if interval is None:
            if (not (xvector is None)) and len(xvector)==len(yvector):
                self.spline=pycspline.cspline3_v(xvector,yvector,len(yvector))
            else:
                raise BufferError()
        else:
            self.spline=pycspline.cspline3_h(interval[0],interval[1],yvector,len(yvector))
    def __call__(self,x):
        return self.spline(x)
    def d(self,x):
        return self.spline.d(x)
    def d2(self,x):
        return self.spline.d2(x)
    def d3(self,x):
        return self.spline.d3(x)
    def derivatives(self,x):
        return [self(x),self.d(x),self.d2(x),self.d3(x)]

if __name__ == '__main__':
    import numpy as np
    def fun(x):
        return x*x-23.
    
    x0=0.
    x1=1.25623
    Np=51
    
    xv=np.linspace(x0,x1,Np)
    yv=fun(xv)
    
    spl_v=MinimalEnergySpline(xvector=xv,yvector=yv)
    spl_h=MinimalEnergySpline(interval=[x0,x1],yvector=yv)
    
    xt=np.linspace(x0,x1,7*Np)
    
    err={'v':0,'h':0}
    
    for x in xt:
        err['v']+=(spl_v(x)-fun(x))**2
        err['h']+=(spl_h(x)-fun(x))**2
        
    err['v']/=len(xt)
    err['h']/=len(xt)
    
    print "err['v']=",err['v']
    print "err['h']=",err['h']