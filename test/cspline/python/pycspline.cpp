#include <boost/python.hpp>
#include <boost/python/return_internal_reference.hpp>
#include <boost/python/list.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/stl_iterator.hpp>
#include <vector>

#include <matrix3_2/inc/cspline.h>

using namespace boost::python;

template< typename T >
inline
std::vector< T > to_std_vector( const object& iterable )
{
    return std::vector< T >( stl_input_iterator< T >( iterable ),
                             stl_input_iterator< T >( ) );
}

template<class val_t>
class Tcspline3_h_wrap
{
    spline3<val_t,spline_grid_h<val_t> > spl;
public:
    Tcspline3_h_wrap(val_t x0, val_t x1, const object& y, int N):spl(x0,x1,&(to_std_vector<val_t>(y))[0],N){}
    inline val_t operator()(val_t x)
    {
        return spl(x);
    }
    inline val_t d(val_t x)
    {
        return spl.d(x);
    }
    inline val_t d2(val_t x)
    {
        return spl.d2(x);
    }
    inline val_t d3(val_t x)
    {
        return spl.d3(x);
    }
};

template <class val_t>
class Tcspline3_v_wrap
{
    spline3<val_t,spline_grid_v<val_t> > spl;
public:
    Tcspline3_v_wrap(const object& x, const object& y, int N):spl(&(to_std_vector<val_t>(x))[0],&(to_std_vector<val_t>(y))[0],N){}
    inline val_t operator()(val_t x)
    {
        return spl(x);
    }
    inline val_t d(val_t x)
    {
        return spl.d(x);
    }
    inline val_t d2(val_t x)
    {
        return spl.d2(x);
    }
    inline val_t d3(val_t x)
    {
        return spl.d3(x);
    }
};

template <class val_t>
class Tcspline1_v_wrap
{
    std::vector<val_t> xd,yd;
    lin_int_ref<val_t,spline_grid_v_ref<val_t> > spl;
public:
    Tcspline1_v_wrap(const object& x, const object& y, int N):  
        xd(to_std_vector<val_t>(x)),
        yd(to_std_vector<val_t>(y)),
        spl(&xd[0],&yd[0],N){}
    inline val_t operator()(val_t x)
    {
        return spl(x);
    }
    inline val_t d(val_t x)
    {
        return 0;//spl.d(x);
    }
    inline val_t d2(val_t x)
    {
        return 0;
    }
    inline val_t d3(val_t x)
    {
        return 0;
    }
    inline val_t integral(val_t x1, val_t x2)
    {
        return ::integral(spl,x1,x2);
    }
    inline val_t integral_pow2(val_t x1, val_t x2)
    {
        return ::integral_pow2(spl,x1,x2);
    }
};

BOOST_PYTHON_MODULE(pycspline)
{

    class_<Tcspline3_h_wrap<double> >("cspline3_h",init<double,double,object&,int>())
        .def("__call__",&Tcspline3_h_wrap<double>::operator())
        .def("d",&Tcspline3_h_wrap<double>::d)
        .def("d2",&Tcspline3_h_wrap<double>::d2)
        .def("d3",&Tcspline3_h_wrap<double>::d3)
        ;

    class_<Tcspline3_v_wrap<double> >("cspline3_v",init<object&,object&,int>())
        .def("__call__",&Tcspline3_v_wrap<double>::operator())
        .def("d",&Tcspline3_v_wrap<double>::d)
        .def("d2",&Tcspline3_v_wrap<double>::d2)
        .def("d3",&Tcspline3_v_wrap<double>::d3)
        ;

    class_<Tcspline1_v_wrap<double> >("cspline1_v",init<object&,object&,int>())
        .def("__call__",&Tcspline1_v_wrap<double>::operator())
        .def("d",&Tcspline1_v_wrap<double>::d)
        .def("d2",&Tcspline1_v_wrap<double>::d2)
        .def("d3",&Tcspline1_v_wrap<double>::d3)
        .def("integral",&Tcspline1_v_wrap<double>::integral)
        .def("integral_pow2",&Tcspline1_v_wrap<double>::integral_pow2)
        ;

}