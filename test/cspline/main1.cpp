#include <iostream>
#include <iomanip>
//#include <vector>
#include "../../inc/cspline.h"
#include <math.h>
#include <stdlib.h>

int main()
{
    const_interpolation<double>::constructor fc;
    fc.add(0,1,0);
    fc.add(0.5,0.6,0.2);
    fc.add(0.8,1.1,0.4);
    fc.add(1.2,1.5,0.8);
    auto& fi=*(fc());
    for(double x=0;x<=1.5;x+=0.01)
    {
        std::cout<<x<<'\t'<<fi(x)<<std::endl;
    }
    delete &fi;
}
