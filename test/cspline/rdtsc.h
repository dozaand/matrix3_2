#ifndef rdtsc_h
#define rdtsc_h
#include <intrin.h>

#ifdef __MSVC__
#pragma intrinsic(__rdtsc)
inline unsigned __int64 rdtsc() {
    return __rdtsc();
}
#else
inline unsigned long long rdtsc() {
    return 0;
}
#endif
#endif