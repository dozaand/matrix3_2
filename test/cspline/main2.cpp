#include <iostream>
#include "../../inc/cspline.h"

using namespace std;

double f(double x)
{
    return (x - 1.)*x*x;
}

int main()
{
    double a = 5.546434;
    double b = 71.54687;
    const int N = 1000;
    double t[N+1],F[N+1],tt;
    double dt = (b-a)/N;
    int i;
    for(tt=a,i=0;i<N+1;tt+=dt,i++)
    {
        t[i] = tt;
        F[i] = f(tt);
        cout<<tt<<'\t'<<F[i]<<endl;
    }
    lin_int_ref<double,spline_grid_v_ref<double> > spl(t,F,N+1);
    cout<<integral_xF(spl,10.,20.);
    return 0;
}
