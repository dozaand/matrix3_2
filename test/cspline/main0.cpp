#include <fstream>
#include <iomanip>
//#include <vector>
#include <include/cspline.h>
#include <math.h>
#include <stdlib.h>

int main()
{
 int i;
 const int n=10;
 double f[n],x[n];
 for(i=0;i<n;i++){x[i]=double(i);f[i]=pow(i,3)+1;}
 double te=9;
 spline3<double,spline_grid_h<double> > sp(0.,te,f,n);
// spline3<double,spline_grid_v<double> > sp(x,f,n);
// lin_int<double,spline_grid_v<double> > sp(x,f,n);
 double d,t;
 std::ofstream s("a.dat");
 for(t=0;t<=te;t+=te/21)
 {
//  s<<t<<' '<<(int_bsearch(n,x,t)-x)<<' '<<t<<std::endl;
  s<<t<<' '<<sp(t)<<' '<<pow(t,3)+1<<std::endl;
 }
 s.close();

 s<<integral(sp,te/2,te);
// sp.integral()
// system("fminiplot -e \"1;2 color 1;\" -f a.dat");
// d=sp.d(te);
//std::cout<<d<<std::endl;
// d=integral(sp,2.8,5.6);
//std::cout<<d<<std::endl;
}
