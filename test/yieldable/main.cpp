#include <iostream>
#include <matrix3_2/inc/yieldable.h>

using namespace std;

class Ta:public Tyieldable
{
public:
 int beg[3],End[3];
 int y,x;
 virtual void body()
 {
  for(y=0;y<3;++y)
  {
   for(x=beg[y];x<End[y];++x)
   {
    yield();
   }
  }
 }
};

int main()
{
 Ta a;
 a.beg[0]=0;a.End[0]=2;
 a.beg[1]=2;a.End[1]=15;
 a.beg[2]=1;a.End[2]=8;
 for(a.begin();!a.end();++a)
 {
  cout<<a.y<<" "<<a.x<<endl;
 }
 return 0;
}
