#include <matrix/matr_com/matr_com.h>
#include <fstream>
#include <iomanip>
#include <util/timer/pentime.h>
#define TReal double
void maintstexp();

void test(matrixns<TReal>& m1,matrixns<TReal>& m2)
{
 const int siz=5;
 TReal r[]={1,1,1,1,1},x[siz],y[siz];
 m1.mul(r,x);
 m2.mul(x,y);
std::cout<<"test\n";
 for(int i=0;i<siz;i++)cout<<std::setw(15)<<r[i]<<std::setw(15)<<x[i]<<std::setw(15)<<y[i]<<std::setw(15)<<(r[i]-y[i])<<std::endl;
}


void testns()
{
 TReal d[15]={1,2,3,4,5,1,2,3,4,5,1,2,3,4,5},b[]={1,1,1,1,1};
// constructors tests
 matr_comns<TReal> m(3,5);
 pt_matr(m);
 matr_comns<TReal> m1(3,5,d);
 pt_matr(m1);
 matr_comns<TReal> m2(m1);
 pt_matr(m2);
 jackd<TReal> jk(15);
 make_jack(&jk,&m2,b);
 pt_matr(jk);
 matr_comns<TReal> m3(jk);
 pt_matr(m3);
 jackd_it<TReal> jk_it(&jk);
 matr_comns<TReal> m4(jk_it);
 pt_matr(m4);
// operations tests
 m3=3.3;
 pt_matr(m3);
 m4=m3;
 pt_matr(m4);
 m4.set_E();
 pt_matr(m4);
 m4+=m3;
/* m3.set_rand();
 m4.set_rand();
 m4.transp();
 matr_comns<TReal> m5(3,3);
 pentimer t;
 int i,j;
 for(j=0;j<10;j++)
 {
  t.reset();
  t.start();
  for(i=0;i<100;i++)m5.prod(m4,m3);
  t.stop();
 std::cout<<t.tim<<std::endl;
 }
 pt_matr(m3);
 pt_matr(m4);
 pt_matr(m5);*/
}

void time_muls()
{
 matr_comns<TReal> a(15,15),b(15,15),c(15,15);
 TReal buf[20];
 a.set_rand(0.,1.);
 a.reset_to_size(4,5);
 b.reset_to_size(6,5);
 b.set_rand(0.,1.);
 matr_comns<TReal> a1(a),b1(b);
//std::cout<<a1<<std::endl<<b1<<std::endl;
 b1.transp();
 c.prod(a1,b1);
//std::cout<<c<<std::endl;
 a.ABT(b,buf);
//std::cout<<a<<std::endl;
 a.reset_to_size(6,5);
 a.set_rand(0.,1.);
 b.reset_to_size(3,5);
 a.ABT(b,buf);
 a.reset_to_size(4,6);
 c-=a;
std::cout<<c.norm()<<std::endl;
// c.set_rand();
 b.reset_to_size(6,5);
 b.set_rand(0.,1.);
 pentimer t;
 for(int i=0;i<10;i++)
 {
//  a.reset_to_size(4,5);
  a.reset_to_size(4,6);
  t.reset();
  t.start();
  a.AB(b,buf);
//  c.prod(a,b);a=c;
  t.stop();
 std::cout<<t.tim<<std::endl;
 }
}

int main()
{
 try
 {
 int i;
/* TReal r[]={
 2,1,1.1,
 1,2,1,
 1,1,2,
 };*/
 TReal r[]={0.582667, 0.116058, 0.227105, 0.664532, 0.869526, 0.754811, 0.0540217,
0.541722, 0.235038, 0.0172668, 0.177537, 0.596514, 0.168184, 0.705241,
0.192832, 0.075101, 0.608445, 0.848264, 0.367002, 0.941444, 0.605328,
0.310773, 0.137471, 0.443716, 0.022661};
// time_muls();
 testns();
 matr_com<TReal> a(5,r);
 pt_matr(a);
// pt_matr_t(a);
 matr_com_lux<TReal> b(a);
// invert(b,a);
 pt_matr(a);
 test(a,b);
 matr_com_luo<TReal> c(a);
 test(a,c);
 matr_com_svd<TReal> d(a);
 test(a,d);
 TReal *rep,*imp;
 spectr<TReal>(a,&rep,&imp);
 for(i=0;i<3;i++){cout<<"("<<rep[i]<<", "<<imp[i]<<")\n";}
 delete [] rep;
 delete [] imp;
 matr_comns_it<TReal> it(&a);
 for(it=0;it();it++)
 {
 std::cout<<std::setw(3)<<it.geti()<<std::setw(3)<<it.getj()<<std::setw(15)<<it.getd()<<std::endl;
 }
 maintstexp();
 }
 catch(...)
 {
 }
 // test svrt;
 float dmtr[]={0.871314, 0.623982, 0.0175145, 0.704597, 0.043878, 0.326017, 0.7906,
0.297205, 0.183908, 0.176439, 0.489711, 0.821059, 0.2859, 0.890637, 0.0745535};
 float xx[]={1,2,3},yy[]={1,2,3,4,5};
 matr_comns<float> mtr(3,5,dmtr);
 cout<<mtr.svrt(xx,yy)<<" 34.8175"<<endl;
 return 0;
}
