import os
os.environ["path"]=os.environ["path"]+";"+os.environ["home"]+r"\boost\lib"
from matr_umf_py import *
import numpy as np
import ctypes

def test_fill():
    u"""test matrix fill"""
    A=matr_umf(5)
    data=np.array([[2, 3, 0, 0, 0],
     [3, 0, 4, 0, 6],
     [0, -1, -3, 2, 0],
     [0, 0, 1, 0, 0],
     [0, 4, 2, 0, 1]])

    i=0
    for row in data:
        j=0
        for x in row:
            if x:
                if(x!=0):
                    A.set_el(i,j,x)
            j+=1
        i+=1
    #A.to_crf()
    data=[[0,2,5,9,10,12],[0,1,0,2,4,1,2,3,4,2,1,4],[2,3,3,-1,4,4,-3,1,2,2,6,1]]
    #res=eval(str(A))
##    assert(res==data)
    b = np.array([8., 45., -3., 3., 19.])
    x = np.zeros(5)
    slv=matr_umf_lu(5)
    slu=matr_superlu_mt(5)
    slu.set_nproc(4)
    slv.update(A)
    slu.update(A)
    slu.mul(b.ctypes.data,x.ctypes.data)
    slu.update(A)
    slv.mul(b.ctypes.data,x.ctypes.data)
    print(x)
    slu.mul(b.ctypes.data,x.ctypes.data)
    print(x)

def aa(self):
    print "hello"

test_fill()
