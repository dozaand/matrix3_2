#include <boost/python.hpp>
#include <matrix3_2/inc/matr_umf.h>
#include <matrix3_2/inc/matr_superlu.h>
#include <boost/python/return_internal_reference.hpp>
//#include "../../inc/matr_umf.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(matr_umf_py)
{

    class_<matr_umf<double>,boost::noncopyable>("matr_umf",init<int>())
         .def("__call__",&matr_umf<double>::get_el)
         .def("set_el",&matr_umf<double>::set_el)
         .def("to_crf",&matr_umf<double>::to_crf)
         .def("__repr__",&matr_umf<double>::repr)
        ;

    class_<matr_umf_lu<double>, boost::noncopyable>("matr_umf_lu",init<int>())
        .def("update",&matr_umf_lu<double>::update)
        .def("mul",&matr_umf_lu<double>::mul_py)
        ;

        class_<matr_superlu<double>, boost::noncopyable>("matr_superlu_mt",init<int>())
        .def("get_nproc",&matr_superlu<double>::get_nproc)
        .def("set_nproc",&matr_superlu<double>::set_nproc)
        .def("update",&matr_superlu<double>::update)
        .def("mul",&matr_superlu<double>::mul_py)
        ;

}