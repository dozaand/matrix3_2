#include "../../inc/s_tab.h"
#include <UnitTest++/src/UnitTest++.h>
#include <iostream>
#include <iomanip>

using namespace std;
using namespace s_tab;

// для класса в котором не все поля сохраняем

class Tsamp_pod
{
public:
 int i;
 double d;
};

class Tsamp
{
public:
    int i;
    double dd;
    double d;

    template <class T>
    void htio(T& arh)
    {
        arh & i & d;
    }
};
HIIO(Tsamp)

class Tsamp_ver
{
public:
 int i;
 double dd;
 double d;
};

namespace s_tab
{
tistream& operator&(tistream& arh,Tsamp_ver& el)
{
    switch(arh.type["Tsamp_ver"])
    {
        case 0:
        {
            arh & el.i & el.d;
        }
    }
    return arh;
}
tostream& operator&(tostream& arh,const Tsamp_ver& el)
{
    arh.type["Tsamp_ver"]=0;
    arh & el.i & el.d;
    return arh;
}
}

class Tsamp_ver1
{
public:
 int i;
 double dd;
 double d;
 void load(tistream& arh,int ver)
 {
    arh & i & d;
 }
 void save(tostream& arh) const
 {  
    arh & i & d;
 }
};
HIIO_V(Tsamp_ver1,0)


SUITE(bm)
{
// test load data
TEST(TEST_SEEK)
{
    tostream s("test.dat");
    {
       tab m(s,"a");
       put(s,"-a-");
    }
    {
       put(s,"-ggg-");
       tab m(s,"b");
       put(s,"-b-");
        {
           tab m1(m,"bb");
           put(s,"-bb-");
           {
               tab m2(m1,"bbb");
               put(s,"-bbb-");
           }
        }
    }
    {
       tab m(s,"c");
       put(s,"-c-");
    }
}
// test convertion errors
TEST(TEST_GROOT_BEG)
{
    tistream s("test.dat");
    char line[4];
    s.cd("a");
    get(s,line);
    CHECK(string(line)=="-a-");
}
TEST(TEST_GROOT_END)
{
    tistream s("test.dat");
    char line[4];
    s.cd("c");
    get(s,line);
    CHECK(string(line)=="-c-");
}

TEST(TEST_GROOT_SUB)
{
    tistream s("test.dat");
    char line[6];

    s.cd("b/bb/bbb");
    get(s,line);
    CHECK(string(line)=="-bbb-");
}
TEST(TEST_GO_ROOT)
{
    tistream s("test.dat");
    char line[6];
    s.cd("b/bb/bbb");
    s.cd(".");
    s.cd("b/bb");
    get(s,line);
    CHECK(string(line)=="-bb-");
}
TEST(TEST_CDROOT)
{
    tistream s("test.dat");
    char line[6];
    s.cd("b/bb/bbb");
    s.cd("/b/bb");
    get(s,line);
    CHECK(string(line)=="-bb-");
}
TEST(TEST_GO_SEQ)
{
    tistream s("test.dat");
    char line[6];
    s.cd("b/bb");
    s.cd("bbb");
    get(s,line);
    CHECK(string(line)=="-bbb-");
}
TEST(TEST_GO_UP)
{
    tistream s("test.dat");
    char line[6];
    s.cd("b/bb/bbb");
    s.cd("..");
    get(s,line);
    CHECK(string(line)=="-bb-");
}

TEST(TEST_POD_WRITE)
{
  int a=22;
  float b[5]={1,2,3,4,5};
  {
    tostream s("test1.dat");
    {
       tab m(s,"a");
       s & a;
    }
    {
       tab m(s,"b");
       s & b;
    }
  }
  b[4]=0.2;
  a=0;
  {
    tistream s("test1.dat");
    s & a & b;
  }
  CHECK(a==22);
  CHECK(b[4]==5);
}
TEST(CLS_IO)
{
  Tsamp_pod samp_pod;
  Tsamp samp;
  Tsamp_ver samp_ver;
  Tsamp_ver1 samp_ver1;
  samp_ver.i=0;
  samp_ver.d=3.3;
  samp.i=0;
  samp.d=3.3;
  {
    tostream s("test1.dat");
    {
     tab m(s,"pod");
     s & samp_pod;
    }
    {
     tab m(s,"partial");
     s & samp;
    }
    {
     tab m(s,"ver");
     s & samp_ver;
    }
    {
     tab m(s,"ver1");
     s & samp_ver1;
    }
  }
  samp_ver.i=1;
  samp_ver.d=0;
  samp.i=1;
  samp.d=0;
  {
    tistream s("test1.dat");
    s.cd("ver");
    s & samp_ver;
  }
  CHECK(samp_ver.i==0);
  CHECK(samp_ver.d==3.3);
}
TEST(STRING_IO)
{
  std::string str="qwe";
  {
    tostream s("test1.dat");
    s & str;
  }
  str="qq";
  {
    tistream s("test1.dat");
    s & str;
  }
  CHECK(str=="qwe");
}
TEST(VECTOR_IO)
{
  std::vector<int> obj;
  obj.push_back(2);
  obj.push_back(3);
  {
    tostream s("test1.dat");
    s & obj;
  }
  obj.push_back(22);
  obj.push_back(33);
  {
    tistream s("test1.dat");
    s & obj;
  }
  CHECK(obj.size()==2);
  CHECK(obj[0]==2);
  CHECK(obj[1]==3);
}
TEST(VECTOR_IO_TO_EMPTY)
{
  std::vector<int> obj;
  std::vector<int> obj1;
  obj.push_back(2);
  obj.push_back(3);
  {
    tostream s("test1.dat");
    s & obj;
  }
  {
    tistream s("test1.dat");
    s & obj1;
  }
  CHECK(obj1.size()==2);
  CHECK(obj1[0]==2);
  CHECK(obj1[1]==3);
}
TEST(MAP_IO)
{
  std::map<int,int> obj;
  obj[2]=2;
  obj[3]=3;
  {
    tostream s("test1.dat");
    s & obj;
  }
  obj.clear();
  {
    tistream s("test1.dat");
    s & obj;
  }
  CHECK(obj.size()==2);
  CHECK(obj[2]==2);
  CHECK(obj[3]==3);
}
TEST(MAP_VECTOR_IO)
{
  std::vector<int> v;
  v.push_back(2);
  v.push_back(3);
  std::map<int,vector<int> > obj;
  obj[2]=v;
  {
    tostream s("test1.dat");
    s & obj;
  }
  obj.clear();
  {
    tistream s("test1.dat");
    s & obj;
  }
  CHECK(obj.size()==1);
  CHECK(obj[2][0]==2);
  CHECK(obj[2][1]==3);
}
TEST(TEST_STR)
{
  std::string ss("novo2");
  int kamp=1;
  {
    tostream s("test1.dat");
    s & ss;
    s & kamp;
  }
  {
    tistream s("test1.dat");
    cout<<s.tellg()<<endl;
    s & ss;
    cout<<s.tellg()<<endl;
    s & kamp;
    cout<<s.tellg()<<endl;
  }
  CHECK(ss=="novo2");
  CHECK(kamp==1);
}
}


int main(int argc,char** argv)
{
    return UnitTest::RunAllTests();
}

