#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import yaml
import codecs

fil="test.dat"
path="b/bb"

def get_sub_dsc(list_path):
    pass

def extract(fil,path,dest):
    u"""извлечение данных как самостоятельного файла"""
    with codecs.open(fil+".yaml","r",encoding="utf-8") as f:
        dsc=yaml.load(f)
    datadsc=dsc["tabs"]

def copy(fil_src,path_src=".",fil_dest=None,path_dest="."):
    """перезаписываем данные в fil_dest данными из файла fil_src если целевой файл не существует создаем его
    """
    with codecs.open(fil+".yaml","r",encoding="utf-8") as f:
        dsc=yaml.load(f)
    datadsc=dsc["tabs"]

def rm(fil_src,path_src):
    """перезаписываем данные в fil_dest данными из файла fil_src если целевой файл не существует создаем его"""
    pass



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="import unk macrosection library to h5 format file")
    parser.add_argument("--out", default="unk.h5", help=u"output file")
    parser.add_argument(
        "-f", "--force", action='store_true', help=u"output file")
    parser.add_argument("infile", help=u"input file")
    args = parser.parse_args()
    ImportUnkLib(args.infile, args.out, args.force)
