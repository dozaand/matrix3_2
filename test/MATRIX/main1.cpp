#include <matrix/matrix/matrix.h>
#define dim 8
void fun(TReal * a,TReal * b)
{
 int i;
 b[0]=a[0]*a[1];
 for(i=1;i<dim;i++)b[i]=b[i-1]+a[i];
}

void main()
{
 int i;
 TReal b[dim];
 for(i=0;i<dim;i++){b[i]=10.;}
 jackd<TReal> jk(dim*dim);
 make_jack(&jk,fun,b,dim);
 plot_matr(jk);
 pt_matr_t(jk);
}
