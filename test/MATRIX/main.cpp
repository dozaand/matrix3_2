#include <matrix/matrix/matrix.h>
#include <matrix/JACK_R/jack_r.h>
#include <iomanip>
#include <matrix/matr3d/matr3d.h>
#define TReal double
void prod()
{
 matr3d<TReal> m1(5,1,1,1),m2(5,-1,2,-1);
 TReal x[5]={1,1,1,1,1},y[5];
 matr_pro2<TReal> mp(&m1,&m2);
 mp.mul(x,y);
std::cout<<mp.dim()<<" "<<mp.dimo()<<std::endl;
 pt_matr_t(m2);
 pt_matr_t(mp);
}

int main()
{
 const int d=8;
 int i,j;
 jack<TReal> jk((d+1)*d/2);
 for(i=0;i<d;i++)
 for(j=0;j<d;j++)if(i<=j)jk.add(j,i);
 pt_matr(jk);
 jack_r<TReal> jk1(jk);
 pt_matr(jk1);
 jack_it<TReal> it(&jk);
 for(it=0;it();it++)
 {
 std::cout<<std::setw(3)<<it.geti()<<std::setw(3)<<it.getj()<<std::setw(3)<<it.getd()<<std::endl;
 }
 prod();
 return 0;
}
/*
void main()
{
 //defjack<TReal> test
 int i,j;
 matr_com<TReal> mt(3),mt1(3);
 TReal x0[3]={1,1,1};
 for(i=0;i<3;i++)
 for(j=0;j<3;j++)mt[i][j]=i+j*10;
 defjak(mt,x0,mt1[0],9);
 mt.pt(cout);
 mt1.pt(cout);
//std::cout<<mt<<mt1;
// test of definition
}*/
