#include <iostream>
#include <matrix3_2/inc/cubic_equation.h>

double test1()
{
    VecN<double,4> cf = {3,0,0,1};
    auto res = cardano(cf);
    return res.data[1] - std::cbrt(-3.);
}

VecN<double,3> test2()
{
    VecN<double,4> cf = {-6,11,-6,1};
    auto res = cardano(cf);
    return {res.data[1] - 1, res.data[2] - 2, res.data[3] - 3};
}

VecN<double,3> test3()
{
    VecN<double,4> cf = {-2,0,1,1};
    auto res = cardano(cf);
    return {res.data[1] - 1, res.data[2] + 1, res.data[3] - 1};
}

int main()
{
    std::cout<<test1()<<std::endl;
    std::cout<<tr(test2())<<std::endl;
    std::cout<<tr(test3())<<std::endl;
}