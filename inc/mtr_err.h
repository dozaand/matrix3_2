#ifndef mtr_err_h
#define mtr_err_h
#include <iostream>
//using namespace std;
//#include <cstring>

//namespace mtr_31
//{
#define db_io        1
#define db_arg       db_io*2
#define db_bounds    db_arg*2
#define db_index     db_bounds*2
#define db_mem       db_index*2
#define db_math      db_mem*2
#define db_decomp    db_math*2
#define db_msg       db_decomp*2
#define db_no_vector db_msg*2
#define db_alg       db_no_vector*2
#define db_all       0xffff
#define db_          db_all

#if db_&db_io
#define d_opened(s,nm) if(!(s)){throw mtr_file_not_found(nm,__FILE__,__LINE__);}
#define d_eof(s) if(!(s)){throw mtr_eof(__FILE__,__LINE__);}
#else
#define d_opened(s,nm)
#define d_eof(s)
#endif

#if db_&db_bounds
#define d_bnd(mi,ma,mi1,ma1) if(mi!=mi1||ma!=ma1){throw mtr_bounds_or(mi,ma,mi1,ma1,__FILE__,__LINE__);}
#define d_index(mi,i,ma) if(i<mi||i>=ma){throw mtr_index_or(mi,i,ma,__FILE__,__LINE__);}
#else
#define d_bnd(mi,ma,mi1,ma1)
#endif

#if db_&db_index
#define d_range(mi,i,ma) if(i<mi||i>=ma){throw mtr_index_or(mi,i,ma,__FILE__,__LINE__);}
#else
#define d_range(mi,i,ma)
#endif

#if db_&db_mem
#define d_mem(p) if(p==NULL){throw mtr_mem(__FILE__,__LINE__);}
#else
#define d_mem(p)
#endif

#if db_&db_math
#define d_LU throw mtr_decomp_fail(__FILE__,__LINE__)
#else
#define d_LU
#endif

#if db_&db_msg
#define d_msg(m) throw mtr_msg(m,__FILE__,__LINE__)
#define d_tst(c,m) if((c)){throw mtr_msg(m,__FILE__,__LINE__);}
#define dbg_tst(c) if((c)){throw mtr_msg("error",__FILE__,__LINE__);}
#else
#define d_msg(m)
#define d_tst(c,m)
#define dbg_tst(c)
#endif

#if db_&db_no_vector
#define d_no_vector(a) if(a==NULL) throw mtr_storage_not_defined(__FILE__,__LINE__)
#else
#define d_no_vector(a)
#endif

#if db_&db_arg
#define d_uarg(a)  throw mtr_i_arg(a,__FILE__,__LINE__)
#define d_arg(c,a) if((c)) throw mtr_i_arg(a,__FILE__,__LINE__)
#define d_ne(i1,i2) if((i1!=i2)) throw mtr_i_ne(i1,i2,__FILE__,__LINE__)
#define d_lt(i1,i2) if((i1<i2)) throw mtr_i_lt(i1,i2,__FILE__,__LINE__)
#define d_le(i1,i2) if((i1<=i2)) throw mtr_i_le(i1,i2,__FILE__,__LINE__)
#else
#define d_arg(c,a)
#define d_ne(i1,i2)
#define d_lt(i1,i2)
#define d_le(i1,i2)
#endif

#if db_&db_alg
#define d_alg_msg(m) throw mtr_msg(m,__FILE__,__LINE__)
#define d_alg(c,m) if((c)){throw mtr_msg(m,__FILE__,__LINE__);}
#define dbg_tst(c) if((c)){throw mtr_msg("error",__FILE__,__LINE__);}
#else
#define d_alg_msg(m)
#define d_alg(c,m)
#define dbg_tst(c)
#endif

#undef db_

#define ___li___ const char* fil,int lin
#define ___lir__ fil,lin

class mtr_err
{
protected:
 const char * file;
 int line;
public:
 void ptl(std::ostream& s){s<<file<<": "<<line<<' ';}
 virtual void pt(std::ostream& s)=0;
 mtr_err(___li___):file(fil),line(lin)
 {
  line=lin;
 }
};
class mtr_msg:public mtr_err
{
public:
 const char* m;
 mtr_msg(const char* msg,___li___):mtr_err(___lir__),m(msg){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<m<<std::endl;
 }
};
class mtr_mem:public mtr_err
{
public:
 mtr_mem(___li___):mtr_err(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"matrix allocation failed\n";
 }
};

class mtr_io_err:public mtr_err
{
public:
 mtr_io_err(___li___):mtr_err(___lir__){}
};

class mtr_file_not_found:public mtr_io_err
{
 const char* file_nm;
public:
 mtr_file_not_found(char * nm,___li___):mtr_io_err(___lir__),file_nm(nm){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"file not found "<<file_nm<<std::endl;
 }
};

class mtr_sintax_error:public mtr_io_err
{
public:
 mtr_sintax_error(___li___):mtr_io_err(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"sintax error\n";
 }
};

class mtr_eof:public mtr_sintax_error
{
public:
 mtr_eof(___li___):mtr_sintax_error(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"unexpected end of file\n";
 }
};

class mtr_unexpected_string:public mtr_sintax_error
{
 const char* file_string;
public:
 mtr_unexpected_string(char * str,___li___):mtr_sintax_error(___lir__),file_string(str){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"unexpected string \""<<file_string<<"\"\n";
 }
};

class mtr_invalid_arg:public mtr_err
{
public:
 mtr_invalid_arg(___li___):mtr_err(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"invalid argument\n";
 }
};
class mtr_i_arg:public mtr_invalid_arg
{
 const char * msg;
public:
 mtr_i_arg(const char * Msg,___li___):mtr_invalid_arg(___lir__),msg(Msg){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"invalid argument\n";
  s<<msg;
 }
};

class mtr_index_or:public mtr_invalid_arg
{
public:
 int mi,ma,i;
 mtr_index_or(int mi_,int ma_,int i_,___li___):mtr_invalid_arg(___lir__),mi(mi_),ma(ma_),i(i_){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  if(i>=ma){s<<"index out of upper bound ";}
  else {s<<"index out of lower bound ";}
  s<<mi<<"<="<<i<<"<"<<ma<<std::endl;
 }
};
class mtr_i_ne:public mtr_invalid_arg
{
public:
 int i1,i2;
 mtr_i_ne(int i1_,int i2_,___li___):mtr_invalid_arg(___lir__),i1(i1_),i2(i2_){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<i1<<"!="<<i2<<std::endl;
 }
};

class mtr_i_lt:public mtr_invalid_arg
{
public:
 int i1,i2;
 mtr_i_lt(int i1_,int i2_,___li___):mtr_invalid_arg(___lir__),i1(i1_),i2(i2_){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<i1<<"<"<<i2<<std::endl;
 }
};
class mtr_i_le:public mtr_invalid_arg
{
public:
 int i1,i2;
 mtr_i_le(int i1_,int i2_,___li___):mtr_invalid_arg(___lir__),i1(i1_),i2(i2_){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<i1<<"<="<<i2<<std::endl;
 }
};

class mtr_storage_not_defined:public mtr_invalid_arg
{
public:
 int mi,ma,i;
 mtr_storage_not_defined(___li___):mtr_invalid_arg(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"no storage  ";
 }
};

class mtr_bounds_or:public mtr_invalid_arg
{
public:
 int mi,ma,mi1,ma1;
 mtr_bounds_or(int mi_,int ma_,int mi1_,int ma1_,___li___):mtr_invalid_arg(___lir__),mi(mi_),ma(ma_),mi1(mi1_),ma1(ma1_){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"bonds not eq"<<'('<<mi<<','<<ma<<")!=("<<mi1<<','<<ma1<<')'<<std::endl;
 }
};

class mtr_alg_fail:public mtr_err
{
public:
 mtr_alg_fail(___li___):mtr_err(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"matrix algorophm faild\n";
 }
};

class mtr_decomp_fail:public mtr_alg_fail
{
public:
 mtr_decomp_fail(___li___):mtr_alg_fail(___lir__){}
 virtual void pt(std::ostream& s)
 {
  ptl(s);
  s<<"matrix decomposition faild\n";
 }
};

//}

#undef ___li___
#undef ___lir__

#endif
