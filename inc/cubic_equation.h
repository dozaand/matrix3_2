#define _USE_MATH_DEFINES
#include <math.h>
//#include <complex>
#include <cassert>
#include "tmatr.h"

template<class real>
VecN<real,4> cardano(const VecN<real,2>& pq)
{
    const real sqr3 = std::sqrt(3.);
    real p = pq.data[0];
    real q = pq.data[1];
    real p3 = p/3.;
    real q2 = 0.5*q;
    real Q = p3*p3*p3 + q2*q2;
    if(Q >= 0)
    {
        real sqrQ = std::sqrt(Q);
        real alfa = std::cbrt(-q2 + sqrQ);
        real beta = std::cbrt(-q2 - sqrQ);
        real y = alfa + beta;
        return {Q, y, -0.5*y, 0.5*fabs(alfa - beta)*sqr3};
    }
    else if(Q < 0)
    {
        real sqrQ = sqrt(-Q);
        real fi = q2 != 0. ? std::atan(-sqrQ/q2) : M_PI_2;
        real ta = 2*sqrt(-p3)*sign(1,-q2);
        real y1 = ta*std::cos((fi - 2.*M_PI)/3);
        real y2 = ta*std::cos(fi/3);
        real y3 = ta*std::cos((fi + 2.*M_PI)/3);
        if(y1 > y2) std::swap(y1,y2);
        if(y2 > y3) std::swap(y2,y3);
        if(y1 > y2) std::swap(y1,y2);
        return {Q, y1, y2, y3};
    }
}

template<class real>
VecN<real,2> get_canonic_cf(const VecN<real,4>& cf)
{
    assert((!(cf.data[3] == 0.)) && "The given coefficients aren't coeffucients of the qubic equation");
    real d = cf.data[0];
    real c = cf.data[1];
    real b = cf.data[2];
    real a = cf.data[3];
    real a2 = a*a;
    real a3 = a2*a;
    real b2 = b*b;
    real b3 = b2*b;
    real ac = a*c;
    real abc = ac*b;
    real a2d = a2*d;
    return {(3*ac - b2)/(3*a2), (2*b3 - 9*abc + 27*a2d)/(27*a3)};
}

template<class real>
VecN<real,4> cardano(const VecN<real,4>& cf)
{
    auto res = cardano(get_canonic_cf(cf));
    real add = -cf.data[2]/(3*cf.data[3]);
    res.data[1] += add;
    res.data[2] += add;
    if(res.data[0] < 0)
    {
        res.data[3] += add;
    }
    return res;
}

template<class real>
std::pair<int,VecN<real,3>> cardano(const VecN<real,4>& cf, real xmin, real xmax)
{
    int ns = 0;
    auto sv = cardano(cf);
    VecN<real,3> res;
    real Q = sv.data[0];
    int nmax = Q < 0 ? 3 : Q > 0 ? 1 : 2;
    int i;
    for(i = 1; i <= nmax; ++i)
    {
        real x = sv.data[i];
        if(x < xmax && x >= xmin)
        {
            res.data[ns] = x;
            ++ns;
        }
    }
    return std::make_pair(ns,res);
}

/*! Solve equation (1,x,x^2,x^3).cf.(1,y,y^2,y^3) == r with respect to y, or with respect to x if transp = true*/
template<class real>
VecN<real,4> solve_cubic_2d(const MatNM<real,4,4>& cf, real x, bool transp = false)
{
    auto F = transp ? tr(cf) : cf;
    MatNM<real,1,4> X = {1, x, x*x, x*x*x};
    VecN<real,4> P = tr(X*F);
    return cardano(P);
}

template<class real>
VecN<real,3> solve_quadr_eq(const VecN<real,3>& cf)
{
    if(cf.data[2] == 0.)
    {
        real x = -cf.data[0]/cf.data[1];
        return {0.,x,x};
    }
    else
    {
        real D = cf.data[1]*cf.data[1] - 4.*cf.data[3]*cf.data[0];
        real sqD = std::sqrt(std::fabs(D));
        return D >= 0 ? VecN<real,3>({D, 0.5*(-cf.data[1] - sqD), 0.5*(-cf.data[1] + sqD)}) : VecN<real,3>({D, -0.5*cf.data[1], sqD});
    }
}

template<class real>
std::pair<int,VecN<real,2>> solve_quadr_eq(const VecN<real,3>& cf, real xmin, real xmax)
{
    auto sv = solve_quadr_eq(cf);
    real D = sv.data[0];
    int nmax = (D > 0) ? ( 2 ) : ((D < 0) ? 0 : 1);
    VecN<real,2> res;
    int n = 0;
    for(int i = 0; i < nmax; ++i)
    {
        real x = sv.data[i+1];
        if(x < xmax && x >= xmin)
        {
            res.data[n] = x;
            ++n;
        }
    }
    return std::make_pair(n, res);
}