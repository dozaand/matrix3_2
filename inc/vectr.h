#ifndef vectr1_h
#define vectr1_h

#include <fstream>
#include <matrix3_2/inc/mtr_err.h>
#include <matrix3_2/inc/mtr_err.h>
template <class T>
class Vect
{
protected:
 int owner;
 int siz;
 void chk(const Vect* el) const
 {
#if db_&(db_range|l2)
  dbg_tst(siz!=el->siz);
#endif
 }
public:
 T* arr;
 Vect(){owner=1;arr=(T*)NULL;siz=0;}
 Vect(T* a,int s){arr=a;siz=s;owner=0;}
 Vect(int s){arr=(T*)NULL;owner=1;resize(s);}
 Vect(Vect<T>& el){owner=1;arr=(T*)NULL;(*this)=el;}
 ~Vect(){if(owner)delete [] arr;}
 Vect& operator=(const T& el);
 Vect& operator=(const Vect<T>& el);
 void set(T* d,int s){if(owner&&arr!=NULL){delete [] arr;}siz=s;arr=d;owner=0;}
 T& operator[](int i)
 {
#if db_&(db_range|l3)
  dbg_tst(i<0||i>=siz);
#endif
  return arr[i];
 }
 int dim() const {return siz;}
 int resize(int s)
 {
  if(owner)
  {
   if(arr!=NULL){delete [] arr;}
   if(s>0)
   {
    arr=new T[s];
    #if db_&(db_nomem|l2)
    dbg_tst(arr==NULL);
    #endif
   }
   siz=s;
  }
  return siz;
 }
 T* data(){return arr;}
 operator T* (){return arr;}
};

template <class T> Vect<T>& Vect<T>::operator=(const T& el){for(int i=0;i<siz;i++){arr[i]=el;}return *this;}
template <class T> Vect<T>& Vect<T>::operator=(const Vect<T>& el)
{
 int si0=siz;if(si0>el.siz)si0=el.siz;
 for(int i=0;i<si0;i++){arr[i]=el.arr[i];}
 return *this;
}

template <class T>
class Vectr:public virtual Vect<T>
{
 using Vect<T>::siz;
 using Vect<T>::arr;
public:
 Vectr():Vect<T>(){}
 Vectr(T* a,int s):Vect<T>(a,s){}
 Vectr(int s):Vect<T>(s){}
 Vectr(Vect<T>& el):Vect<T>(el){}
 Vectr& operator+=(T el){for(int i=0;i<siz;i++){arr[i]+=el;}return *this;}
 Vectr& operator-=(T el){for(int i=0;i<siz;i++){arr[i]-=el;}return *this;}
 Vectr& operator*=(T el){for(int i=0;i<siz;i++){arr[i]*=el;}return *this;}
 Vectr& operator/=(T el){for(int i=0;i<siz;i++){arr[i]/=el;}return *this;}
 Vectr& operator+=(const Vectr<T>& el){chk(&el);for(int i=0;i<siz;i++){arr[i]+=el.arr[i];}return *this;}
 Vectr& operator-=(const Vectr<T>& el){chk(&el);for(int i=0;i<siz;i++){arr[i]-=el.arr[i];}return *this;}
 Vectr& operator*=(const Vectr<T>& el){chk(&el);for(int i=0;i<siz;i++){arr[i]*=el.arr[i];}return *this;}
 Vectr& operator/=(const Vectr<T>& el){chk(&el);for(int i=0;i<siz;i++){arr[i]/=el.arr[i];}return *this;}
 Vectr& operator=(const T& el){Vect<T>::operator=(el);return *this;}
 Vectr& operator=(const Vectr<T>& el){Vect<T>::operator=(el);return *this;}
};

template <class T> T norm(Vectr<T>& el){T d=0;for(int i=0;i<el.dim();i++)
{
  d+=el.arr[i]*el.arr[i];}return d;
}
template <class T> T dot(Vectr<T>& el1,Vectr<T>& el2){T d=0;for(int i=0;i<el1.dim();i++){d+=el1.arr[i]*el2.arr[i];}return d;}

template <class T>
class Vectio:public virtual Vect<T>
{
 using Vect<T>::siz;
 using Vect<T>::arr;
 using Vect<T>::resize;
public:
 Vectio():Vect<T>(){}
 Vectio(T* a,int s):Vect<T>(a,s){}
 Vectio(int s):Vect<T>(s){}
 Vectio(Vect<T>& el):Vect<T>(el){}
 Vectio(std::istream& s):Vect<T>(){rd(s);}
 void pt(std::ostream& s);
 void rd(std::istream& s);
 Vectio& operator=(const T& el){Vectio<T>::operator=(el);return *this;}
 Vectio& operator=(const Vectio<T>& el){Vect<T>::operator=((const Vect<T>&)el);return *this;}
};
template <class T> void Vectio<T>::pt(std::ostream& s)
{
 s<<siz<<std::endl;
#if db_&(db_io|l2)
  dbg_tst(!s);
#endif
 for(int i=0;i<siz;i++)
 {
  s<<arr[i]<<std::endl;
#if db_&(db_io|l3)
  dbg_tst(!s);
#endif
 }
}
template <class T> void Vectio<T>::rd(std::istream& s)
{
 int i,ss;s>>ss;
#if db_&(db_io|l2)
  dbg_tst(!s);
#endif
#if db_&(db_io|l1)
 dbg_tst(ss!=siz&&!owner);
#endif
 resize(ss);
 for(i=0;i<siz;i++)
 {
  s>>arr[i];
#if db_&(db_io|l2)
  dbg_tst(!s);
#endif
 }
}

template <class T>
class Vectrio:public Vectr<T>,public Vectio<T>
{
public:
 Vectrio():Vect<T>(),Vectr<T>(),Vectio<T>(){}
 Vectrio(T* a,int s):Vect<T>(a,s),Vectr<T>(a,s),Vectio<T>(a,s){}
 Vectrio(int s):Vect<T>(s),Vectr<T>(s),Vectio<T>(s){}
 Vectrio(Vect<T>& el):Vectr<T>(el),Vectio<T>(el){}
 Vectrio(std::istream& s):Vectr<T>(),Vectio<T>(s){}
 Vectrio& operator=(const T& el){Vect<T>::operator=(el);return *this;}
 Vectrio& operator=(const Vect<T>& el){Vect<T>::operator=((const Vect<T>&)el);return *this;}
};

/*
template <class T> std::ostream& operator<<(std::ostream& s,Vectio<T>& el){el.pt(s);return s;}
template <class T> std::istream& operator>>(std::istream& s,Vectio<T>& el){el.rd(s);return s;}
*/

inline std::ostream& operator<<(std::ostream& s,Vectio<int>& el){el.pt(s);return s;}
inline std::istream& operator>>(std::istream& s,Vectio<int>& el){el.rd(s);return s;}
inline std::ostream& operator<<(std::ostream& s,Vectio<long>& el){el.pt(s);return s;}
inline std::istream& operator>>(std::istream& s,Vectio<long>& el){el.rd(s);return s;}
inline std::ostream& operator<<(std::ostream& s,Vectio<float>& el){el.pt(s);return s;}
inline std::istream& operator>>(std::istream& s,Vectio<float>& el){el.rd(s);return s;}
inline std::ostream& operator<<(std::ostream& s,Vectio<double>& el){el.pt(s);return s;}
inline std::istream& operator>>(std::istream& s,Vectio<double>& el){el.rd(s);return s;}


#endif
