#ifndef spvect_h
#define spvect_h

#include <vector>
#include <map>
#include <iostream>

using namespace std;

template <class T,class Op>
void map_funct(const map<int,T>& m1,const map<int,T>& m2,map<int,T>& result,Op el)
{
 pair<int,T> *pm=new pair<int,T>[m1.size()+m2.size()],*pp;
 typename map<int,T>::const_iterator it1=m1.begin(),it2=m2.begin(),end1=m1.end(),end2=m2.end();
 result.clear();
 pp=pm;
 for(;;)
 {
  if(it1==end1)
  {
   for(;it2!=end2;++it2,++pp)
   {
    *pp=*it2;
   }
   break;
  }
  if(it2==end2)
  {
   for(;it1!=end1;++it1,++pp)
   {
    *pp=*it1;
   }
   break;
  }
  if((*it1).first<(*it2).first)
  {
   *pp=*it1;
   ++pp;++it1;
  }
  else if((*it1).first>(*it2).first)
  {
   *pp=*it2;
   ++pp;++it2;
  }
  else
  {
   (*pp).first=(*it1).first;
   (*pp).second=el((*it1).second,(*it2).second);
   ++pp;++it1;++it2;
  }
 }
 result.insert(pm,pp);
 delete [] pm;
}

template <class T>
class TPlus_op
{
public:
 inline T operator()(T f,T s) const {return f+s;}
};
template <class T>
class TMinus_op
{
public:
 inline T operator()(T f,T s) const {return f-s;}
};
template <class T>
class TTimes_op
{
public:
 inline T operator()(T f,T s) const {return f*s;}
};

template<class T>
class spvect
{
 map<int,T> data;
public:
 spvect(){}
 spvect(int * v,T* p,int n)
 {
  for(int i=0;i<n;i++)
  {
   if(p[i]!=0){data[v]=p;}
  }
 }
 void flush(){data.clear();}
 void set(int i,T d){data[i]=d;}
 void operator*=(T x)
 {
  typename map<int,T>::iterator it;
  for(it=data.begin();it!=data.end();++it)
  {
   (*it).second*=x;
  }
 }
 void do_plus(const spvect<T>& e,spvect<T>& to) const
 {
  map_funct(this->data,e.data,to.data,TPlus_op<T>());
 }
 void do_times(const spvect<T>& e,spvect<T>& to) const {map_funct(this->data,e.data,to.data,TTimes_op<T>());}
 spvect<T>& operator+=(const spvect<T> v)
 {
  typename map<int,T>::const_iterator it,end;
  end=v.data.end();
  for(it=v.data.begin();it!=end;++it)
  {
   data[(*it).first]+=(*it).second;
  }
  return *this;
 }
 spvect<T> operator+(const spvect<T> v) const
 {
  spvect<T> res;
  do_plus(v,res);
  return res;
 }
 T dot(const T * v) const
 {
  typename map<int,T>::const_iterator it=data.begin(),ite=data.end();
  T res=0;
  for(;it!=ite;++it)
  {
   res+=(*it).second*v[(*it).first];
  }
  return res;
 }
 void pt(ostream& s) const
 {
  typename map<int,T>::const_iterator it;
  for(it=data.begin();it!=data.end();++it)
  {
   cout<<(*it).first<<' '<<(*it).second<<std::endl;
  }
 }
 T& operator[](int i){return data[i];}
 T operator[](int i) const {return data[i];}
 typename map<int,T>::iterator begin() {return data.begin();}
 typename map<int,T>::const_iterator begin() const {return data.begin();}
 typename map<int,T>::const_iterator end() const {return data.end();}
 typename map<int,T>::iterator end() {return data.end();}
 int first_index() const
 {
  if(data.count()==0){return -1;}
  else{return *(data.begin()).first;}
 }
 int last_index() const
 {
  if(data.count()==0){return -1;}
  else{return *(--data.end()).first;}
 }
};
#endif