#ifndef int_bsearch_h
#define int_bsearch_h
#include <stdlib.h>

//! поиск места в упорядоченом массиве ближайшего снизу к заданному значению (исключая послеюнюю точку)
template <class T,class Tel>
int int_bsearch(int n, const T& a, const Tel& s)
{
 int i,j=0;
 for(;n>2;)
 {
  i=n/2;
  Tel t=a[i+j];
  if(s<t)
  {
      n=i+1;
  }
  else
  {
      j+=i;
      n-=i;
  }
 }
 return j;
}
/*
template <class T>
const T* int_bsearch(int n, const T* a, const T& s)
{
 int i;
 for(;n>2;)
 {
  i=n/2;
  if(s<a[i]){n=i+1;}
  else{a+=i;n-=i;}
 }
 return a;
}
*/
//! поиск места в упорядоченом массиве ближайшего снизу к заданному значению (исключая послеюнюю точку)
/*!
 это интерфейс для массивов
*/
template <class T>
const T* int_bsearch(const T* a,const T* end, const T &s)
{
 int i;
 int n=end-a;
 for(;n>2;)
 {
  i=n/2;
  if(s<a[i]){n=i+1;}
  else{a+=i;n-=i;}
 }
 return a;
}
template <class T>
int iint_bsearch(T* a,T* end, const T &s)
{
 return int_bsearch(a,end,s)-a;
}

//! двоичный поиск элемента в упорядоченном массиве
/*!
 это интерфейс для массивов
*/
template <class T>
T* bin_search(T* a,T* end,const T& s)
{
 int i;
 int n=end-a;
 for(;n>2;)
 {
  i=n/2;
  if(s<a[i]){n=i+1;}
  else{a+=i;n-=i;}
 }
 if(a[0]<s) return NULL;
 return a;
}

#endif