#pragma once

#if defined __GNUC__ || defined __STDC__
#define _hypot
#endif
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#ifndef NOT_USE_YAML_DSC
#include <yaml-cpp/yaml.h>
#endif

/*! маркировка файла
*/

namespace s_tab
{

template <class T>
std::ostream& put(std::ostream& s,const T& obj)
{
 s.write((char*)&obj,sizeof(T));return s;
}
template <class T>
std::istream& get(std::istream& s,const T& obj)
{
 s.read((char*)&obj,sizeof(T));return s;
}

class Node
{
public:
    unsigned int beg,end;
    std::map<std::string,Node> items;
#ifdef NOT_USE_YAML_DSC
    void save(std::ostream& s);
    void load(std::istream& s);
#endif
};

#ifndef NOT_USE_YAML_DSC
YAML::Emitter& operator << (YAML::Emitter& out, const Node& v);
const YAML::Node& operator >> (const YAML::Node& node, Node& v);
#endif

class tostream;

class tab
{
    Node* nd;
public:
    tostream& s;
    tab(tab& s_,const char* nm);
    tab(tostream& s_,const char* nm);
    ~tab();
};

class tstream
{
protected:
 std::string marker_nm;
 Node root;
public:
 std::map<std::string,int> type;// versions of used types
 tstream(const char* nm);
 static const char* ver(){return "tstream 1.0";}
};

class tostream:public tstream,public std::ofstream
{
 friend class tab;
public:
 tostream(const char* nm);
 ~tostream();
 void flush();
};


class tistream:public tstream,public std::ifstream
{
 std::vector<Node*> stk;
public:
 tistream(const char* nm);
 ~tistream();
 void cd(const char * pos);
 inline void cd(const std::string& pos) {cd(pos.c_str());}
 void extract(const char * file_name,const char * pos) const;
};

// default  is to save as pod type
template<typename T>
inline tostream& operator&(tostream& s,const T& el)
{
 s.write((char*)&el,sizeof(el));
 return s;
}

template<typename T>
inline tistream& operator&(tistream& s,T& el)
{
 s.read((char*)&el,sizeof(T));
 return s;
}

// some stl types
inline tistream& operator&(tistream& s, std::string& obj)
{
    int len;
    s & len;
    obj.resize(len);
    for(int i=0;i<len;++i)
    {
      obj[i]=s.get();
    }
    return s;
}

inline tostream& operator&(tostream& s, const std::string& obj)
{
    int len=obj.size();
    s & len;
    s.write(obj.c_str(), len);
    return s;
}
template <typename T>
inline tistream& operator&(tistream& s, std::vector<T>& obj)
{
    unsigned len,i;
    s & len;
    if(len!=obj.size()){obj.resize(len);}
    for(i=0;i<len;++i)
    {
        s & obj[i];
    }
    return s;
}

template <typename T>
inline tostream& operator&(tostream& s, const std::vector<T>& obj)
{
    unsigned len=obj.size(),i;
    s & len;
    for(i=0;i<len;++i)
    {
        s & obj[i];
    }
    return s;
}


template <typename Tkey,typename Tval>
inline tistream& operator&(tistream& s, std::map<Tkey,Tval>& obj)
{
    unsigned len,i;
    Tkey key;
    s & len;
    for(i=0;i<len;++i)
    {
        s & key;
        s & obj[key];
    }
    return s;
}

template <typename Tkey,typename Tval>
inline tostream& operator&(tostream& s, const std::map<Tkey,Tval>& obj)
{
    unsigned len=obj.size();
    s & len;
    for(auto it=obj.begin();it!=obj.end();++it)
    {
        s & it->first & it->second;
    }
    return s;
}

}

// single input and output function named htio (TYP&)el) - removes const check :(
#define HIIO(TYP) namespace s_tab \
{ \
inline tistream& operator&(tistream& arh,TYP& el) \
{   el.htio(arh); \
    return arh; \
} \
inline tostream& operator&(tostream& arh,const TYP& el) \
{   ((TYP&)el).htio(arh); \
    return arh; \
} \
} 

// versioned io type tag in file is type name in file
#define HIIO_V(TYP,VER) namespace s_tab \
{ \
inline tistream& operator&(tistream& arh,TYP& el) \
{ el.load(arh,arh.type[#TYP]); \
    return arh; \
} \
inline tostream& operator&(tostream& arh,const TYP& el) \
{ \
    arh.type[#TYP]=VER;el.save(arh); \
    return arh; \
} \
} 

// versioned io type tag defined explicit. if type name si simple
#define HIIO_VT(TYP,TAG,VER) namespace s_tab \
{ \
inline tistream& operator&(tistream& arh,TYP& el) \
{ \
    return el.load(arh,arh.type[#TAG]); \
} \
inline tostream& operator&(tostream& arh,const TYP& el) \
{ \
    arh.type[#TAG]=VER;el.save(arh); \
    return arh; \
} \
} 
