#ifndef Tvectr1_h
#define Tvectr1_h

template <class T,int N>
class TVect
{
public:
  enum {SIZE=N};
 T arr[N];
 TVect(){}
 TVect(const T* el){(*this)=el;}
 TVect(const TVect<T,N>& el)
 {
  (*this)=el;
 }
 TVect& operator=(const T& el)
 {
  for(int i=0;i<N;i++){arr[i]=el;}
 }
 TVect& operator=(const T* el)
 {
  for(int i=0;i<N;i++){arr[i]=el[i];}
  return *this;
 }
 inline TVect& operator=(const TVect<T,N>& el)
 {
  for(int i=0;i<N;i++){arr[i]=el.arr[i];}
  return *this;
 }
 T& operator[](int i)
 {
  return arr[i];
 }
 const T& operator[](int i) const
 {
  return arr[i];
 }
 int dim() const {return N;}
 operator T*(){return arr;}
 T* data(){return arr;}
 const T* data() const {return arr;}
};


template <class T,int N>
class TVectr:public virtual TVect<T,N>
{
// using TVectr<T,N>::arr;
 using TVect<T,N>::arr;
public:
 TVectr():TVect<T,N>(){}
 TVectr(TVect<T,N>& el):TVect<T,N>(el){}
 TVectr& operator+=(T el);
 TVectr& operator-=(T el);
 TVectr& operator*=(T el);
 TVectr& operator/=(T el);
 TVectr& operator+=(const TVectr<T,N>& el);
 TVectr& operator-=(const TVectr<T,N>& el);
 TVectr& operator*=(const TVectr<T,N>& el);
 TVectr& operator/=(const TVectr<T,N>& el);
 TVectr& operator=(const T& el){TVect<T,N>::operator=(el);return *this;}
 TVectr& operator=(const T* el){TVect<T,N>::operator=(el);return *this;}
 TVectr& operator=(const TVectr<T,N>& el){TVect<T,N>::operator=(el);return *this;}
};
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator+=(const TVectr<T,N>& el)
{
    for(int i=0;i<N;i++){arr[i]+=el.arr[i];}return *this;
}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator-=(const TVectr<T,N>& el)
{for(int i=0;i<N;i++){arr[i]-=el.arr[i];}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator*=(const TVectr<T,N>& el)
{for(int i=0;i<N;i++){arr[i]*=el.arr[i];}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator/=(const TVectr<T,N>& el)
{for(int i=0;i<N;i++){arr[i]/=el.arr[i];}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator+=(T el){for(int i=0;i<N;i++){arr[i]+=el;}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator-=(T el){for(int i=0;i<N;i++){arr[i]-=el;}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator*=(T el){for(int i=0;i<N;i++){arr[i]*=el;}return *this;}
template <class T,int N>  TVectr<T,N>& TVectr<T,N>::operator/=(T el){for(int i=0;i<N;i++){arr[i]/=el;}return *this;}
template <class T,int N> T norm(TVectr<T,N>& el){T d=0;for(int i=0;i<el.dim();i++)
{
    d+=el.arr[i]*el.arr[i];}
    return d;
}
template <class T,int N> T dot(const TVectr<T,N>& el1,const TVectr<T,N>& el2)
{
 T d=0;
 for(int i=0;i<N;i++){d+=el1.arr[i]*el2.arr[i];}
 return d;
}



#endif
