#ifndef __MATRIX_MULTIPLE
#define __MATRIX_MULTIPLE

#include <matrix3_2/inc/matr_com.h>
#include <matrix3_2/inc/rqueue.h>

namespace matmul
{
//!return res=A.B
template <class T1, class T2, class T3>
void mul(const T1& A, const T2 &B, T3 &res)
{
  if(A.dim()!=B.dimo()) throw "Input dimension of A must be equal to output dimension of B!";
  else
  {
    if((res.dim()!=B.dim())||(res.dimo()!=A.dimo()))throw "Input dimension of A must be equal to output dimension of B!";
    int i,j,k;
    for(i=0;i<res.dimo();i++)
    {
      for(j=0;j<res.dim();j++)
      {
        res[i][j]=0;
        for(k=0;k<A.dim();k++)((T3&)res)[i][j]+=((T1&)A)[i][k]*((T2&)B)[k][j];
      }
    }
  }
}

//!return res=Transpose[A].B
template <class T1, class T2, class T3>
void trmul(const T1& A, const T2 &B, T3 &res)
{
  if(A.dimo()!=B.dimo()) throw "Input dimension of Transpose[A] must be equal to output dimension of B!";
  else
  {
    if((res.dim()!=B.dim())||(res.dimo()!=A.dim()))throw "Input dimension of Transpose[A] must be equal to output dimension of result!";
    int i,j,k;
    for(i=0;i<res.dimo();i++)
    {
      for(j=0;j<res.dim();j++)
      {
        res[i][j]=0;
        for(k=0;k<A.dimo();k++)((T3&)res)[i][j]+=((T1&)A)[k][i]*((T2&)B)[k][j];
      }
    }
  }
}

//!return res=A.Transpose[B]
template <class T1, class T2, class T3>
void multr(T1 &A, T2 &B, T3 &res)
{
  if(A.dim()!=B.dim()) throw "Input dimension of A must be equal to output dimension of Transpose[B]!";
  else
  {
    if((res.dim()!=B.dimo())||(res.dimo()!=A.dimo()))throw "Input dimension of A must be equal to output dimension of Transpose[B]!";
    int i,j,k;
    for(i=0;i<res.dimo();i++)
    {
      for(j=0;j<res.dim();j++)
      {
        res[i][j]=0;
        for(k=0;k<A.dim();k++)
        {
          ((T3&)res)[i][j]+=((T1&)A)[i][k]*((T2&)B)[j][k];
        }
      }
    }
  }
}

//!return res=A.B
template <class T1, class T2, class T3>
void trmultr(const T1& A, const T2 &B, T3 &res)
{
  if(A.dimo()!=B.dim()) throw "Input dimension of A must be equal to output dimension of B!";
  else
  {
    if((res.dim()!=B.dimo())||(res.dimo()!=A.dim()))throw "Input dimension of A must be equal to output dimension of B!";
    int i,j,k;
    for(i=0;i<res.dimo();i++)
    {
      for(j=0;j<res.dim();j++)
      {
        res[i][j]=0;
        for(k=0;k<A.dimo();k++)((T3&)res)[i][j]+=((T1&)A)[k][i]*((T2&)B)[j][k];
      }
    }
  }
}

}

#endif