#ifndef MATR_SUPERLU_H
#define MATR_SUPERLU_H

#include <matrix3_2/inc/matr_umf.h>
#include <supermatrix.h>
#include <slu_mt_ddefs.h>

template <typename TReal>
class matr_superlu: public matrix<TReal>
{
protected:
  typedef matr_umf<TReal> matr_t;
  using matrix<TReal>::Dim;
  using matrix<TReal>::Dimo;
  using matrix<TReal>::dim;
  using matrix<TReal>::dimo;
  TReal& operator()(int i, int j){static TReal x=0; return x;}
  bool permutated;
  SuperMatrix A, L, U, B, R, Atr, Aperm;
  std::vector<int> perm_c_v;
  std::vector<int> perm_r_v;
  std::vector<int> etree_v;
  std::vector<int> colcnt_h_v;
  std::vector<int> part_super_h_v;
  superlumt_options_t options;
  Gstat_t Gstat;
  bool Gstat_allocated;
  trans_t trans;
  matr_t* Matr;
  int nprocs;
  bool solving_first_time;
  void destroy_all();
/*
* Get column permutation vector perm_c[], according to permc_spec:
* permc_spec = 0: natural ordering
* permc_spec = 1: minimum degree on structure of A�*A
* permc_spec = 2: minimum degree on structure of A�+A
* permc_spec = 3: approximate minimum degree for unsymmetric matrices
*/
  enum ordering
  {
      NATURAL = 0,
      MINIMIM_DEGREE_ATA = 1,
      MINIMIM_DEGREE_APA = 2,
      UNSYMMETRIC = 3
  };
public:
 void set_nproc(int n);
 int get_nproc()const{return nprocs;}
 matr_superlu(int x);
 ~matr_superlu();
 void update(matr_t& ma);
 TReal* mul(const TReal*, TReal*);
 void mul_py(long, long);
};

#endif