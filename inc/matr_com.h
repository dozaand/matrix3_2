#ifndef matr_com_h
#define matr_com_h
#include <matrix3_2/inc/matrix.h>

//#include <complex>

using namespace std;

namespace SolveMethod
{

enum {LU,LU_R,UTU,SVD,QR};

}

template <class TReal> class matr_comns_it;
template <class TReal>
class matr_comns:public matrixns<TReal> 
{
 friend class matr_comns_it<TReal>;
protected:
 using matrixns<TReal>::Dimo;
 using matrixns<TReal>::Dim;
 int Size;
 TReal *arr;
public:
 using matrixns<TReal>::dimo;
 using matrixns<TReal>::dim;
 matr_comns(istream &ist);
 matr_comns(int y,int x=0);
 matr_comns(int y,int x,TReal *d);
 matr_comns(const matr_comns<TReal>& m);
 matr_comns(jack<TReal>& jk);
 matr_comns(oper_iter<TReal>& it);
 virtual ~matr_comns(){delete [] arr;}
 matr_comns<TReal>& operator=(TReal x);
 virtual TReal& operator()(int i_row,int i_col){return arr[i_row*Dim+i_col];}
 TReal* operator[](int i)const
 {
  d_range(0,i,Dimo);
  return &arr[i*Dim];
 }
// do whith care reset_to_size
 int reset_to_size(int y,int x,TReal* data=NULL)
 {
     int retval = 0;
     if (x*y <= Size){ Dim = x; Dimo = y; retval = 1; }
     else { Dim = x; Dimo = y; delete[]arr; Size = x*y; arr = new TReal[Size]; retval = 2; }
  if(data)
  {
    for(int i=0;i<x*y;++i){arr[i]=data[i];}
  }
  return retval;
 }
 TReal* addmul(const TReal *x,TReal *y);
 TReal* addmultr(const TReal *x,TReal *y);
 TReal* mul(const TReal *x,TReal *y=NULL);
 TReal* multr(const TReal *x,TReal *y=NULL);
 void transp();
#ifndef __MAKECINT__
 std::ostream& pt(std::ostream& s);
#endif
// special functions
 matr_comns<TReal>& prod(const matr_comns<TReal>& ma1,const matr_comns<TReal>& ma2);// this matrix<TReal> -> product of m1,m2
 matr_comns<TReal>& operator+=(const matr_comns<TReal>& ma);
 matr_comns<TReal>& operator-=(const matr_comns<TReal>& ma);
 matr_comns<TReal>& operator*=(const matr_comns<TReal>& ma);
 matr_comns<TReal>& operator*=(TReal d);
 void set_E();
 void set_rand();
 void set_rand(TReal from,TReal to);
 matr_comns<TReal>& operator=(const matr_comns<TReal>& ma);
 matr_comns<TReal>& AB(matr_comns<TReal>& B,TReal * buf);
 matr_comns<TReal>& ABT(matr_comns<TReal>& B,TReal * buf);
// matr_comns<TReal>& BA(matr_comns<TReal>& B,TReal * buf);
// matr_comns<TReal>& BTA(matr_comns<TReal>& B,TReal * buf);
 TReal norm() const;
 TReal* residual(const TReal * x,TReal * b); //b->b-A.x
 int size()const;
 TReal svrt(TReal* x,TReal* y) const;
};

template <class TReal>
class sub_matr_comns
{
  TReal **data;
  int Dim,Dimo;
public:
  sub_matr_comns(matr_comns<TReal>& A, int x, int y, int dx, int dy):
    data(new TReal*[dx*dy]),Dim(dx),Dimo(dy)
  {
    int i,j;
    for(i=0;i<Dimo;i++)
    {
      for(j=0;j<Dim;j++)
      {
        data[j+i*Dim]=&(A[i+y][j+x]);
      }
    }
  }
  ~sub_matr_comns(){delete[]data;}
  TReal** operator[](int i){return &(data[i*Dim]);}
  TReal& operator()(int i, int j)
  {
    return *data[j+i*Dim];
  }
  void reset(matr_comns<TReal>& A,int x,int y)
  {
    int i,j;
    for(i=0;i<Dimo;i++)
    {
      for(j=0;j<Dim;j++)
      {
        (*this)[i][j]=&(A[i+y][j+x]);
      }
    }
  }
  int dim(){return Dim;}
  int dimo(){return Dimo;}
};

template <class TReal>
inline int defjak(operat<TReal>& op,TReal *x0,matr_comns<TReal>& ma,TReal deviat=1.1){return defjak(op,x0,ma[0],ma.dim()*ma.dim(),deviat);}

template <class TReal> class matr_com_lu;
template <class TReal>
class matr_com;

template <class TReal>
void spectr(matr_com<TReal>& mat,TReal ** re,TReal ** im);

template <class TReal>
class matr_com:public matr_comns<TReal>
{
  friend void spectr<>(matr_com<TReal>& mat,TReal ** re,TReal ** im);
  friend class matr_com_lu<TReal>;
public:
  using matr_comns<TReal>::Dim;
  using matr_comns<TReal>::Dimo;
  using matr_comns<TReal>::arr;
  using matr_comns<TReal>::Size;
 matr_com(istream& __s):matr_comns<TReal>(__s){}
 matr_com(int x):matr_comns<TReal>(x,x){}
 matr_com(int x,TReal *d):matr_comns<TReal>(x,x,d){dbg_tst(Dim!=Dimo);}
 matr_com(jack<TReal>& jk):matr_comns<TReal>(jk){dbg_tst(Dim!=Dimo);}
 matr_com(oper_iter<TReal>& it):matr_comns<TReal>(it){dbg_tst(Dim!=Dimo);}
 matr_com(const matr_com<TReal> &it):matr_comns<TReal>(it.dimo(),it.dim())
 {
  memcpy(arr,it[0],Size*sizeof(TReal));
  dbg_tst(Dim!=Dimo);
 }
 matr_com<TReal>* matr_exp(TReal dt,TReal eps=1e-6,matr_com<TReal>* elm=NULL);
 matr_comns<TReal>& operator=(TReal x){return matr_comns<TReal>::operator=(x);}
 void get_diag(TReal* d)const{int i;for(i=0;i<Dim;i++)d[i]=arr[i+i*Dim];}
 void set_diag(const TReal* d){int i;for(i=0;i<Dim;i++)arr[i+i*Dim]=d[i];}
 void set_diag(TReal d){int i;for(i=0;i<Dim;i++)arr[i+i*Dim]=d;}
 int reset_to_size(int x){if(x*x<=Size){Dim=Dimo=x;return 1;}return 0;}
 void E_hA(int a);
 void kvadr(int times=1);
};


template <class TReal>
class matr_comns_it:public oper_iter<TReal> {// element iterator
  using oper_iter<TReal>::pos;
 int i,j,lim,Dim;
 matr_comns<TReal> *el;
public:
 matr_comns_it(matr_comns<TReal>* elm){pos=0;el=elm;lim=el->dim()*el->dimo();Dim=elm->dim();}
 int operator()(){return pos<lim;}// test position to be in range
 oper_iter<TReal>& operator=(int ii){pos=ii;j=pos/Dim;i=pos-j*Dim;return *this;}// set position
 oper_iter<TReal>& operator=(matr_comns_it<TReal>& elm){memcpy(this,&elm,sizeof(matr_comns_it));return *this;}// set position
 int operator++(int){if(++i>=Dim){i=0;j++;}return ++pos;}// go to next position
 TReal& getd(){return el->arr[pos];}// get data pointer
 int geti(){return i;}  // corespondent row and column indexes
 int getj(){return j;}
 int dim() const {return el->dim();}
 int dimo() const {return el->dimo();}
};

template <class TReal>
class matr_com_lu:public matrix<TReal> {// common lu decomposition and backsubstitution
protected:
  using matrix<TReal>::Dim;
  using matrix<TReal>::Dimo;
  using matrix<TReal>::dim;
  using matrix<TReal>::dimo;
 TReal *lu;
 virtual void dcmp(void){};
 void copy(matr_com<TReal>& el);
public:
 matr_com_lu(int x){Dim=x;lu=new TReal[Dim*Dim];d_mem(lu);}
 matr_com_lu(matr_com<TReal>& ma)
 {
  Dim=ma.dim();
  Dimo=ma.dimo();
  lu=new TReal[Dim*Dim];d_mem(lu);
  copy(ma);
 }
 ~matr_com_lu(){delete [] lu;}
 TReal* operator[](int i){return &lu[i*Dim];}
 TReal& operator()(int i, int j){return lu[i*Dim+j];}
 TReal operator()(int i, int j)const {return lu[i*Dim+j];}
 void update(){dcmp();}// do not use it twise
 void update(matr_com<TReal>& ma)
 {
  dbg_tst(Dim<ma.dim());
  copy(ma);
  dcmp();
 }
 void pt(std::ostream&);
 TReal svrt(TReal* x,TReal* y) const;
};

template <class TReal>
class matr_com_lux:public matr_com_lu<TReal> 
{
// simple decomposition (no reodering)
 using matr_com_lu<TReal>::estop;
 using matr_com_lu<TReal>::Dim;
 using matr_com_lu<TReal>::lu;
 void dcmp(void);
public:
 matr_com_lux(int x):matr_com_lu<TReal>(x){}
 matr_com_lux(matr_com<TReal>& ma):matr_com_lu<TReal>(ma){dcmp();};
 TReal* addmul (const TReal *x,TReal *y){estop();return NULL;}
 TReal* addmultr (const TReal *x,TReal *y){estop();return NULL;}
 TReal* mul (const TReal *x,TReal *y);
 TReal* multr (const TReal *x,TReal *y){estop();return NULL;}
 void bksb (TReal *x);
};

template <class TReal>
class matr_com_luo:public matr_com_lu<TReal> {// decomposition  pivot serch  is on
 int *indx;
 using matr_com_lu<TReal>::Dim;
 using matr_com_lu<TReal>::estop;
 using matr_com_lu<TReal>::lu;
 using matr_com_lu<TReal>::dim;
 void dcmp(void);
public:
 matr_com_luo(int x):matr_com_lu<TReal>(x){indx=new int[Dim];d_mem(indx);}
 matr_com_luo(matr_com<TReal>& ma):matr_com_lu<TReal>(ma)
 {
  indx=new int[Dim];d_mem(indx);
  dcmp();
 }
 ~matr_com_luo(){delete [] indx;}
 TReal* addmul (const TReal *x,TReal *y){estop();return NULL;}
 TReal* addmultr (const TReal *x,TReal *y){estop();return NULL;}
 TReal* mul (const TReal *x,TReal *y);
 TReal* multr (const TReal *x,TReal *y){estop();return NULL;}
 void lubksbp(TReal *b) const;
};

template <class TReal>
class matr_com_svd:public matrix<TReal> 
{
// singular value decomposition
  using matrix<TReal>::Dim;
 int Size;
 TReal *u,*w,*v,*rv1;
 void free_all(){delete [] u;delete [] w,delete [] v;delete [] rv1;u=w=v=rv1=NULL;}
 void alloc()
 {
  u=new TReal[Size*Size];d_mem(u);
  w=new TReal[Size];     d_mem(w);
  v=new TReal[Size*Size];d_mem(v);
  rv1=new TReal[Size];   d_mem(rv1);
 }
 void svbksb(const TReal * b,TReal *x) const;
 void svdcmp();
 virtual TReal& operator()(int i_row,int i_col){throw std::exception("operator()(i,j) is not implemented");}
public:
 matr_com_svd(int x){Size=Dim=x;alloc();}
 matr_com_svd(matr_com_svd<TReal>& el);
 matr_com_svd(matr_com<TReal>& el);
 ~matr_com_svd(){free_all();}
 void update(matr_com<TReal>& el);
 int max_size()const{return Size;}
 TReal* addmul (const TReal *x,TReal *y);
 TReal* addmultr (const TReal *x,TReal *y);
 TReal* mul (const TReal *x,TReal *y=NULL);
 TReal* multr (const TReal *x,TReal *y=NULL);
 void regul(TReal eps=1e-6);
#ifndef __MAKECINT__
 void pt(std::ostream& s);
#endif
};

#ifndef __MAKECINT__
template <class TReal>
inline std::ostream& operator<<(std::ostream& s,matr_comns<TReal>& el){el.pt(s);return s;}

template <class TReal>
inline std::ostream& operator<<(std::ostream& s,matr_com_lu<TReal>& el){el.pt(s);return s;}
#endif

//inline std::ostream& operator<<(std::ostream& s,matr_com_svd<TReal>& el){el.pt(s);return s;}

template <class TReal> int invert(matr_com_lu<TReal>& lu,matr_com<TReal>& m);

template <class TReal>
class eigensystem
{// calculate eigensystem<TReal> for symmetric matrix<TReal>
 TReal *b,*z__,*a;
 int nrot,nmax,nn;
 int jacobi(int n,TReal * a);
 void alloc()
 {
  a=new TReal[nmax*nmax]; d_mem(a);  
  d=new TReal[nmax+1];    d_mem(d);  
  v=new TReal[nmax*nmax]; d_mem(v);  
  b=new TReal[nmax+1];    d_mem(b);  
  z__=new TReal[nmax+1];  d_mem(z__);
 }
 void bac(int n)
 {
    v+=1+n;
    ++d;
    a+=1+n;
 }
public:
 TReal *d,*v;// eigenvalues+ eigenvectors
 eigensystem(int nv):nrot(0),nmax(nv),nn(0){alloc();}
 eigensystem(matr_com<TReal>& ma):nrot(0),nmax(ma.dim()),nn(0){alloc();(*this)(ma);}
 ~eigensystem()
 {
  delete [] a;
  delete [] d;
  delete [] v;
  delete [] b;
  delete [] z__;
 }
 int operator()(int n,TReal * a){return n>nmax?0:jacobi(nn=n,a),1;}
 int operator()(matr_com<TReal>& ma){return ma.dim()>nmax?0:jacobi(nn=ma.dim(),ma[0]),1;}
 TReal* operator[](int m){return v+m*nn;}
};

#ifdef __MAKECINT__
template class matr_comns_it<float>;
template class matr_comns_it<double>;
template class matr_comns<double>;
template class matr_comns<float>;
template class matr_com<float>;
template class matr_com<double>;
template class matr_com_lu<float>;
template class matr_com_lu<double>;
template class matr_com_luo<float>;
template class matr_com_luo<double>;
template class matr_com_svd<float>;
template class matr_com_svd<double>;
template class matr_com_lux<float>;
template class matr_com_lux<double>;
template class eigensystem<float>;
template class eigensystem<double>;
#endif


#endif