#pragma once
#include <matrix3_2/inc/int_bsearch.h>

template<class T>
class hspline3_ref
{
public:
 T x0,h;
 T *t;
 T *f;
 T *df;
 int npoint;
 int index;
 T x;
 inline void set_point(T X)
 {
   index = int_bsearch(t,t+npoint);
   x = X-t[index];
 }
 inline T interpolation()
 {
   T f0  = f[index];
   T f1  = f[index+1];
   T df0 = df[index];
   T df1 = df[index+1];
   T df = f0 - f1;
   return f0 + x*(df0 - x*(3*df + (2*df0 + df1) - x*(2*df + df0 + df1)));
 }
 inline T operator()(T X)
 {
   set_point(X);
   return interpolation();
 }
 T begin() const {return t[0];}
 T end() const {return t[npoint];}
};

