#ifndef ran_f_h
#define ran_f_h
#include <stdlib.h>
inline double random_e(){return (double)rand()/32768;}
inline double random(double a,double b){return (double)rand()/32768*(b-a)+a;}
double gasdev();
//! генератор случайных чисел с нормальным распределением.
class gauss
{
 double a,b;
public:
 gauss(){b=1;a=1;}
 gauss(double x0,double sigma){b=x0;a=sigma;}
 void set(double m,double d){b=m;a=d;}
 inline double operator()(){return a*gasdev()+b;}
};
#endif
