#ifndef sqr_h
#define sqr_h
//! квадрат
template <class T>
inline T sqr(T v){return v*v;}
//! куб
template <class T>
inline T sq3(T v){return v*v*v;}
//! v^4;
template <class T>
inline T sq4(T v){return v*v*v*v;}
#endif
