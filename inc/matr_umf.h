#ifndef matr_umf_h
#define matr_umf_h

#include <matrix3_2/inc/matrix.h>
#include <map>
#include <vector>
#include <matrix3_2/inc/int_bsearch.h>
#include <string>
#include <iostream>
#include <iomanip>



//#include <complex>

//using namespace std;

namespace SolveMethod_UMF
{
enum {LU,LU_R,UTU,SVD,QR};
}

struct Tcrf_accel
{
    bool is_ready;
    bool finished;
    int iX,iY;
    int curr_p;
    std::vector<int> posbuf;
    Tcrf_accel();
    Tcrf_accel& operator+=(int p);
    Tcrf_accel& operator++();
    Tcrf_accel& operator()(int y, int x);
    operator int();
    void start();
    void finish();
};

template <class TReal>
class matr_umf:public matrix<TReal>
{
public:
 typedef Tcrf_accel crf_accel_t;
 using matrix<TReal>::Dim;
 std::vector<int> Ap,Ai;
 std::vector<TReal> Ax;
 std::map<int,std::map<int,double> > first_matrix;
 matr_umf(int Dim_):matrix<TReal>(Dim_),matr_filled(0),print_mode(0) 
 {
   Ap.resize(Dim+1);
 }
 bool matr_filled;
 virtual TReal& operator()(int i_row,int i_col);
 virtual TReal  operator()(int i_row,int i_col)const;
 virtual TReal& operator()(Tcrf_accel&);
// virtual TReal  operator()(Tcrf_accel&)const;
 TReal get_el(int i,int j)const{return operator()(i,j);}
 void set_el(int i, int j, TReal x){operator()(i,j)=x;}
 void to_crf();
 void clear();
 bool has_row(int i){return first_matrix.find(i) != first_matrix.end();}
 int print_mode;
 std::string repr()const;
 matr_umf<TReal>& operator*=(TReal d);
  bool chknan(int i_row,int i_col)const
  {
   return false;
   if(matr_filled)
   {
    int Api = Ap[i_row];
    int len = Ap[i_row+1]-Api;
    int pos = int_bsearch(len+1, (Ai.data()+Api), i_col);
    return isnan(Ax[pos]);
   }
   else
   {
     return false;
   }
  }
  bool chknan() const
  {
   if(matr_filled)
   {
     for(auto el: Ax)
     {
       if(isnan(el))
        return true;
     }
   }
   return false;
  }
};


template <class TReal>
class matr_umf_lu:public matrix<TReal> {// common lu decomposition and backsubstitution
protected:
  using matrix<TReal>::Dim;
  using matrix<TReal>::Dimo;
  using matrix<TReal>::dim;
  using matrix<TReal>::dimo;
  /*static void (*umfpack_symbolic_t)(int, int, const int*, const int*, const TReal*, void**, const TReal*, TReal*) ;
  static void (*umfpack_numeric_t) (const int*, const int*, const TReal*, void*, void**, const TReal*, TReal*) ;
  static void (*umfpack_solve_t)(int, const int*, const int*, const TReal*, TReal*, const TReal*, void *, const TReal*, TReal*) ;
  static void (*umfpack_free_symbolic_t)(void**) ;
  static void (*umfpack_free_numeric_t) (void**) ;*/
  void* Numeric;
  void* Symbolic;
  void* Matr; //Для проверки валидности матрицы
  TReal& operator()(int i, int j);
public:
 typedef matr_umf<TReal> matr_t;
 matr_umf_lu(int x);
 ~matr_umf_lu();
 void update(matr_t& ma);
 void clear();
 TReal* mul(const TReal*, TReal*);
 void mul_py(long, long);
};

#endif