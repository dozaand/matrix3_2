#ifndef matrix_h
#define matrix_h
#include <string>
#include <iostream>
#include <matrix3_2/inc/mtr_err.h>
#include <matrix3_2/inc/vectr.h>

template <class TReal>
class operat
{// nonsymmetric operator
protected:
 void estop() const
 {
 std::cout<<"not implemented\n";
 }
public:
 TReal* mul(Vect<TReal>& x, Vect<TReal>& out)
 {
  dbg_tst(x.dim()!=dim());
  dbg_tst(out.dim()!=dimo());
  return mul(x.data(),out.data());
 }
 TReal* multr (Vect<TReal>& x, Vect<TReal>& out)
 {
  dbg_tst(x.dim()!=dimo());
  dbg_tst(out.dim()!=dim());
  return multr(x.data(),out.data());
 }
 TReal* addmul (Vect<TReal>& x,Vect<TReal>& out)
 {
  dbg_tst(x.dim()!=dim());
  dbg_tst(out.dim()!=dimo());
  return addmul(x.data(),out.data());
 }
 TReal* addmultr (Vect<TReal>& x,Vect<TReal>& out)
 {
  dbg_tst(x.dim()!=dimo());
  dbg_tst(out.dim()!=dim());
  return addmultr(x.data(),out.data());
 }
 virtual TReal* mul (const TReal* x,TReal* out) {estop();return out;}
 virtual TReal* multr (const TReal* x,TReal* out) {estop();return out;}
 virtual TReal* addmul (const TReal* x,TReal* out) {estop();return out;}
 virtual TReal* addmultr (const TReal* x,TReal* out) {estop();return out;}
 virtual int dim() const=0;
 virtual int dimo() const {return dim();}
 virtual ~operat(){}
};

template <class TReal>
class op_tran:public operat<TReal> {
 operat<TReal> * op;
public:
 op_tran(operat<TReal>* opo):op(opo){}
 TReal* mul(const TReal* x,TReal* out) {return op->multr(x,out);}
 TReal* multr(const TReal* x,TReal* out) {return op->mul(x,out);}
 TReal* addmul(const TReal* x,TReal* out) {return op->addmultr(x,out);}
 TReal* addmultr(const TReal* x,TReal* out) {return op->addmul(x,out);}
 virtual int dim() const {return op->dimo();}
 virtual int dimo() const {return op->dim();}
};

template <class TReal>
class oper_iter:public operat<TReal> {// element iterator
protected:
// static TReal tmpval;
 int pos,count;
 void mk_count();
public:
 void testran();
 virtual oper_iter<TReal>& operator=(int i)=0;// set position
// virtual oper_iter<TReal>& operator=(oper_iter<TReal>& elm)=0;// set position
 virtual int operator++(int){return ++pos;}// go to next position
 virtual int operator==(oper_iter<TReal>& elm){return pos==elm.pos;}
 virtual int operator()()=0;// test position to be in range
 virtual TReal& getd()=0;// get data pointer
 virtual int geti()=0;  // corespondent row and column indexes
 virtual int getj()=0;
 virtual int dim() const =0;
 virtual int dimo() const =0;
 int Count() const{return count;}
 TReal* mul (const TReal* x,TReal* out);
 TReal* multr (const TReal* x,TReal* out);
 TReal* addmul (const TReal* x,TReal* out);
 TReal* addmultr (const TReal* x,TReal* out);
};

template <class TReal>
class matr_pro2:public operat<TReal> {
// prpduct of 2 opers o1*o2*x
 operat<TReal> *o1,*o2;
 TReal * bufer;
 int Dim;
public:
 matr_pro2(operat<TReal> *eo1,operat<TReal> *eo2):o1(eo1),o2(eo2)
 {
  dbg_tst(o1->dim()!=o2->dimo());
  Dim=o2->dim();
  bufer=new TReal[o2->dimo()];d_mem(bufer);
 }
 ~matr_pro2(){delete [] bufer;}
 virtual int dim() const {return o2->dim();}
 virtual int dimo() const {return o1->dimo();}
 TReal* mul (const TReal* x,TReal* out)
 {
  o2->mul(x,bufer);
  return o1->mul(bufer,out);
 }
};

template <class TReal>
class matr_summ:public operat<TReal> {
//summ of 2 opers (o1+o2)*x
 operat<TReal> *o1,*o2;
 TReal * bufer;
 int Dim;
public:
 matr_summ(operat<TReal> *eo1,operat<TReal> *eo2):o1(eo1),o2(eo2)
 {
  dbg_tst(o1->dim()!=o2->dim());
  Dim=o2->dim();
  bufer=new TReal[o2->dimo()];d_mem(bufer);
 }
 ~matr_summ(){delete [] bufer;}
 virtual int dimo()const {return o1->dimo();}
 virtual int dim()const {return o1->dim();}
 TReal* mul (const TReal* x,TReal* out)
 {
  o2->mul(x,out);
  return o1->addmul(x,out);
 }
 TReal* addmul (const TReal* x,TReal* out)
 {
  o2->addmul(x,out);
  return o1->addmul(x,out);
 }
 TReal* multr (const TReal* x,TReal* out)
 {
  o2->multr(x,out);
  return o1->addmultr(x,out);
 }
 TReal* addmultr (const TReal* x,TReal* out)
 {
  o2->addmultr(x,out);
  return o1->addmultr(x,out);
 }
};

template <class TReal>
class matrixns:public operat<TReal> 
{// non sqvear marix base
public:
 int Dim,Dimo;
 matrixns(){Dimo=Dim=0;}
 matrixns(int d,int d1):Dim(d),Dimo(d1){}
 virtual TReal& operator()(int i_row,int i_col)=0;
 virtual int dim() const {return Dim;}
 virtual int dimo() const {return Dimo;}
};

template <class TReal>
class matrix:public matrixns<TReal> 
{// only for type check's
public:
 matrix(){}
 matrix(int d):matrixns<TReal>(d,d){}
};

template <class TReal>
class Ematrix: public matrix<TReal>
{
  using matrix<TReal>::Dim;
public:
  Ematrix():matrix<TReal>(){}
  Ematrix(int d):matrix<TReal>(d){}
  TReal* mul(const TReal *x, TReal *y) {memcpy(y,x,Dim*sizeof(TReal));return y;}
  TReal* multr(const TReal *x, TReal *y) {return mul(x,y);}
  TReal* addmul(const TReal *x, TReal *y){int i;for(i=0;i<Dim;i++)y[i]+=x[i];return y;}
  TReal* addmultr(const TReal *x, TReal *y){return addmul(x,y);}
};

template <class TReal>
class jack:public oper_iter<TReal>
{
//jack<TReal> -storage for indexes i,j but simple matrix<TReal> too
// friend class jack_it<TReal>;
 TReal tmpval;
protected:
public:
 int Dim,Dimo;
 int dimx,dimy,ntot,max_ntot;
 int *ii,*jj,pos;
 jack(int Ntot)
 {
  ii=new int[Ntot];d_mem(ii);
  jj=new int[Ntot];d_mem(jj);
  Dimo=Dim=dimx=dimy=ntot=pos=0;max_ntot=Ntot;
 }
 ~jack(){delete [] ii;delete [] jj;}
 void flush(){dimx=dimy=ntot=pos=0;}
 int i(){return ii[pos];}
 int j(){return jj[pos];}
 oper_iter<TReal>& operator=(int i)
 {
  dbg_tst(i<0||i>ntot);
  pos=i;
  return *this;
 }
 int operator()(){return pos<ntot;}
 int operator++(int){return pos++;}
 void add(int j,int i)
 {
  dbg_tst(pos==max_ntot);
  ii[pos]=i;jj[pos]=j;
  if(i>=dimx){dimx=i+1;}
  if(j>=dimy){dimy=j+1;}
  if(Dim<dimx){Dim=dimx;}
  if(Dimo<dimy){Dimo=dimy;}
  pos++;ntot++;
 }
 int count(){return ntot;}
 TReal* mul (const TReal* x,TReal* out);
 TReal* multr (const TReal* x,TReal* out);
 TReal* addmul (const TReal* x,TReal* out);
 TReal* addmultr (const TReal* x,TReal* out);
 int geti(){return ii[pos];}
 int getj(){return jj[pos];}
 TReal& getd(){return tmpval=1;}
#ifndef __MAKECINT__
 void rd(std::istream& s);
 void pt(std::ostream& s);
#endif
 void correct();
 virtual int dim() const {return Dim;}
 virtual int dimo() const {return Dimo;}
};
//inline ostream& operator<<(ostream& s,jack<TReal>& el){el.pt(s);return s;}
//inline istream& operator>>(istream& s,jack<TReal>& el){el.rd(s);return s;}


template <class TReal>
class jack_it:public oper_iter<TReal> 
{// element iterator
  using oper_iter<TReal>::pos;
 TReal tmpd;
 jack<TReal> *el;
public:
 jack_it(jack<TReal>* elm){pos=0;el=elm;}
 int operator()(){return pos<el->ntot;}// test position to be in range
 oper_iter<TReal>& operator=(int i){pos=i;return *this;}// set position
 oper_iter<TReal>& operator=(jack_it<TReal>& elm){memcpy(this,&elm,sizeof(jack_it));return *this;};// set position
 TReal& getd(){tmpd=TReal(pos);return tmpd;}// get data pointer
 int geti(){return el->ii[pos];}  // corespondent row and column indexes
 int getj(){return el->jj[pos];}
 virtual int dim() const {return el->dim();}
 virtual int dimo() const {return el->dimo();}
};

template <class TReal>
class jackd:public jack<TReal> 
{//jack<TReal> whith data
  using jack<TReal>::Dim;
  using jack<TReal>::Dimo;
  using jack<TReal>::dimx;
  using jack<TReal>::dimy;
  using jack<TReal>::pos;
  using jack<TReal>::ntot;
  using jack<TReal>::max_ntot;
  using jack<TReal>::ii;
  using jack<TReal>::jj;
void add(int j,int i){cout<<"attampt make you full\n"<<i<<" "<<j;}
public:
  using jack<TReal>::flush;
 TReal *dat;
 jackd(int Ntot):jack<TReal>(Ntot)
 {
  dat=new TReal[Ntot];d_mem(dat);
 }
 ~jackd(){delete [] dat;}
 void add(int j,int i,TReal d)
 {
  jack<TReal>::add(j,i);
  dat[pos-1]=d;
 }
 TReal* mul (const TReal* x,TReal* out);
 TReal* multr (const TReal* x,TReal* out);
 TReal* addmul (const TReal* x,TReal* out);
 TReal* addmultr (const TReal* x,TReal* out);
 void copy(const jackd<TReal>& el);
 void operator*=(TReal d);
 void operator+=(TReal d);
 void shdiag(TReal d);
 TReal d(){return dat[pos];}
 TReal& getd(){return dat[pos];}
#ifndef __MAKECINT__
 void rd(std::istream& s);
 void pt(std::ostream& s);
#endif
};
template <class TReal>
inline std::ostream& operator<<(std::ostream& s,jackd<TReal>& el){el.pt(s);return s;}
template <class TReal>
inline std::istream& operator>>(std::istream& s,jackd<TReal>& el){el.rd(s);return s;}

template <class TReal>
class jackd_it:public oper_iter<TReal> {// element iterator
 jackd<TReal> *el;
  using oper_iter<TReal>::pos;
public:
 jackd_it(jackd<TReal>* elm){pos=0;el=elm;}
 int operator()(){return pos<el->ntot;}// test position to be in range
 oper_iter<TReal>& operator=(int i){pos=i;return *this;}// set position
 oper_iter<TReal>& operator=(jackd_it<TReal>& elm){memcpy(this,&elm,sizeof(jackd_it));return *this;};// set position
 TReal& getd(){return el->dat[pos];}// get data pointer
 int geti(){return el->ii[pos];}  // corespondent row and column indexes
 int getj(){return el->jj[pos];}
 virtual int dim() const {return el->dim();}
 virtual int dimo() const {return el->dimo();}
};

template <class TReal>
TReal test_mul_lu(operat<TReal>& a,operat<TReal>& alu);

template <class TReal>
int defjak(operat<TReal>& op,TReal *x0,TReal * ma,int length,TReal deviat=1.1);
template <class TReal>
void pt_matr(operat<TReal>& ma,int ib=0,int jb=0,std::ostream& s=cout,int di=8,int dj=20);

template <class TReal>
void pt_matr_t(operat<TReal>& ma,int ib=0,int jb=0,std::ostream& s=cout,int di=8,int dj=20);

// fill jk  from function or operator
template <class TReal>
void make_jack(jackd<TReal>* jk,void (*fun)(TReal *a,TReal* e),TReal *b,int dim,TReal eps=3e-3,TReal tol=0);
template <class TReal>
void make_jack(jackd<TReal>* jk,operat<TReal>* ma,TReal *b,TReal eps=3e-3,TReal tol=0);
template <class TReal>
void make_jack(jackd<TReal>* jk,operat<TReal>* ma,TReal *b,TReal *eps,TReal tol=0);
template <class TReal>
void plot_matr(operat<TReal>& ma);

#ifndef __MAKECINT__
template <class TReal>
inline std::ostream& operator<<(std::ostream& s,operat<TReal>& ma){pt_matr(ma,0,0,s);return s;}
#endif

#ifdef __MAKECINT__
template class oper_iter<double>;
template class oper_iter<float>;
template class matrix<double>;
template class matrix<float>;
template class matrixns<double>;
template class matrixns<float>;
#endif

#endif