#ifndef hspline_h
#define hspline_h

template<class T,int Nt,int Nf>
class hspline3
{
public:
 T x0,h;
 T data[Nt][Nf][2];
 typedef T FP(T t,int i);
 void operator()(T X,T* out)
 {
   int index = int((X-x0)/h);
   // make extrapolation
   if(index<0){index=0;}
   else if(index >= Nt-1){index=Nt-2;}
   T x = ((X-x0)-h*index)/h;

   for(int i=0;i<Nf;++i)
   {
    T f0  = data[index][i][0];
    T f1  = data[index+1][i][0];
    T df0 = data[index][i][1]*h;
    T df1 = data[index+1][i][1]*h;
    T df = f0 - f1;
    out[i] = f0 + x*(df0 - x*(3*df + (2*df0 + df1) - x*(2*df + df0 + df1)));
   }

 }
 //! init by function call
 void fill(T x0_,T h_,FP f,FP df)
 {
    x0=x0_;
    h=h_;
    int iT,iF;
//    T* p;
    for(iT=0;iT<Nt;++iT)
    {
     for(iF=0;iF<Nf;++iF)
     {
        data[iT][iF][0]=f(iT*h,iF);
        data[iT][iF][1]=df(iT*h,iF);
     }
    }
 }
 T begin() const {return x0;}
 T end() const {return x0+h*Nt;}
};

#endif