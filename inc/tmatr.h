#ifndef tmatr_h
#define tmatr_h
#include <fstream>
#include <iomanip>
#if defined __GNUC__ || defined __STDC__
#define _hypot
#endif
#include <math.h>
#include <matrix3_2/inc/dot.h>
//using namespace std;
template <class T>
class Tmat_set_iter
{
public:
 T* p;
 int n;
 Tmat_set_iter(T* pp,int n_):p(pp),n(n_){}
 //! присвоить еще один элемент
 Tmat_set_iter& operator,(T el)
 {
  n--;
  if(n>0)
  {
   *p=el;p++;
  }
  else
  {
    throw "some except";
  }
  return *this;
 }
};

#include <initializer_list>

template <class T,int Dimo,int Dim>
class MatNM
{
public:
 T data[Dimo*Dim];
 T* operator[](int i) const {return ((T*)data)+i*Dim;}
 T operator()(int i) const {return data[i];}
 T& operator()(int i){return data[i];}
 MatNM(T* d){memcpy(data,d,Dimo*Dim*sizeof(T));}
 template<class iterable>
 MatNM(const iterable& d){auto it = d.begin();for(int i = 0;;++i,++it){if(i==Dimo*Dim)break;if(it == d.end())break;data[i] = *it;}}
 MatNM(const std::initializer_list<T>& d){auto it = d.begin();for(int i = 0;;++i,++it){if(i==Dimo*Dim)break;if(it == d.end())break;data[i] = *it;}}
 MatNM(const MatNM<T,Dimo,Dim>& el){cpy(el.data);}
 MatNM(){memset(data,0,sizeof(data));}
 inline MatNM<T,Dimo,Dim>& cpy(const T* el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]=el[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator=(const MatNM<T,Dimo,Dim>& el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]=el.data[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator=(const T* el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]=el[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator+=(const MatNM<T,Dimo,Dim>& el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]+=el.data[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator-=(const MatNM<T,Dimo,Dim>& el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]-=el.data[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& set_all(T el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]=el;
  return *this;
 }
// inline MatNM<T,Dimo,Dim>& operator=(T el)
 inline MatNM<T,Dimo,Dim>& operator+=(T el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]+=el;
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator*=(T el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]*=el;
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator/=(T el)
 {
  T in=1./el;
  for(int i=0;i<Dim*Dimo;i++)data[i]*=in;
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator-=(T el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]+=el;
  return *this;
 }
 inline MatNM<T,Dimo,Dim> operator*(T el)
 {
  MatNM<T,Dimo,Dim> v;
  for(int i=0;i<Dim*Dimo;i++)v.data[i]=data[i]*el;
  return v;
 }
 inline MatNM<T,Dimo,Dim> operator-() const
 {
  MatNM<T,Dimo,Dim> el;
  for(int i=0;i<Dimo*Dim;i++){el.data[i]=-data[i];}
  return el;
 }
 inline MatNM<T,Dimo,Dim>& operator*=(const MatNM<T,Dimo,Dim>& el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]*=el.data[i];
  return *this;
 }
 inline MatNM<T,Dimo,Dim>& operator/=(const MatNM<T,Dimo,Dim>& el)
 {
  for(int i=0;i<Dim*Dimo;i++)data[i]/=el.data[i];
  return *this;
 }
 void operator=(T v)
 {
  int i;
  for(i=0;i<Dim*Dimo;i++)
  {
   data[i]=v;
  }
 }
// Tmat_set_iter<T> operator=(T v) {data[0]=v;return Tmat_set_iter<T>(data+1,Dim*Dimo);}
 void mul(const T* x,T* ax) const
 {
  int i,j;
  const T* p;
  T sum;
  p=data;
  for(j=0;j<Dimo;j++)
  {
   sum=0;
   for(i=0;i<Dim;i++)
   {
    sum+=p[i]*x[i];
   }
   p+=Dim;
   ax[j]=sum;
  }
 }
 void multr(const T* x,T* ax) const
 {
  int i,j;
  const T* p;
  T sum;
  for(j=0;j<Dim;j++)
  {
   p=data+j;
   sum=0;
   for(i=0;i<Dimo;i++,p+=Dim)
   {
    sum+=*p*x[i];
   }
   ax[j]=sum;
  }
 }
 
 template<class Archive>
 void serialize(Archive & ar)
 {
  ar(cereal::binary_data(data,sizeof(data)));
 }
//  template<class Archive>
//  void serialize(Archive & ar, const unsigned int version)
//  {
//   ar & data;
//  }
};

template<class T, int N>
using VecN = MatNM<T,N,1>;

/*template <class T,int Dim>
class VecN:public MatNM<T,Dim,1>
{
public:
};*/
template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator+(const MatNM<T,Dimo,Dim>& el1,const MatNM<T,Dimo,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 for(int i=0;i<Dim*Dimo;i++) m.data[i]=el1.data[i]+el2.data[i];
 return m;
}
template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator-(const MatNM<T,Dimo,Dim>& el1,const MatNM<T,Dimo,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 for(int i=0;i<Dim*Dimo;i++) m.data[i]=el1.data[i]-el2.data[i];
 return m;
}
template <class T,int Dim,int Dimo,int DS>
inline MatNM<T,Dimo,Dim> operator*(const MatNM<T,Dimo,DS>& el1,const MatNM<T,DS,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 int i,j,k;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   m[i][j]=0;
   for(k=0;k<DS;k++)m[i][j]+=el1[i][k]*el2[k][j];
  }
 }
 return m;
}
// для visualc 6.0
template <class T,int Dim,int Dimo,int DS>
inline MatNM<T,Dimo,Dim> op_mul(const MatNM<T,Dimo,DS>& el1,const MatNM<T,DS,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 int i,j,k;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   m[i][j]=0;
   for(k=0;k<DS;k++)m[i][j]+=el1[i][k]*el2[k][j];
  }
 }
 return m;
}

template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator+(const MatNM<T,Dimo,Dim>& el2,T el1)
{
 MatNM<T,Dimo,Dim> m;
 for(int i=0;i<Dim*Dimo;i++) m.data[i]=el1+el2.data[i];
 return m;
}
template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator-(T el1,const MatNM<T,Dimo,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 for(int i=0;i<Dim*Dimo;i++) m.data[i]=el1-el2.data[i];
 return m;
}
template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator-(const MatNM<T,Dimo,Dim>& el2,T el1)
{
 MatNM<T,Dimo,Dim> m;
 for(int i=0;i<Dim*Dimo;i++) m.data[i]=el2.data[i]-el1;
 return m;
}
template <class T,int Dim,int Dimo>
inline MatNM<T,Dimo,Dim> operator*(T el1,const MatNM<T,Dimo,Dim>& el2)
{
 MatNM<T,Dimo,Dim> m;
 int i,j;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   m[i][j]=el1*el2[i][j];
  }
 }
 return m;
}
template <class T,int Dim,int Dimo>
inline MatNM<T,Dim,Dimo> tr(const MatNM<T,Dimo,Dim>& el)
{
 MatNM<T,Dim,Dimo> m;
 int i,j;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   m[j][i]=el[i][j];
  }
 }
 return m;
}

template <class T,int Dim>
inline MatNM<T,Dim,Dim> I_matr()
{
 MatNM<T,Dim,Dim> m;
 m.set_all(0);
 for(int i=0;i<Dim;i++)
 {
  m[i][i]=1;
 }
 return m;
}

template <class T,int Dimo,int DS,int Dim>
inline void mulAB_C(const MatNM<T,Dimo,DS>& A,const MatNM<T,DS,Dim>& B, MatNM<T,Dimo,Dim> &res)
{
 int i,j,k;
// T* p1,*p2,*p3;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   res[i][j]=0;
   for(k=0;k<DS;k++)res[i][j]+=A[i][k]*B[k][j];
  }
 }
}

template <class T,int Dimo,int DS,int Dim>
inline void mulATB_C(const MatNM<T,DS,Dimo>& A, const MatNM<T,DS,Dim>& B, MatNM<T,Dimo,Dim> &res)
{
 int i,j,k;
 for(i=0;i<Dimo;i++)
 {
  for(j=0;j<Dim;j++)
  {
   res[i][j]=0;
   for(k=0;k<DS;k++)res[i][j]+=A[k][i]*B[k][j];
  }
 }
}

template <class T,int Dimo,int DS,int Dim>
inline void mulABT_C(const MatNM<T,Dimo,DS>& A, const MatNM<T,Dimo,DS>& B, MatNM<T,Dimo,Dim> &res)
{
 int i,j,k;
 for(i=0;i<res.dimo();i++)
 {
  for(j=0;j<res.dim();j++)
  {
   res[i][j]=0;
   for(k=0;k<A.dim();k++)res[i][j]+=A[i][k]*B[j][k];
  }
 }
}

template <class T,int Dim>
class lu_s:public MatNM<T,Dim,Dim>
{
public:
 void operator()(void);
 void operator()(const MatNM<T,Dim,Dim>& A){(*this)=A;operator()();}
 void solve(MatNM<T,Dim,1>& b) const;
 inline lu_s<T,Dim>& operator=(const MatNM<T,Dim,Dim>& el){(*(MatNM<T,Dim,Dim>*)this)=el;return *this;}
};

template <class T,int Dim>
void lu_s<T,Dim>::operator()(void)
{
int i,j,k;
 for(k=0;k<Dim-1;k++)
 {
  for(i=k+1;i<Dim;i++)
  {
   if(MatNM<T,Dim,Dim>::data[k+k*Dim]==0){throw "singularyty in dcmp";}
   MatNM<T,Dim,Dim>::data[i+k*Dim]/=MatNM<T,Dim,Dim>::data[k+k*Dim];
   for(j=k+1;j<Dim;j++)
   {
    MatNM<T,Dim,Dim>::data[i+j*Dim]-=MatNM<T,Dim,Dim>::data[i+k*Dim]*MatNM<T,Dim,Dim>::data[k+j*Dim];
   }
  }
 }
}

template <class T,int Dim>
void lu_s<T,Dim>::solve(MatNM<T,Dim,1>& b) const
{
 int i,j;
 for(i=0;i<Dim;i++)
 {
  for(j=0;j<i;j++){b.MatNM<T,Dim,1>::data[i]-=b.MatNM<T,Dim,1>::data[j]*MatNM<T,Dim,1>::data[j+Dim*i];}
  b.MatNM<T,Dim,1>::data[i]/=MatNM<T,Dim,Dim>::data[i+Dim*i];
 }
 for(i=Dim-1;i>=0;i--)
 {
  for(j=i+1;j<Dim;j++)
  {
   b.MatNM<T,Dim,1>::data[i]-=b.MatNM<T,Dim,1>::data[j]*MatNM<T,Dim,Dim>::data[j+Dim*i];
  }
 }
}

template <class T,int Dim>
class lu_r:public MatNM<T,Dim,Dim>
{
 int indx[Dim];
public:
 void operator()(void);
 void operator()(const MatNM<T,Dim,Dim>& el){ (*(MatNM<T,Dim,Dim>*)this)=el;(*this)();}
 void solve(MatNM<T,Dim,1>& b) const;
 inline lu_r<T,Dim>& operator=(const MatNM<T,Dim,Dim>& el){(*(MatNM<T,Dim,Dim>*)this)=el;return *this;}
};

#define TINY 1.0e-20
template <class T,int Dim>
void lu_r<T,Dim>::operator()(void)
{
int i,imax,j,k;
T aamax,dum,sum,d;
T vv[Dim];
 d=1.;
// for(i=0;i<Dim;i++){indx[i]=i;}
 for(i=0;i<Dim;i++)
 {
  aamax=0.;
  for(j=0;j<Dim;j++){if(fabs(MatNM<T,Dim,Dim>::data[i*Dim+j])>aamax)aamax=fabs(MatNM<T,Dim,Dim>::data[i*Dim+j]);}
  if(aamax==0.){throw "singular matrix in ludcmp";}
  vv[i]=1./aamax;
 }
 for(j=0;j<Dim;j++)
 {
  for(i=0;i<j;i++)
  {
   sum=MatNM<T,Dim,Dim>::data[i*Dim+j];
   for(k=0;k<i;k++){sum-=MatNM<T,Dim,Dim>::data[i*Dim+k]*MatNM<T,Dim,Dim>::data[k*Dim+j];}
   MatNM<T,Dim,Dim>::data[i*Dim+j]=sum;
  }
  aamax=0.;
  for(i=j;i<Dim;i++)
  {
   sum=MatNM<T,Dim,Dim>::data[i*Dim+j];
   for(k=0;k<j;k++){sum-=MatNM<T,Dim,Dim>::data[i*Dim+k]*MatNM<T,Dim,Dim>::data[k*Dim+j];}
   MatNM<T,Dim,Dim>::data[i*Dim+j]=sum;
   dum=vv[i]*fabs(sum);
   if(dum>=aamax){imax=i;aamax=dum;}
  }
  if(j!=imax)
  {
   for(k=0;k<Dim;k++)
   {
    dum=MatNM<T,Dim,Dim>::data[imax*Dim+k];
    MatNM<T,Dim,Dim>::data[imax*Dim+k]=MatNM<T,Dim,Dim>::data[j*Dim+k];
    MatNM<T,Dim,Dim>::data[j*Dim+k]=dum;
   }
   d=-d;
   vv[imax]=vv[j];
  }
  indx[j]=imax;
  if(MatNM<T,Dim,Dim>::data[j*Dim+j]==0.)MatNM<T,Dim,Dim>::data[j*Dim+j]=TINY;
  if(j!=Dim)
  {
   dum=1./MatNM<T,Dim,Dim>::data[j*Dim+j];
   for(i=j+1;i<Dim;i++){MatNM<T,Dim,Dim>::data[i*Dim+j]=MatNM<T,Dim,Dim>::data[i*Dim+j]*dum;}
  }
 }
}
#undef TINY

template <class T,int Dim>
void lu_r<T,Dim>::solve(MatNM<T,Dim,1>& b) const
{
 int i,ii,j,ll;
 T sum;
 const T *lubuf=MatNM<T,Dim,Dim>::data;
 ii=-1;
 for(i=0;i<Dim;i++)
 {
   ll=indx[i];
   sum=b.MatNM<T,Dim,1>::data[ll];
   b.MatNM<T,Dim,1>::data[ll]=b.MatNM<T,Dim,1>::data[i];
   if(ii!=-1)
   {
    for(j=ii;j<i;j++){sum-=lubuf[i*Dim+j]*b.MatNM<T,Dim,1>::data[j];}
   }
   else if(sum!=0.){ii=i;}
   b.MatNM<T,Dim,1>::data[i]=sum;
 }
 for(i=Dim-1;i>=0;i--)
 {
   sum=b.MatNM<T,Dim,1>::data[i];
   for(j=i+1;j<Dim;j++){sum-=lubuf[i*Dim+j]*b.MatNM<T,Dim,1>::data[j];}
   b.MatNM<T,Dim,1>::data[i]=sum/lubuf[i*Dim+i];
 }
}

template <class T,int Dim>
class Mat_svd:public MatNM<T,Dim,Dim>//u[Dim*Dim]
{// singular value decomposition
 int Size;
 T w[Dim];
 T v[Dim*Dim];
public:
 void operator()();
// void dcmp(const MatNM<T,Dim,Dim>& A){(*this)=A;}
 void regul(T eps=1e-6);
 void solve(MatNM<T,Dim,1>& b) const;
 inline Mat_svd<T,Dim>& operator=(const MatNM<T,Dim,Dim>& el)
 {
  int i,j;
  // transpose it
  for(j=0;j<Dim;j++)
  {
   for(i=0;i<Dim;i++){MatNM<T,Dim,Dim>::data[j+i*Dim]=el.MatNM<T,Dim,Dim>::data[i+j*Dim];}
  }
  return *this;
 }
};

template <class T,int Dim>
 void Mat_svd<T,Dim>::regul(T eps)
 {
  T ma=w[0];
  int i;
  for(i=1;i<Dim;i++){if(w[i]>ma)ma=w[i];}
  eps*=ma;
  for(i=0;i<Dim;i++){if(w[i]<eps){w[i]=0.;}}
 }

template <class T>
T pythag(T a,T b)
{
 T absa, absb, pythag_v;
 absa = fabs( a );
 absb = fabs( b );
 if( absa > absb )
 {
  pythag_v = absa*sqrt( 1. + absb*absb/(absa*absa) );
 }
 else
 {
  if( absb == 0. )
  {
   pythag_v = 0.;
  }
  else
  {
   pythag_v = absb*sqrt( 1. + absa*absa/(absb*absb) );
  }
 }
 return( pythag_v );
}

template <class T,int Dim>
void Mat_svd<T,Dim>::solve(MatNM<T,Dim,1>& b) const
{
 int i,j,jj;
 T rv1[Dim];
 T s;
 for(j=0;j<Dim;j++)
 {
  int n_j;
  n_j=Dim*j;
  s=0.;
  if(w[j]!=0.)
  {
   for(i=0;i<Dim;i++)
   {
    s+=MatNM<T,Dim,Dim>::data[n_j+i]*b.MatNM<T,Dim,1>::data[i];
   }
   s/=w[j];
  }
  rv1[j]=s;
 }
 for(j=0;j<Dim;j++)
 {
  s=0.;
  for(jj=0;jj<Dim;jj++)
  {
   s+=v[Dim*jj+j]*rv1[jj];
  }
  b.MatNM<T,Dim,1>::data[j]=s;
 }
}

inline double sign(double a,double b){if(b>=0){return fabs(a);}else{return -fabs(a);}}

template <class T,int Dim>
void Mat_svd<T,Dim>::operator()()
{
 T rv1[Dim];
 int i,its,j,jj,k,l,nm,m_i,m_j;
 T anorm,c,f,g,h,s,scale,x,y,z;
 g=0.0;
 scale=0.0;
 anorm=0.0;
 for(i=0;i<Dim;i++)
 {
  l=i+1;
  m_i=Dim*i;
  rv1[i]=scale*g;
  g=0.0;
  s=0.0;
  scale=0.0;
  if(i<Dim)
  {
   for(k=i;k<Dim;k++)
   {
    scale=scale+fabs(MatNM<T,Dim,Dim>::data[m_i+k]);
   }
   if(scale!=0.0)
   {
    for(k=i;k<Dim;k++)
    {
     MatNM<T,Dim,Dim>::data[m_i+k]=MatNM<T,Dim,Dim>::data[m_i+k]/scale;
     s+=MatNM<T,Dim,Dim>::data[m_i+k]*MatNM<T,Dim,Dim>::data[m_i+k];
    }
    f=MatNM<T,Dim,Dim>::data[m_i+i];
    g=-sign(sqrt(s),f);
    h=f*g-s;
    MatNM<T,Dim,Dim>::data[m_i+i]=f-g;
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     m_j=Dim*j;
     for(k=i;k<Dim;k++)
     {
      s+=MatNM<T,Dim,Dim>::data[m_i+k]*MatNM<T,Dim,Dim>::data[m_j+k];
     }
     f=s/h;
     for(k=i;k<Dim;k++)
     {
      MatNM<T,Dim,Dim>::data[m_j+k]+=f*MatNM<T,Dim,Dim>::data[m_i+k];
     }
    }
    for(k=i;k<Dim;k++)
    {
     MatNM<T,Dim,Dim>::data[m_i+k]=scale*MatNM<T,Dim,Dim>::data[m_i+k];
    }
   }
  }
  w[i] =scale*g;
  g=0.0;
  s=0.0;
  scale=0.0;
  if((i<Dim)&&(i!=Dim-1))
  {
   int m_k;
   for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
   {
    scale+=fabs(MatNM<T,Dim,Dim>::data[m_k]);
   }
   if(scale!=0.0)
   {
    for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
    {
     MatNM<T,Dim,Dim>::data[m_k]/=scale;
     s+=MatNM<T,Dim,Dim>::data[m_k]*MatNM<T,Dim,Dim>::data[m_k];
    }
    f=MatNM<T,Dim,Dim>::data[Dim*l+i];
    g=-sign(sqrt(s),f);
    h=f*g-s;
    MatNM<T,Dim,Dim>::data[Dim*l+i]=f-g;
    for(k=l,m_k=Dim*l;m_k<Dim*Dim;k++,m_k+=Dim)
    {
     rv1[k]=MatNM<T,Dim,Dim>::data[m_k+i]/h;
    }
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     for(m_k=Dim*l;m_k<Dim*Dim;m_k+=Dim)
     {
      s+=MatNM<T,Dim,Dim>::data[m_k+j]*MatNM<T,Dim,Dim>::data[m_k+i];
     }
     for(k=l,m_k=Dim*l;m_k<Dim*Dim;k++,m_k+=Dim)
     {
      MatNM<T,Dim,Dim>::data[m_k+j]+=s*rv1[k];
     }
    }
    for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
    {
     MatNM<T,Dim,Dim>::data[m_k]*=scale;
    }
   }
  }
  T mmem;
  mmem=fabs(w[i])+fabs(rv1[i]);
  if(anorm<mmem)anorm=mmem;
 }

 for(i=Dim-1;i>=0;i--)
 {
  if(i<Dim-1)
  {
   if(g!=0.0)
   {
    for(j=l;j<Dim;j++)
    {
     v[Dim*i+j]=(MatNM<T,Dim,Dim>::data[Dim*j+i]/MatNM<T,Dim,Dim>::data[Dim*l+i])/g;
    }
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     for(k=l;k<Dim;k++)
     {
      s+=MatNM<T,Dim,Dim>::data[Dim*k+i]*v[Dim*j+k];
     }
     for(k=l;k<Dim;k++)
     {
      v[Dim*j+k]+=s*v[Dim*i+k];
     }
    }
   }
   for(j=l;j<Dim;j++)
   {
    v[Dim*j+i]=0.0;
    v[Dim*i+j]=0.0;
   }
  }
  v[Dim*i+i]=1.0;
  g=rv1[i];
  l=i;
 }
 for(i=Dim-1;i>=0;i--)
 {
  l=i+1;
  g=w[i];
  for(j=l;j<Dim;j++)
  {
   MatNM<T,Dim,Dim>::data[Dim*j+i]=0.0;
  }
  if(g!=0.0)
  {
   g=1.0/g;
   for(j=l;j<Dim;j++)
   {
    s=0.0;
    for(k=l;k<Dim;k++)
    {
     s+=MatNM<T,Dim,Dim>::data[Dim*i+k]*MatNM<T,Dim,Dim>::data[Dim*j+k];
    }
    f=(s/MatNM<T,Dim,Dim>::data[Dim*i+i])*g;
    for(k=i;k<Dim;k++)
    {
     MatNM<T,Dim,Dim>::data[Dim*j+k]=MatNM<T,Dim,Dim>::data[Dim*j+k]+f*MatNM<T,Dim,Dim>::data[Dim*i+k];
    }
   }
   for(j=i;j<Dim;j++)
   {
    MatNM<T,Dim,Dim>::data[Dim*i+j]=MatNM<T,Dim,Dim>::data[Dim*i+j]*g;
   }
  }
  else
  {
   for(j=i;j<Dim;j++)
   {
    MatNM<T,Dim,Dim>::data[Dim*i+j]=0.0;
   }
  }
  MatNM<T,Dim,Dim>::data[Dim*i+i]=MatNM<T,Dim,Dim>::data[Dim*i+i]+1.0;
 }
 for(k=Dim-1;k>=0;k--)
 {
  for(its=1;its<=30;its++)
  {
   for(l=k;l>=0;l--)
   {
    nm=l-1;
    if((fabs(rv1[l])+anorm)==anorm)goto L_2;
    if((fabs(w[nm])+anorm)==anorm)goto L_1;
   }
   L_1:
   c=0.0;
   s=1.0;
   for(i=l;i<=k;i++)
   {
    f=s*rv1[i];
    rv1[i]=c*rv1[i];
    if((fabs(f)+anorm)==anorm)
    goto L_2;
    g=w[i];
    h=pythag(f,g);
    w[i]=h;
    h=1.0/h;
    c=g*h;
    s=-(f*h);
    for(j=0;j<Dim;j++)
    {
     y=MatNM<T,Dim,Dim>::data[Dim*nm+j];
     z=MatNM<T,Dim,Dim>::data[Dim*i+j];
     MatNM<T,Dim,Dim>::data[Dim*nm+j]=(y*c)+(z*s);
     MatNM<T,Dim,Dim>::data[Dim*i+j]=-(y*s)+(z*c);
    }
   }
   L_2:
   z=w[k];
   if(l==k)
   {
    if(z<0.0)
    {
     w[k]=-z;
     for(j=0;j<Dim;j++)
     {
      v[Dim*k+j]=-v[Dim*k+j];
     }
    }
    goto L_3;
   }
   if(its==30){throw "no convergence in svdcmp number of iterations ==30";}
   x=w[l];
   nm=k-1;
   y=w[nm];
   g=rv1[nm];
   h=rv1[k];
   f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
   g=pythag(f,1.0);
   f=((x-z)*(x+z)+h*((y/(f+sign(g,f)))-h))/x;
   c=1.0;
   s=1.0;
   for(j=l;j<=nm;j++)
   {
    i=j+1;
    g=rv1[i];
    y=w[i];
    h=s*g;
    g=c*g;
    z=pythag(f,h);
    rv1[j]=z;
    c=f/z;
    s=h/z;
    f=(x*c)+(g*s);
    g=-(x*s)+(g*c);
    h=y*s;
    y=y*c;
    for(jj=0;jj<Dim;jj++)
    {
     x=v[Dim*j+jj];
     z=v[Dim*i+jj];
     v[Dim*j+jj]=(x*c)+(z*s);
     v[Dim*i+jj]=-(x*s)+(z*c);
    }
    z=pythag(f,h);
    w[j]=z;
    if(z!=0.0)
    {
     z=1.0/z;
     c=f*z;
     s=h*z;
    }
    f=(c*g)+(s*y);
    x=-(s*g)+(c*y);
    for(jj=0;jj<Dim;jj++)
    {
     y=MatNM<T,Dim,Dim>::data[Dim*j+jj];
     z=MatNM<T,Dim,Dim>::data[Dim*i+jj];
     MatNM<T,Dim,Dim>::data[Dim*j+jj]=(y*c)+(z*s);
     MatNM<T,Dim,Dim>::data[Dim*i+jj]=-(y*s)+(z*c);
    }
   }
   rv1[l]=0.0;
   rv1[k]=f;
   w[k]=x;
  }
  L_3:;
 }
}

template <class T,int Dimo,int Dim>
class Mat_qr//a[Dim*Dim]
{// qr decomposition
 T a[Dim*Dimo],diag[Dim],gamm[Dim];
 void triangsolve(const T * QTb,T * x) const;
 void proect(T * QTb) const;
public:
 T& operator()(int no,int ni){return a[ni*Dimo+no];}
 inline Mat_qr<T,Dimo,Dim>& operator=(const MatNM<T,Dimo,Dim>& A)
 {
  int i,j;
  for(i=0;i<Dim;i++)
  {
   for(j=0;j<Dimo;j++)
   {
    a[i*Dimo+j]=A.MatNM<T,Dim,Dim>::data[i+j*Dim];
   }
  }
  return *this;
 }
 void operator()();
 void operator()(const MatNM<T,Dim,Dim>& A)
 {
  int i,j;
  for(i=0;i<Dim;i++)
  {
   for(j=0;j<Dimo;j++)
   {
    a[i*Dimo+j]=A.MatNM<T,Dim,Dim>::data[i+j*Dim];
   }
  }
  operator()();
 }
 void solve(const MatNM<T,Dimo,1>& x,MatNM<T,Dim,1>& b) const
 {
  MatNM<T,Dimo,1> tmpb;
  tmpb=x;
  proect(tmpb.MatNM<T,Dim,Dim>::data);
  triangsolve(tmpb.MatNM<T,Dim,Dim>::data,b.data);
 }
};

template <class T,int Dimo,int Dim>
void Mat_qr<T,Dimo,Dim>::operator()()
{
 int k,j,i;
 T s,*col,alfa,*col1;//col,col1 -follow diagonal
 for(k=0,col=a;k<Dim;k++,col+=Dimo+1)
 {
  s=Dot(Dimo-k,col,col);
  if(s<=0){s=0.0000001;}
  else{s=sqrt(s);}
  if(*(col)>0){s=-s;}//change sign
  gamm[k]=1./(s*(s-*col));
  // store u in lower part of matrix
  *col-=s;
  diag[k]=s;
  for(j=k+1,col1=col;j<Dim;j++)
  {
   col1+=Dimo;
   alfa=gamm[k]*Dot(Dimo-k,col,col1);
   for(i=0;i<(Dimo-k);i++)
   {
    col1[i]-=alfa*col[i];
   }
  }
 }
}
/*
template <class T,int Dimo,int Dim>
void Mat_qr<T,Dimo,Dim>::proect(const T * b,T * QTb) const
{
 int k,i;
 T alfa;
 const T *col=a,*bb=b;
// if(tmpb!=b)for(i=0;i<Dimo;i++){tmpb[i]=b[i];}
 for(k=0,bb=b;k<Dim;k++,col+=Dimo+1,bb++)
 {
  alfa=gamm[k]*Dot(Dimo-k,bb,col);
  for(i=0;i<Dimo-k;i++)
  {
   bb[i]-=alfa*col[i];
  }
 }
// if(QTb!=tmpb){for(i=0;i<Dim;i++){QTb[i]=tmpb[i];}}
 for(i=0;i<Dim;i++){QTb[i]=b[i];}
}*/

template <class T,int Dimo,int Dim>
void Mat_qr<T,Dimo,Dim>::proect(T * b) const
{
 int k,i;
 T alfa;
 const T *col=a;
 T *bb=b;
 for(k=0,bb=b;k<Dim;k++,col+=Dimo+1,bb++)
 {
  alfa=gamm[k]*Dot(Dimo-k,bb,col);
  for(i=0;i<Dimo-k;i++)
  {
   bb[i]-=alfa*col[i];
  }
 }
}

template <class T,int Dimo,int Dim>
void Mat_qr<T,Dimo,Dim>::triangsolve(const T * QTb,T * x) const
{
 int k;
 const T *dg=a+Dimo*Dim-(Dimo-Dim)-1;
 T s;
 for(k=Dim-1;k>=0;k--,dg-=Dimo+1)
 {
  s=0;
  for(int i=0;i<Dim-k-1;i++)
  {
   s+=dg[Dimo*(i+1)]*x[k+1+i];
  }
//  s=dot(Dim-k-1,dg+Dimo,Dimo,x+k+1);
  x[k]=(QTb[k]-s)/diag[k];
 }
}

/*
void mul(int nx,int ny,int ni,float * A,float * B,float * res)
{
 int i,j,k;
 for(i=0;i<ny;i++)
 {
  for(j=0;j<nx;j++)
  {
   res[i*nx+j]=0;
   for(k=0;k<ni;k++)res[i*ny+j]=A[i*ni+k]*B[k*ni+j];
  }
 }
}
*/
template <class T,int Dimo,int Dim>
std::ostream& operator<<(std::ostream& s,const MatNM<T,Dimo,Dim>& el)
{
 int i,j;
 T* p;
 for(j=0;j<Dimo;j++)
 {
  p=el[j];
  for(i=0;i<Dim;i++)s<<std::setw(15)<<p[i];
//  if(Dimo!=1){s<<std::endl;}
  s<<std::endl;
 }
  return s;
}
#ifndef _MSC_VER

template <class T,int Dim>
std::ostream& operator<<(std::ostream& s,const MatNM<T,1,Dim>& el)
{
 int i;
 for(i=0;i<Dim;i++)s<<std::setw(15)<<el.data[i];
 return s;
}
template <class T,int Dimo,int Dim>
std::istream& operator>>(std::istream& s,MatNM<T,Dimo,Dim>& el)
{
 int i,j;
 T* p;
 for(j=0;j<Dimo;j++)
 {
  p=el[j];
  for(i=0;i<Dim;i++)s>>p[i];
 }
 return s;
}
#endif

template <class T,int Dim>
class Teigensystem:public MatNM<T,Dim,Dim>
{// calculate eigensystem for symmetric matrix
 T b[Dim+1];
 int nrot,nmax,nn;
 int jacobi();
public:
 MatNM<T,Dim,Dim> v;//+ eigenvectors  -rows
 MatNM<T,Dim,1> d;// eigenvalues
 Teigensystem& operator=(MatNM<T,Dim,Dim>& ma){*((MatNM<T,Dim,Dim>*)this)=ma;return *this;}
 void operator()(MatNM<T,Dim,Dim>& ma){(*this)=ma;jacobi();}
 T* ev(int i){return v+i*Dim;}
};

template <class T> T mabs(T x){return x>=0?x:-x;}
template <class T> int mabsv(T x,T delt){T ax=mabs(x);return (ax+delt)==ax;}

template <class T,int Dim>
int Teigensystem<T,Dim>::jacobi()
{
    /* System generated locals */
    T * vr=v.MatNM<T,Dim,Dim>::data,*dr=d.MatNM<T,Dim,Dim>::data,*datar=MatNM<T,Dim,Dim>::data;
    T z__[Dim+1];
    long int a_dim1, a_offset, v_dim1, v_offset, i__1, i__2, i__3;
    T r__1, r__2, r__3, r__4;

    /* Local variables */
    T c__, g, h__;
    long int i__, j;
    T s, t,  theta, tresh;
    long int ip, iq;
    T sm, tau;

    /* Parameter adjustments */
    v_dim1 = Dim;
    v_offset = 1 + v_dim1 * 1;
    vr -= v_offset;
    --dr;
    a_dim1 = Dim;
    a_offset = 1 + a_dim1 * 1;
    datar -= a_offset;

    /* Function Body */
    for (ip = 1; ip <= Dim; ++ip) {
	for (iq = 1; iq <= Dim; ++iq) {
	    vr[ip + iq * v_dim1] = 0.;
	}
	vr[ip + ip * v_dim1] = 1.;
    }
    for (ip = 1; ip <= Dim; ++ip) {
	b[ip - 1] = datar[ip + ip * a_dim1];
	dr[ip] = b[ip - 1];
	z__[ip - 1] = 0.;
/* L13: */
    }
    nrot = 0;
    for (i__ = 1; i__ <= 50; ++i__) {
	sm = 0.;
	i__1 = Dim - 1;
	for (ip = 1; ip <= i__1; ++ip) {
	    i__2 = Dim;
	    for (iq = ip + 1; iq <= i__2; ++iq) {
		sm += mabs(datar[ip + iq * a_dim1]);
/* L14: */
	    }
/* L15: */
	}
	if (sm == 0.) {return 0;}
	if (i__ < 4) {
/* Computing 2nd power */
	    i__1 = Dim;
	    tresh = sm * .2 / (i__1 * i__1);
	} else {
	    tresh = 0.;
	}
	i__1 = Dim - 1;
	for (ip = 1; ip <= i__1; ++ip) {
	    i__2 = Dim;
	    for (iq = ip + 1; iq <= i__2; ++iq) {
		g = mabs(datar[ip + iq * a_dim1])*100.;
		if (i__>4 && mabsv(dr[ip],g) && mabsv(dr[iq],g))
                {
	          datar[ip + iq * a_dim1] = 0.;
                }
                else if (mabs(datar[ip + iq * a_dim1])> tresh)
                {
	         h__ = dr[iq] - dr[ip];
 	         if(mabsv(h__,g))
                 {
	          t = datar[ip + iq * a_dim1] / h__;
	         }
                 else
                 {
		  theta = h__ * .5 / datar[ip + iq * a_dim1];
		  r__1 = theta;
		  t = 1. / (mabs(theta) + sqrt(r__1 * r__1 + 1.));
		  if (theta < 0.){t = -t;}
	         }
/* Computing 2nd power */
		    r__1 = t;
		    c__ = 1. / sqrt(r__1 * r__1 + 1);
		    s = t * c__;
		    tau = s / (c__ + 1.);
		    h__ = t * datar[ip + iq * a_dim1];
		    z__[ip - 1] -= h__;
		    z__[iq - 1] += h__;
		    dr[ip] -= h__;
		    dr[iq] += h__;
		    datar[ip + iq * a_dim1] = 0.;
		    i__3 = ip - 1;
		    for (j = 1; j <= i__3; ++j) {
			g = datar[j + ip * a_dim1];
			h__ = datar[j + iq * a_dim1];
			datar[j + ip * a_dim1] = g - s * (h__ + g * tau);
			datar[j + iq * a_dim1] = h__ + s * (g - h__ * tau);
/* L16: */
		    }
		    i__3 = iq - 1;
		    for (j = ip + 1; j <= i__3; ++j) {
			g = datar[ip + j * a_dim1];
			h__ = datar[j + iq * a_dim1];
			datar[ip + j * a_dim1] = g - s * (h__ + g * tau);
			datar[j + iq * a_dim1] = h__ + s * (g - h__ * tau);
/* L17: */
		    }
		    i__3 = Dim;
		    for (j = iq + 1; j <= i__3; ++j) {
			g = datar[ip + j * a_dim1];
			h__ = datar[iq + j * a_dim1];
			datar[ip + j * a_dim1] = g - s * (h__ + g * tau);
			datar[iq + j * a_dim1] = h__ + s * (g - h__ * tau);
/* L18: */
		    }
		    i__3 = Dim;
		    for (j = 1; j <= i__3; ++j) {
			g = vr[j + ip * v_dim1];
			h__ = vr[j + iq * v_dim1];
			vr[j + ip * v_dim1] = g - s * (h__ + g * tau);
			vr[j + iq * v_dim1] = h__ + s * (g - h__ * tau);
/* L19: */
		    }
		    ++(nrot);
		}
/* L21: */
	    }
/* L22: */
	}
	i__1 = Dim;
	for (ip = 1; ip <= i__1; ++ip) {
	    b[ip - 1] += z__[ip - 1];
	    dr[ip] = b[ip - 1];
	    z__[ip - 1] = 0.;
/* L23: */
	}
/* L24: */
    }
    return 0;
} /* jacobi_ */

template <class TReal,int Dim>
MatNM<TReal,Dim,Dim>& ip_inv(MatNM<TReal,Dim,Dim>& m)
{// do inplace calculations of m^-1
 lu_r<TReal,Dim> lu;
 lu(m);
 int i,j;
 MatNM<TReal,Dim,1> x={0};
// x=0;
 for(i=0;i<Dim;i++)
 {
  x(i)=1;
  lu.solve(x);
  for(j=0;j<Dim;j++)
  {
   m[j][i]=x(j);
  }
  for(j=0;j<Dim;j++)x(j)=0;
//  x=0;
 }
 return m;
}

#ifndef _MSC_VER
template <class TReal>
MatNM<TReal,1,1>& ip_inv(MatNM<TReal,1,1>& m)
{// do inplace calculations of m^-1
 m(0)=1./m(0);
 return m;
}
template <class TReal>
MatNM<TReal,2,2>& ip_inv(MatNM<TReal,2,2>& m)
{// do inplace calculations of m^-1
 TReal * a=m.data,tmp;
 TReal ed=1./(a[0]*a[3]-a[1]*a[2]);
 tmp=a[0];
 a[0]=a[3]*ed;
 a[3]=tmp*ed;
 a[1]*=-ed;
 a[2]*=-ed;
 return m;
}
#endif
template <class TReal,int Dim>
MatNM<TReal,Dim,Dim> inv(MatNM<TReal,Dim,Dim>& m)
{
 MatNM<TReal,Dim,Dim> mm;
 mm=m;
 return ip_inv(mm);
}

template <class T,int Dim>
class SMat
{
public:
 enum {NT=(Dim*(1 + Dim))/2};
 T data[NT];
 T* operator[](int i) const
 {
  return ((T*)data)+((Dim-1)*i-((i-1)*i)/2);
 }
 T& operator()(int i,int j)
 {
  if(j<i) throw "i < j\n error!\n";
  return data[(Dim-1)*i-((i-1)*i)/2];
 }
 T operator()(int i) const {return data[i];}
 T& operator()(int i){return data[i];}
 inline SMat<T,Dim>& cpy(T* el)
 {
  for(int i=0;i<NT;i++)data[i]=el[i];
  return *this;
 }
 inline SMat<T,Dim>& operator=(const SMat<T,Dim>& el)
 {
  for(int i=0;i<NT;i++)data[i]=el.data[i];
  return *this;
 }
 inline SMat<T,Dim>& operator=(const MatNM<T,Dim,Dim>& el)
 {
  int i,j,k=0;
  T* p;
  for(j=0;j<Dim;j++)
  {
   p=el[j];
   for(i=j;i<Dim;i++)
   data[k++]=p[i];
  }
  return *this;
 }
 operator MatNM<T,Dim,Dim>()
 {
  MatNM<T,Dim,Dim> el;
  int i,j;
  for(j=0;j<Dim;j++)
  {
   el[j][j]=(*this)[j][j];
   for(i=j+1;i<Dim;i++)
   {
    el[j][i]=el[i][j]=(*this)[i][j];
   }
  }
  return el;
 }
 inline SMat<T,Dim>& operator+=(const SMat<T,Dim>& el)
 {
  for(int i=0;i<NT;i++)data[i]+=el.data[i];
  return *this;
 }
 inline SMat<T,Dim>& operator-=(const SMat<T,Dim>& el)
 {
  for(int i=0;i<NT;i++)data[i]-=el.data[i];
  return *this;
 }
 inline SMat<T,Dim>& operator=(T el)
 {
  for(int i=0;i<NT;i++)data[i]=el;
  return *this;
 }
 inline SMat<T,Dim>& operator+=(T el)
 {
  for(int i=0;i<NT;i++)data[i]+=el;
  return *this;
 }
 inline SMat<T,Dim>& operator*=(T el)
 {
  for(int i=0;i<NT;i++)data[i]*=el;
  return *this;
 }
 inline SMat<T,Dim>& operator-=(T el)
 {
  for(int i=0;i<NT;i++)data[i]-=el;
  return *this;
 }
 inline SMat<T,Dim> operator*(T el)
 {
  SMat<T,Dim> v;
  for(int i=0;i<NT;i++)v.data[i]=data[i]*el;
  return v;
 }
 inline SMat<T,Dim> operator-() const
 {
  SMat<T,Dim> el;
  for(int i=0;i<NT;i++){el.data[i]=-data[i];}
  return el;
 }
};

#ifndef _MSC_VER
template <class T,int Dim,int NX>
inline void mulAB_C(const SMat<T,Dim>& A,const MatNM<T,Dim,NX>& B, MatNM<T,Dim,NX> &C)
{
 int i,j,k;
 T b[Dim],a[Dim],sum,*p;
 T d,xd;
 for(k=0;k<NX;k++)
 {
    for(j=0;j<Dim;j++){b[j]=0;a[j]=A[j][k];}
    for(i=0;i<Dim;i++)
    {
      xd=a[i];
      p=A[i];
      b[i]+=xd*p[i];
      for(j=i+1;j<Dim;j++)
      {
        d=p[j];
        b[i]+=d*a[j];
        b[j]+=d*xd;
      }
    }
    for(j=0;j<Dim;j++){B[j][k]=b[j];}
 }
}
template <class T,int Dim>
inline void mulAB_C(const SMat<T,Dim>& A,const MatNM<T,Dim,1>& B, MatNM<T,Dim,1> &C)
{
 int i,j;
 const T *a=B.data;
 T *b=C.data;
 for(i=0;i<Dim;i++){b[i]=0;}
 T d,xd;
 T* p;
 for(i=0;i<Dim;i++)
 {
  xd=a[i];
  p=A[i];
  b[i]+=xd*p[i];
  for(j=i+1;j<Dim;j++)
  {
   d=p[j];
   b[i]+=d*a[j];
   b[j]+=d*xd;
  }
 }
}
#endif

template <class T,int Dim>
class SMat_lus:public SMat<T,Dim>
{
  using SMat<T,Dim>::data;
public:
 T& operator()(int i,int j)
 {
  if(j<i) throw "i < j\n error!\n";
  return data[(Dim-1)*i-((i-1)*i)/2+j];
 }
 void operator()(void)
 {
  int i,j,k;  //i - row counter, j - column counter
  T aii,s,uii,*p,*diag;
  diag=data;
  for(i=0;i<Dim;diag+=Dim-i,i++)
  {
   s=0;
   p=data+i;
   for(k=1;k<=i;p+=(Dim-k),k++)
   {
//    s+=(*this)[k-1][i]*(*this)[k-1][i];
    s+=*p* *p;
   }
//   aii=(*this)[i][i];
   *diag=sqrt(*diag-s);
   uii=1./ *diag;
//   (*this)[i][i]=uii=sqrt((*this)[i][i]-s);
   for(j=i+1;j<Dim;j++)
   {
    s=0;
    p=data;
    for(k=1;k<=i;p+=(Dim-k),k++)
    {
//     s+=(*this)[k-1][i]*(*this)[k-1][j];
     s+=p[i]*p[j];
    }
    p[j]=(p[j]-s)*uii;
//    (*this)[i][j]=((*this)[i][j]-s)/uii;
   }
  }
 }
 void operator()(const MatNM<T,Dim,Dim>& el){ (*(MatNM<T,Dim,Dim>*)this)=el;(*this)();}
 void operator()(const SMat<T,Dim>& el){ (*(SMat<T,Dim>*)this)=el;(*this)();}
 void solve(const MatNM<T,Dim,1>& c,MatNM<T,Dim,1>& rv) const
 {
  int i,k;
  T s,*p;
  for(i=0;i<Dim;i++)
  {
   s=0;
   p=(*this)[0]+i;
   for(k=0;k<=i-1;p+=(Dim-1-k),k++)
   {
    s+=*p*rv(k);
   }
   rv(i)=(c(i)-s)/ *p;
//   for(k=0;k<=i-1;k++){s+=(*this)[k][i]*rv(k);}
//   rv(i)=(c(i)-s)/(*this)[i][i];
  }
  for(i=Dim-1;i>=0;i--)
  {
   s=0;
   p=(*this)[i];
   for(k=i+1;k<Dim;k++){s+=p[k]*rv(k);}
   rv(i)=(rv(i)-s)/p[i];
  }
 }
 inline SMat_lus<T,Dim>& operator=(const MatNM<T,Dim,Dim>& el){(*(MatNM<T,Dim,Dim>*)this)=el;return *this;}
 inline SMat_lus<T,Dim>& operator=(const SMat<T,Dim>& el){(*(SMat<T,Dim>*)this)=el;return *this;}
};

template <class T,int Dim>
std::ostream& operator<<(std::ostream& s,const SMat<T,Dim>& el)
{
 int i,j;
 for(i=0;i<Dim;i++)
 {
  for(j=0;j<Dim;j++)
  {
   if(i>j)
   {
    s<<std::setw(15)<<el[j][i];
   }
   else
   {
    s<<std::setw(15)<<el[i][j];
   }
  }
  s<<std::endl;
 }
 return s;
}

template <class TReal,int N>
class TMatr3
{
public:
 TReal a[N-1];
 TReal b[N];
 TReal c[N-1];
 //multiply matrix<TReal> on vector
 void mul (const TReal *x,TReal *y)
 {
  int i;
  for(i=1;i<N-1;i++)
  {
   y[i]=x[i-1]*a[i-1]+x[i]*b[i]+x[i+1]*c[i];
  }
  y[0]=x[0]*b[0]+x[1]*c[0];
  y[i]=x[i-1]*a[i-1]+x[i]*b[i];
 }
 void multr (const TReal *x,TReal *y=NULL)
 {
  int i;
  for(i=1;i<N-1;i++)
  {
   y[i]=x[i-1]*c[i-1]+x[i]*b[i]+x[i+1]*a[i];
  }
  y[0]=x[0]*b[0]+x[1]*a[0];
  y[i]=x[i-1]*c[i-1]+x[i]*b[i];
 }
 void addmul (const TReal *x,TReal *y)
 {
  int i;
  for(i=1;i<N-1;i++)
  {
   y[i]+=x[i-1]*a[i-1]+x[i]*b[i]+x[i+1]*c[i];
  }
  y[0]+=x[0]*b[0]+x[1]*c[0];
  y[i]+=x[i-1]*a[i-1]+x[i]*b[i];
 }
 void addmultr (const TReal *x,TReal *y)
 {
  int i;
  for(i=1;i<N-1;i++)
  {
   y[i]+=x[i-1]*c[i-1]+x[i]*b[i]+x[i+1]*a[i];
  }
  y[0]+=x[0]*b[0]+x[1]*a[0];
  y[i]+=x[i-1]*c[i-1]+x[i]*b[i];

 }
 void solve(const TReal *r,TReal *u)
 {
 int j,k;
 TReal bet,psi[N];
// dbg_tst(b[0]==0.);
 bet=b[0];
 u[0]=r[0]/bet;
 for(j=1;j<N;j++)
 {
  k=j-1;
  psi[j]=c[k]/bet;
  bet=b[j]-a[k]*psi[j];
//  dbg_tst(bet==0.);// tridag failed
  u[j]=(r[j]-a[k]*u[k])/bet;
 }
 for(j=N-2;j>=0;j--){u[j]=u[j]-psi[j+1]*u[j+1];}
 }
 void solvetr(const TReal *r,TReal *u)
 {
 int j,k;
 TReal bet,psi[N];
// dbg_tst(b[0]==0.);
 bet=b[0];
 u[0]=r[0]/bet;
 for(j=1;j<N;j++)
 {
  k=j-1;
  psi[j]=a[k]/bet;
  bet=b[j]-c[k]*psi[j];
//  dbg_tst(bet==0.);// tridag failed
  u[j]=(r[j]-c[k]*u[k])/bet;
 }
 for(j=N-2;j>=0;j--){u[j]=u[j]-psi[j+1]*u[j+1];}
 }
};

template <class T,int N>
T norm(const MatNM<T,N,1>& v)
{
 return sqrt(Dot(N,v.data,v.data));
}
template <class T,int N>
T norm2(const MatNM<T,N,1>& v)
{
 return Dot(N,v.data,v.data);
}
template <class T,int N>
MatNM<T,N,1> normir(const MatNM<T,N,1>& v)
{
 MatNM<T,N,1> vv=v;
 vv*=(T)1./norm(v);
 return vv;
}
template <class T,int N>
T dot(const MatNM<T,N,1>& v1,const MatNM<T,N,1>& v2)
{
 return Dot(N,v1.data,v2.data);
}
template <class T,int N>
T dist(const MatNM<T,N,1>& v1,const MatNM<T,N,1>& v2)
{
#ifndef _MSC_VER
 return norm(v2-v1);
#else
 MatNM<T,N,1> tmp;
	tmp=v1;
	tmp-=v2;
	return norm(tmp);
#endif
}
template <class T,int N>
T dist2(const MatNM<T,N,1>& v1,const MatNM<T,N,1>& v2)
{
#ifndef _MSC_VER
 return norm2(v2-v1);
#else
 MatNM<T,N,1> tmp;
	tmp=v1;
	tmp-=v2;
	return norm2(tmp);
#endif
}

template <class T, int N, int M>
MatNM<T,N,M> Abs(MatNM<T,N,M>& A)
{
  int i,j;
  MatNM<T,N,M> b;
  for(i=0;i<N;i++)
    for(j=0;j<M;j++)
      b[i][j]=fabs(A[i][j]);
  return b;
}

template<class T,int N,int M>
T min(const MatNM<T,N,M>& el)
{
 T mi=el.data[0];
 for(int i=1;i<N*M;++i)
 {
  mi=min(el.data[i],mi);
 }
 return mi;
}
template<class T,int N,int M>
T max(const MatNM<T,N,M>& el)
{
 T mi=el.data[0];
 for(int i=1;i<N*M;++i)
 {
  mi=max(el.data[i],mi);
 }
 return mi;
}
template<class T,int N,int M>
T sum(const MatNM<T,N,M>& el)
{
 T mi=el.data[0];
 for(int i=1;i<N*M;++i)
 {
  mi+=el.data[i];
 }
 return mi;
}
template<class T,int N,int M>
T mean(const MatNM<T,N,M>& el)
{
 return sum(el)/(N*M);
}

#endif