#pragma once
#include <matrix3_2/inc/cspline.h>
#include <limits>

template <typename T>
class Tpoint_info
{
public:
    Tpoint_info():t(std::numeric_limits<T>::quiet_NaN()){}
    T t;
    Tspl_acsel<T> a;
};

//! класс для проведения интерполяции кубическими сплайнами
template <class T,class Tgrid>
class spline3_multipoint:public spline3<T,Tgrid>
{
public:
 Tpoint_info<T>* acc;
 spline3_multipoint(Tpoint_info<T>* pacc, T t0,T t1,const T *f,int N):spline3<T,Tgrid>(t0,t1,f,N),acc(pacc){}
 spline3_multipoint(Tpoint_info<T>* pacc, const T* pp,const T *f,int N):spline3<T,Tgrid>(pp,f,N),acc(pacc){}
 spline3_multipoint(Tpoint_info<T>* pacc, const T* pp,const T *f,int N,T* ext_koeff):spline3<T,Tgrid>(pp,f,N,ext_koeff),acc(pacc){}
 spline3_multipoint(Tpoint_info<T>* pacc, const T* pp,T *kk,int N,int none):spline3<T,Tgrid>(pp.kk,N),acc(pacc){}
 inline T operator()(T t) const 
 {
    //if(acc->t-0.001 >t || t> acc->t+0.001)
    if(t!=acc->t)
    {
        acc->t=t;
        acc->a=Tgrid::acsel(t);
    }
    return this->spline3<T,Tgrid>::operator()(acc->a);
 }
};
