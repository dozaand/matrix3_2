#ifndef cspline_h
#define cspline_h
#include <malloc.h>
#include <matrix3_2/inc/int_bsearch.h>
#include <stdexcept>
#include <map>
#include <string.h>
#include <limits>
#include <cmath>
#include <vector>
#include <cereal/archives/binary.hpp>

/*
enum {EX_OUT_OF_RANGE,INVALID_DATA};
class Tspline_out_of_range
{
public:
 virtual string msg()=0;
};

template <class T>
class TTspline_out_of_range_b:public Tspline_out_of_range
{
public:
 T limit;//!< предельное значение
 T x;//!< текущее значение
};

template <class T>
class Tspline_lo:public Tspline_out_of_range_b
{
public:
 T limit;//!< предельное значение
 T x;//!< текущее значение
};
*/
template <class T>
class Tspl_acsel
{
public:
 Tspl_acsel(){}
 Tspl_acsel(int i,T t,T e_h_):ipoi(i), a(t), e_h(e_h_){}
 int ipoi;//
 T a;// Доля интервала отсекаемая точкой t
 T e_h;
};


template <class T>
class spline_grid_h
{
public:
 int n;// number of points
 T h,e_h;
 T t0;// start position of grid
 spline_grid_h(T T0,T T1,int N):t0(T0){n=N;h=(T1-T0)/(n-1);e_h=1./h;}
 void set(T T0,T T1,int N){t0=T0;n=N;h=(T1-T0)/(n-1);e_h=1./h;}
 Tspl_acsel<T> acsel(T t) const //!< ускоритель расчета если известна точка расчета
 {
  T b;
  int ipoi;
  ipoi=b=(t-t0)*e_h;
  if(ipoi<0){ipoi=0;};if(ipoi>=n-1){ipoi=n-2;}
  return Tspl_acsel<T>(ipoi,b-ipoi,e_h);
 }
 T get_t(int i)const {return t0+h*i;}
 T dt(int){return h;}
};



// это способ выбора отрезка свыдачей ошибок при выходе за диапазон
template <class T>
class spline_grid_h_th
{
public:
 int n;// number of points
 T h,e_h;
 T t0;// start position of grid
 spline_grid_h_th(T T0,T T1,int N):t0(T0){n=N;h=(T1-T0)/(n-1);e_h=1./h;}
 void set(T T0,T T1,int N){t0=T0;n=N;h=(T1-T0)/(n-1);e_h=1./h;}
 Tspl_acsel<T> acsel(T t) const //!< ускоритель расчета если известна точка расчета
 {
  T b;
  int ipoi;
  ipoi=b=(t-t0)*e_h;
  if( (ipoi<0)||(ipoi>=n) )
  {
    throw std::out_of_range("spline arg");
//   throw Err_msg()<<"Value "<<t<<" is out of range "<<t0<<' '<<t0+h*n;
  };
  return Tspl_acsel<T>(ipoi,b-ipoi,e_h);
 }
 T get_t(int i)const {return t0+h*i;}
 T dt(int){return h;}
};


template <class T>
class spline_grid_v
{
public:
    int n;// number of points
    spline_grid_v(int N,const T * p){n=N;t=new T[n];memcpy(t,p,sizeof(T)*n);}
    virtual ~spline_grid_v(){if(t!=NULL)delete [] t;}
    T *t;//!< grid points times
    Tspl_acsel<T> acsel(T tx) const //!< ускоритель расчета если известна точка расчета
    {
        int ipoi=int_bsearch(t,t+n,tx)-t;
        T t2 = t[ipoi+1];
        T t1 = t[ipoi];
        T e_h=1./(t2-t1);
        return Tspl_acsel<T>(ipoi,(tx-t1)*e_h,e_h);
    }
    T get_t(int i)const {return t[i];}
    T dt(int i)const {return t[i+1]-t[i];}
    T a()const{return t[0];}
    T b()const{return t[n-1];}
};

template <class T>
class spline_grid_v_cashed: public spline_grid_v<T>
{
protected:
    typedef spline_grid_v<T> parent_t;
    mutable T t1,t2;
    mutable int ipoi_cashed; //Запомненный индекс интервала с предыдущего поиска
    mutable bool cashed; //признак того что кэширование было
public:
    using parent_t::n;
    using parent_t::t;
    spline_grid_v_cashed(int N,const T * p):spline_grid_v<T>(N,p){}
    Tspl_acsel<T> acsel(T tx) const //!< ускоритель расчета если известна точка расчета
    {
        int ipoi;
        if(cashed)
        {
            if(tx >= t1 && tx < t2)
            {
                ipoi = ipoi_cashed;
            }
            else
            {
                ipoi=int_bsearch(t,t+n,tx)-t;
                ipoi_cashed = ipoi;
                t2 = t[ipoi+1];
                t1 = t[ipoi];
            }
        }
        else
        {
            cashed = true;
            ipoi=int_bsearch(t,t+n,tx)-t;
            ipoi_cashed = ipoi;
            t2 = t[ipoi+1];
            t1 = t[ipoi];
        }
        T e_h=1./(t2-t1);
        return Tspl_acsel<T>(ipoi,(tx-t1)*e_h,e_h);
    }
};


template<class T, class int_info>
class spline_grid_v_cashed_interval: public spline_grid_v_cashed<T>
{
public:
  using spline_grid_v<T>::t;
  using spline_grid_v<T>::n;
    int_info& iinfo_cashed;
    spline_grid_v_cashed_interval(int N,const T * p,int_info& iinfo): spline_grid_v<T>(N,p), iinfo_cashed(iinfo){}
    Tspl_acsel<T> acsel(T tx) const //!< определяем ячейку сетки в которой лежит точка только если она не запомнена
    {
        int ipoi;
        if(iinfo_cashed.includes(tx))
        {
            ipoi = iinfo_cashed.index();
        }
        else
        {
            ipoi=int_bsearch(t,t+n,tx)-t;
            iinfo_cashed.update(ipoi,t[ipoi],t[ipoi+1]);
        }
        T e_h=1./(t[ipoi+1]-t[ipoi]);
        return Tspl_acsel<T>(ipoi,(tx-t[ipoi])*e_h,e_h);
    }
};


template <class T>
class spline_grid_v_ref
{
public:
 int n;// number of points
 spline_grid_v_ref(int N,const T * p):n(N),t(p){}
 const T *t;//!< grid points times
 Tspl_acsel<T> acsel(T tx) const //!< ускоритель расчета если известна точка расчета
 {
  int ipoi;
  ipoi=int_bsearch(t,t+n,tx)-t;
  T e_h;
  e_h=1./(t[ipoi+1]-t[ipoi]);
  return Tspl_acsel<T>(ipoi,(tx-t[ipoi])*e_h,e_h);
 }
 T get_t(int i)const {return t[i];}
 T dt(int i)const {return t[i+1]-t[i];}
};
template <class T>
class spline_grid_v_ref_th
{
public:
 int n;// number of points
 spline_grid_v_ref_th(int N,T * p):n(N),t(p){}
 T *t;//!< grid points times
 Tspl_acsel<T> acsel(T tx) const //!< ускоритель расчета если известна точка расчета
 {
  if(tx<t[0]||tx>t[n-1])
  {
    throw std::out_of_range("spline arg");
//   throw Err_msg()<<"Value "<<tx<<" is out of range "<<t[0]<<' '<<t[n-1];
  }
  int ipoi;
  ipoi=int_bsearch(t,t+n,tx)-t;
  T e_h;
  e_h=1./(t[ipoi+1]-t[ipoi]);
  return Tspl_acsel<T>(ipoi,(tx-t[ipoi])*e_h,e_h);
 }
 T get_t(int i)const {return t[i];}
};

#include <vector>
#include <set>
#include <algorithm>

//! класс для проведения кусочно-постоянной аппроксимации
template <class T>
class const_interpolation:public spline_grid_v<T>
{
public:
    T* data;
    const_interpolation(const T* px, const T* py, int nx): spline_grid_v<T>(nx,px)
    {
        data = nullptr;
        if(nx>1)
        {
            data=new T[nx-1];
            memcpy(data,py,(nx-1)*sizeof(T));
        }
    }
    inline T operator()(const Tspl_acsel<T>& ac)
    {
        return *(data+ac.ipoi);
    }
    inline T operator()(T x){return (*this)(acsel(x));}
    virtual ~const_interpolation()
    {
        delete[]data;
    }
    struct constructor
    {
        struct Txy
        {
            T x;
            mutable T y;
            Txy(T x_):x(x_),y(0){}
            bool operator < (const Txy& el)const{return x<el.x;}
            void addv(T val)const{y+=val;}
        };
        mutable std::vector<T> X;
        mutable std::vector<T> Y;
        std::set<Txy> Xset;
        inline void add(T x1, T x2, T y)
        {
            auto place1 = Xset.insert(Txy(x1));
            auto place2 = Xset.insert(Txy(x2));
            if(place1.second)
            {
                auto ii=place1.first;
                if(ii!=Xset.begin())
                {
                    place1.first->addv((ii--)->y);
                }
            }
            for(auto i = place1.first;i != place2.first; ++i)
            {
                i->addv(y);
            }
        }
        inline void add1(T x1, T x2, T y)
        {
            using namespace std;
            if(x1>=x2)throw "const_interpolation constructor: invalid args!\n";
            if(X.size()==0)
            {
                X.push_back(x1);
                X.push_back(x2);
                Y.push_back(y);
            }
            else
            {
                int n=X.size()+1;
                int i1 = int_bsearch(n,X,x1);
                int i2 = int_bsearch(n,X,x2);
                if(x1>=X.front() && x2<X.back())
                {
                    X.insert(X.begin()+i1+1,x1);
                    T yy=Y[i1];
                    Y.insert(Y.begin()+i1,yy);
                    X.insert(X.begin()+i2+2,x2);
                    yy=Y[i2];
                    Y.insert(Y.begin()+i2+1,yy);
                    for(int i=i1+1;i<i2+2;i++)
                    {
                        Y[i]+=y;
                    }
                }
                else if(x1<X.back())
                {
                    X.insert(X.begin()+i1,x1);
                    T yy=Y[i1];
                    Y.insert(Y.begin()+i1,yy);
                    X.push_back(x2);
                    Y.push_back(0);
                    for(int i=i1+1;i<Y.size();i++)
                    {
                        Y[i]+=y;
                    }
                }
                else
                {
                    X.push_back(x1);
                    X.push_back(x2);
                    Y.push_back(0);
                    Y.push_back(y);
                }
            }
        }
        inline const_interpolation<T>* operator()()const
        {
            X.clear();
            Y.clear();
            for(auto i=Xset.begin();i!=Xset.end();++i)
            {
                X.push_back(i->x);
                Y.push_back(i->y);
            }
            return empty()?nullptr:new const_interpolation<T>(&(X[0]),&(Y[0]),X.size());
        }
        inline void clear(){X.clear();Y.clear();}
        inline bool empty()const{return Y.size()<1;}
    };
};


//! класс для проведения линейной интерполяции
template <class T,class Tgrid>
class lin_int:public Tgrid
{
 T * data;
 using Tgrid::acsel;
public:
 lin_int(T t0,T t1,const T *f,int N):Tgrid(t0,t1,N)//spline_grid_h<T>
 {
  data= new T[N];
  memcpy(data,f,N*sizeof(T));
 }
 lin_int(const T* pp, const T *f,int N):Tgrid(N,pp)//spline_grid_v<T>
 {
  data= new T[N];
  memcpy(data,f,N*sizeof(T));
 }
 ~lin_int(){delete [] data;}
 inline T operator()(const Tspl_acsel<T>& ac) const
 {
  T* k=data+ac.ipoi;
  return (1-ac.a)*k[0]+ac.a*k[1];
 }
 inline T operator()(T t) const {return (*this)(acsel(t));}
};

template <class T,class Tgrid>
class lin_int_ref:public Tgrid
{
public:
 const T * data;
 lin_int_ref(T t0,T t1,const T *f,int N):Tgrid(t0,t1,N),data(f){}
 lin_int_ref(const T* pp,const T *f,int N):Tgrid(N,pp),data(f){}
 ~lin_int_ref(){}
 inline T operator()(const Tspl_acsel<T>& ac) const
 {
  const T* k=data+ac.ipoi;
  return (1-ac.a)*k[0]+ac.a*k[1];
 }
 inline T operator()(T t) const {return (*this)(acsel(t));}
// friend T integral_pow2(const lin_int_ref<T,Tgrid >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1);
};


template <class TT,class TV>
class lin_int_ref_map
{
 const std::map<TT,TV>* data;
public:
 int exceptons;
 lin_int_ref_map(const std::map<TT,TV>& d):data(&d){}
 ~lin_int_ref_map(){}
 inline TV operator()(TT t) const
 {
  TT k;
  if(data->size()<=1)
  {
   throw "invalid data";
  }
  typename std::map<TT,TV>::const_iterator it1,it2;
//  t=90;
  it1=data->upper_bound(t);
  if(it1==data->begin())
  {
  // вне диапазона снизу
    throw std::out_of_range("out of lower bound");
   it2=it1;
   ++it2;
  }
  else if(it1==data->end())
  {
  // вне диапазона сверху
    throw std::out_of_range("out of upper bound");
   it2=it1=data->end();
   --it1;--it1;
   --it2;
  }
  else
  {
   it2=it1;
   --it1;
  // внутри диапазона
  }
  double x1,x2,v2,v1;
  x1=(*it1).first;
  x2=(*it2).first;
  v1=(*it1).second;
  v2=(*it2).second;
  k=(t-(*it1).first)/((*it2).first-(*it1).first);
  return (*it1).second*(1-k)+(*it2).second*k;
 }
};

template <class T,class Tgrid>
class lin_int_nd:public Tgrid
{
 T * data;
public:
 lin_int_nd(T t0,T t1,const T *f,int N):Tgrid(t0,t1,N)//spline_grid_h<T>
 {data=f;}
 lin_int_nd(T* pp,T *f,int N):Tgrid(N,pp)//spline_grid_v<T>
 {data=f;}
 inline T operator()(const Tspl_acsel<T>& ac) const
 {
  T* k=data+ac.ipoi;
  return (1-ac.a)*k[0]+ac.a*k[1];
 }
 inline T operator()(T t) const {return (*this)(acsel(t));}
};

#define Qnan(T) std::numeric_limits<T>::quiet_NaN()
template <class T>
void slv3d(const T* a,const T* b,const T* c,const T* r,T* u,int n)// solve 3d matr push result to D
{// x[j+1]=A[j]-B[j]*x[0]
 int j,k;
 T bet,*psi;
 psi=(T*)alloca(sizeof(T)*n);
 bet=b[0];
 u[0]=r[0]/bet;
 for(j=1;j<n;j++)
 {
  k=j-1;
  psi[j]=c[k]/bet;
  bet=b[j]-a[j]*psi[j];
///  dbg_tst(bet==0.);// tridag failed
  u[j]=(r[j]-a[j]*u[k])/bet;
 }
 for(j=n-2;j>=0;j--){u[j]=u[j]-psi[j+1]*u[j+1];}
}

template <typename T>
std::vector<T> f2vector(T (*f)(T),T t0,T t1,int N)
{
    std::vector<T> ff(N);
    T delta=(t1-t0)/(N-1),x;
    x=t0;
    for(int i=0;i<N;++i,x+=delta)
    {
       ff[i]=(*f)(x);
    }
    return ff;
}
//! класс для проведения интерполяции кубическими сплайнами
template <class T,class Tgrid>
class spline3:public Tgrid
{
 T *A,*B,*C,*D,*P;
 void fillkoeff(const T* f,const T* p)
 {
  T * k=koeff;
  int j;
  for(j=0;j<Tgrid::n-1;j++,k+=4)
  {
   T pj=p[j]*Tgrid::dt(j),pj1=p[j+1]*Tgrid::dt(j);
   k[0]=f[j];
   k[1]=pj;
   k[2]=3*(f[j+1]-f[j])-2*pj-pj1;
   k[3]=2*(f[j]-f[j+1])+pj+pj1;
  }
 }
 void expandA(int N)
 {
    B=A+N;
    C=B+N;
    D=C+N;
    P=D+N;
 }
 int nmax;
public:
 int selfkoeff;
 T * koeff;
 int update(int np,const T *f,T* df)
 {
  if(np>nmax)return 0;
  Tgrid::n=np;
  fillkoeff(f,df);
  return 1;
 }
 //! сплайн по границам интервала значениям функции в узлах, количеству значений функций
 spline3(T t0,T t1,const T *f,int N,T dxa=Qnan(T),T dxb=Qnan(T)):Tgrid(t0,t1,N)//spline_grid_h<T>
 {
  int i;
  nmax=N;
  A = (T*)alloca(sizeof(T)*N*5);
  expandA(N);
  koeff=new T[(N-1)*4];
  selfkoeff=1;
  A[N-1]=1;B[N-1]=2;C[N-1]=1;D[N-1]=3*(f[N-1]-f[N-2])*Tgrid::e_h;
  for(i=1;i<N-1;i++)
  {
   A[i]=1;
   B[i]=4;
   C[i]=1;
   D[i]=3*(f[i+1]-f[i-1])*Tgrid::e_h;
  }
  if(std::isnan(dxa))
  {
      slv3d(A, B, C, D, P, N);
      fillkoeff(f, P);
  }
  else
  {
      D[1] -= A[0] * dxa;
      D[N - 2] -= C[N - 2] * dxb;
      slv3d(A + 1, B + 1, C + 1, D + 1, P + 1, N - 2);
      fillkoeff(f, P);
      P[0] = dxa;
      P[N - 1] = dxb;
  }
 }
 //! сплайн по значениям x,f(x) и количеству этих значений
 spline3(const T* pp,const T *f,int N,T dxa=Qnan(T),T dxb=Qnan(T)):Tgrid(N,pp)//spline_grid_v<T>
 {
  A = (T*)alloca(sizeof(T)*N*5);
  expandA(N);
  koeff=new T[N*4];
  selfkoeff=1;
  const T* t=Tgrid::t;
  int i;
  A[0]=0;B[0]=2*(t[1]-t[0]);C[0]=(t[1]-t[0]);D[0]=3*(f[1]-f[0]);
  A[N-1]=(t[N-1]-t[N-2]);B[N-1]=2*(t[N-1]-t[N-2]);C[N-1]=0;D[N-1]=3*(f[N-1]-f[N-2]);
  for(i=1;i<N-1;i++)
  {
   A[i]=t[i]-t[i-1];
   B[i]=2*(t[i+1]-t[i-1]);
   C[i]=t[i+1]-t[i];
   D[i]=3*(f[i+1]-f[i-1]);
  }
  slv3d(A,B,C,D,P,Tgrid::n);
  fillkoeff(f,P);
 }
 //! сплайн по значениям x,f(x), количеству этих значений и по кэшированному интервалу
 template<class Tci>
 spline3(const T* pp,const T *f,int N,Tci& ci,T dxa=Qnan(T),T dxb=Qnan(T)):Tgrid(N,pp,ci)//spline_grid_v_cashed_interval<T,Tci>
 {
  A = (T*)alloca(sizeof(T)*N*5);
  expandA(N);
  koeff=new T[N*4];
  selfkoeff=1;
  T* t=Tgrid::t;
  int i;
  A[0]=0;B[0]=2*(t[1]-t[0]);C[0]=(t[1]-t[0]);D[0]=3*(f[1]-f[0]);
  A[N-1]=(t[N-1]-t[N-2]);B[N-1]=2*(t[N-1]-t[N-2]);C[N-1]=0;D[N-1]=3*(f[N-1]-f[N-2]);
  for(i=1;i<N-1;i++)
  {
   A[i]=t[i]-t[i-1];
   B[i]=2*(t[i+1]-t[i-1]);
   C[i]=t[i+1]-t[i];
   D[i]=3*(f[i+1]-f[i-1]);
  }
  slv3d(A,B,C,D,P,Tgrid::n);
  fillkoeff(f,P);
 }
 //! сплайн по значениям x,f(x) и количеству этих значений. Коэффициенты сплайна храним во внешнем хранилище ext_koeff
 spline3(const T* pp,const T *f,int N,T* ext_koeff):Tgrid(N,pp)//spline_grid_v<T>
 {
//  koeff=new T[N*4];
  A = (T*)alloca(sizeof(T)*N*5);
  expandA(N);
  koeff=ext_koeff;
  selfkoeff=0;
  const T* t=Tgrid::t;
  int i;
  A[0]=0;B[0]=2*(t[1]-t[0]);C[0]=(t[1]-t[0]);D[0]=3*(f[1]-f[0]);
  A[N-1]=(t[N-1]-t[N-2]);B[N-1]=2*(t[N-1]-t[N-2]);C[N-1]=0;D[N-1]=3*(f[N-1]-f[N-2]);
  for(i=1;i<N-1;i++)
  {
   A[i]=t[i]-t[i-1];
   B[i]=2*(t[i+1]-t[i-1]);
   C[i]=t[i+1]-t[i];
   D[i]=3*(f[i+1]-f[i-1]);
  }
  slv3d(A,B,C,D,P,Tgrid::n);
  fillkoeff(f,P);
 }
 //! сплайн по уже рассчитанным ранее значениям коэффициентов из внешнего хранилища kk pp - координаты точек.
 spline3(const T* pp,T *kk,int N,int none):Tgrid(N,pp)//spline_grid_v<T>
 {
  koeff=kk;
  selfkoeff=0;
 }
 virtual ~spline3()
 {
  if(selfkoeff)
  {
    delete [] koeff;
  }
 }
 virtual T operator()(const Tspl_acsel<T>& ac) const
 {
  T* k=koeff+ac.ipoi*4;
  return k[0]+(k[1]+(k[2]+k[3]*ac.a)*ac.a)*ac.a;
 }
 inline T operator()(T t) const {return (*this)(Tgrid::acsel(t));}
 inline T d0(T t)const{return (*this)(t);}
 inline T d(const Tspl_acsel<T>& ac) const
 {
  T* k=koeff+ac.ipoi*4;
  return (k[1]+(2*k[2]+3*k[3]*ac.a)*ac.a)*ac.e_h;
 }
 inline T d(T t)const{return d(Tgrid::acsel(t));}
 virtual T d2(const Tspl_acsel<T>& ac) const
 {
  T* k=koeff+ac.ipoi*4;
  return (2*k[2]+3*k[3]*ac.a)*ac.e_h*ac.e_h;
 }
 inline T d2(T t) const {return d2(Tgrid::acsel(t));}
 virtual T d3(const Tspl_acsel<T>& ac) const
 {
  T* k=koeff+ac.ipoi*4;
  return (6*k[3])*ac.e_h*ac.e_h*ac.e_h;
 }
 inline T d3(T t) const {return d2(Tgrid::acsel(t));}
 T integral(Tspl_acsel<T> ac) const
 {
  T* k=koeff+ac.ipoi*4;
  return (k[0]+(0.5*k[1]+(k[2]/3+0.25*k[3]*ac.a)*ac.a)*ac.a)*ac.a;
 }
// T integral(T t0,T t1) const;
 void operator+=(T val){for(int i=0;i<Tgrid::n;i++){koeff[i*4]+=val;}}
 void operator*=(T val){for(int i=0;i<Tgrid::n*4;i++){koeff[i]*=val;}}
 template <class Tgrid1>
 void operator+=(const spline3<T,Tgrid1>& val)
 {
  int i;
  T tt;
  T *k=koeff;
  T f[2],p[2];
  f[1]=val(tt);
  p[1]=val.d(tt);
  for(i=0;i<Tgrid::n;i++,k+=4)
  {
   p[0]=p[1];
   f[0]=f[1];
   tt=Tgrid::get_t(i+1);
   f[1]=val(tt);
   p[1]=val.d(tt);
   k[0]+=f[0];
   k[1]+=p[0];
   k[2]+=3*(f[1]-f[0])-2*p[0]-p[1];
   k[3]+=2*(f[0]-f[1])+p[0]+p[1];
  }
 }
};

/*
template <class T,class Tgrid>
T integral(const spline3<T,Tgrid>& s,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
 return 0;
}
*/
template <class T>
 T integral(const spline3<T,spline_grid_h<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
 {
  int i,j;
  T s[4]={0};
  T* k,r;
  for(i=ac_0.ipoi,k=sp.koeff+i*4;i<ac_1.ipoi;i++,k+=4)
  {
   for(j=0;j<4;j++)
   {
    s[j]+=k[j];
   }
  }
  r=s[0]+0.5*s[1]+0.333333333333333333*s[2]+0.25*s[3];
  return (r-sp.integral(ac_0)+sp.integral(ac_1))*sp.h;
 }

#if _MSC_VER>=1400 || defined __GNUC__ || defined __STDC__
template <class T>
 T integral(const spline3<T,spline_grid_v<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
 {
  int i,j;
  int i0,i1;
  T* t=sp.t;
  T s[4]={0};
  T* k,r;
  for(i=ac_0.ipoi,k=sp.koeff+i*4;i<ac_1.ipoi;i++,k+=4)
  {
   for(j=0;j<4;j++)
   {
    s[j]+=k[j]*(t[i+1]-t[i]);
   }
  }
  r=s[0]+0.5*s[1]+0.3333333333333333*s[2]+0.25*s[3];
  return r-sp.integral(ac_0)*(t[ac_0.ipoi+1]-t[ac_0.ipoi])+sp.integral(ac_1)*(t[ac_1.ipoi+1]-t[ac_1.ipoi]);
 }

template <class T>
 T integral(const spline3<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
 {
  int i,j;
  int i0,i1;
  const T* t=sp.t;
  T s[4]={0};
  T* k,r;
  for(i=ac_0.ipoi,k=sp.koeff+i*4;i<ac_1.ipoi;i++,k+=4)
  {
   for(j=0;j<4;j++)
   {
    s[j]+=k[j]*(t[i+1]-t[i]);
   }
  }
  r=s[0]+0.5*s[1]+0.3333333333333333*s[2]+0.25*s[3];
  return r-sp.integral(ac_0)*(t[ac_0.ipoi+1]-t[ac_0.ipoi])+sp.integral(ac_1)*(t[ac_1.ipoi+1]-t[ac_1.ipoi]);
 }
#endif

template <class T>
T integral(const const_interpolation<T>& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T s=0;
    if(ac_0.a>=1.)return 0;
    if(ac_0.ipoi<ac_1.ipoi)
    {
        int i=ac_0.ipoi+1,j;
        const T* k;
        for(k=sp.data+i;i<ac_1.ipoi;i++,k+=1)
        {
            s += k[0]*(t[i+1]-t[i]);
        }
        
        i=ac_0.ipoi;
        k=sp.data+i;
        s+=k[0]*(T(1.)-ac_0.a)*(t[i+1]-t[i]);
        
        i=ac_1.ipoi;
        k=sp.data+i;
        ac_1.a=fmin(ac_1.a,1.);
        s+=k[0]*ac_1.a*(t[i+1]-t[i]);
        
        return s;
    }
    else if(ac_0.ipoi==ac_1.ipoi)
    {
        int i0 = ac_0.ipoi;
        int i1 = i0+1;
        T a0 = fmax(0.,ac_0.a);
        T a1 = fmin(ac_1.a,1.);
        T t0 = t[i0];
        T t1 = t[i1];
        T k0=sp.data[i0];
        s=k0*(a1 - a0)*(t1-t0);
        return s;
    }
    else
    {
        //std::cout<<"integral: integration limits swap\n";
        return integral(sp,ac_1,ac_0);
    }
}

template <class T>
T integral(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T s=0;
    if(ac_0.ipoi<ac_1.ipoi)
    {
        int i=ac_0.ipoi+1,j;
        const T* k;
        for(k=sp.data+i;i<ac_1.ipoi;i++,k+=1)
        {
            s += (k[0]+k[1])*(t[i+1]-t[i]);
        }
        
        i=ac_0.ipoi;
        k=sp.data+i;
        s+=(k[0]+k[1])*(T(1.)-ac_0.a)*(t[i+1]-t[i]);
        
        i=ac_1.ipoi;
        k=sp.data+i;
        s+=(k[0]+k[1])*ac_1.a*(t[i+1]-t[i]);
        
        s*=T(0.5);
        return s;
    }
    else if(ac_0.ipoi==ac_1.ipoi)
    {
        int i0 = ac_0.ipoi;
        int i1 = i0+1;
        T a0 = ac_0.a;
        T a1 = ac_1.a;
        T t0 = t[i0];
        T t1 = t[i1];
        T k0=sp.data[i0];
        T k1=sp.data[i1];
        s=-0.5*(a0-1.*a1)*((-2.+a0+a1)*k0-1.*(a0+a1)*k1)*(t0-t1);
        return s;
    }
    else
    {
        //std::cout<<"integral: integration limits swap\n";
        return integral(sp,ac_1,ac_0);
    }
}

//! Вычисление интеграла функции, взятого по длине соответствующей кривой: I=\int_{x1}^{x2}f(x)\sqrt{f'(x)^2+1}dx
template <class T>
T integral_fdl(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T s=0;
    T dfdt=0;
    T dt;
    if(ac_0.ipoi<ac_1.ipoi)
    {
        int i=ac_0.ipoi+1,j;
        const T* k;
        for(k=sp.data+i;i<ac_1.ipoi;i++,k+=1)
        {
            dt = t[i+1]-t[i];
            dfdt = (k[1]-k[0])/dt;
            s += (k[0]+k[1])*sqrt(dfdt*dfdt + 1.)*dt;
        }
        
        i=ac_0.ipoi;
        k=sp.data+i;
        dt = t[i+1]-t[i];
        dfdt = (k[1]-k[0])/dt;
        s+=(k[0]+k[1])*(T(1.)-ac_0.a)*dt;
        
        i=ac_1.ipoi;
        k=sp.data+i;
        dt = t[i+1]-t[i];
        dfdt = (k[1]-k[0])/dt;
        s+=(k[0]+k[1])*ac_1.a*dt;
        
        s*=T(0.5);
        return s;
    }
    else if(ac_0.ipoi==ac_1.ipoi)
    {
        int i0 = ac_0.ipoi;
        int i1 = i0+1;
        T a0 = ac_0.a;
        T a1 = ac_1.a;
        T t0 = t[i0];
        T t1 = t[i1];
        dt = t1 - t0;
        T k0=sp.data[i0];
        T k1=sp.data[i1];
        dfdt = (k1 - k0)/dt;
        s=0.5*(a0-1.*a1)*((-2.+a0+a1)*k0-1.*(a0+a1)*k1)*sqrt(dfdt*dfdt + 1.)*dt;
        return s;
    }
    else
    {
        //std::cout<<"integral: integration limits swap\n";
        return integral_fdl(sp,ac_1,ac_0);
    }
}

//!Приближенное вычисление интеграла функции, взятого по длине соответствующей кривой: I=\int_{x1}^{x2}f(x)\sqrt{f'(x)^2+1}dx
template <class T>
T integral_fdl(const spline3<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T s=0;
    T dfdt=0;
    T dt;
    T itg;
    T A;
    if(ac_0.ipoi<ac_1.ipoi)
    {
        int j;
        for(j=ac_0.ipoi;j<ac_1.ipoi;j++)
        {
            A = (sp(t[j+1]) - sp(t[j]))/(t[j+1] - t[j]);
            A = sqrt(A*A + 1);
            itg = integral(sp,t[j],t[j+1]);
            s += itg*A;
        }
        T x = ac_1.a*ac_1.e_h+t[j];
        T ntj = std::nextafter(t[j],t[j]+0.01);
        T nx = std::nextafter(t[j],t[j]+0.01);
        T eps = fmax(ntj-t[j],nx-x);
        T dxt = x - t[j]; 
        if(fabs(dxt) < eps)
        {
            A = (T)0.0;
        }
        else
        {
            A = (sp(x) - sp(t[j]))/dxt;
        }
        A = sqrt(A*A + 1);
        itg = integral(sp,t[j],x);
        s += itg*A;
        return s;
    }
    else if(ac_0.ipoi==ac_1.ipoi)
    {
        T x0 = t[ac_0.ipoi] + ac_0.a*ac_0.e_h;
        T x1 = t[ac_1.ipoi] + ac_1.a*ac_1.e_h;
        A = (sp(x1) - sp(x0))/(x1 - x0);
        A = sqrt(A*A + 1);
        itg = integral(sp,ac_0,ac_1);
        s = itg*A;
        return s;
    }
    else
    {
        //std::cout<<"integral: integration limits swap\n";
        return integral_fdl(sp,ac_1,ac_0);
    }
}

template <class T>
T integral_pow2(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T o[7];
    T s=0;
    if(ac_0.ipoi < ac_1.ipoi)
    {
        int i=ac_0.ipoi+1,j;
        const T* k;
        T a;
        T dt;
        
        for(k=sp.data+i;i<ac_1.ipoi;i++,k+=1)
        {
            s += 0.3333333333333333*(k[0]*k[0]+k[0]*k[1]+k[1]*k[1])*(t[i+1]-t[i]);
        }
        
        i=ac_0.ipoi;
        k=sp.data+i;
        a=ac_0.a;
        dt = t[i+1] - t[i];
        o[0]=-1.+a;
        o[1]=o[0]*o[0];
        o[2]=k[0];
        o[3]=o[2]*o[2];
        o[4]=a*a;
        o[5]=k[1];
        o[6]=o[5]*o[5];
        s+=(-0.333333333333333*o[0]*(o[1]*o[3]+o[2]*(1.+a-2.*o[4])*o[5]+(1.+a+o[4])*o[6]))*dt;
        
        i=ac_1.ipoi;
        k=sp.data+i;
        a=ac_1.a;
        dt = t[i+1] - t[i];
        o[0]=k[0];
        o[1]=o[0]*o[0];
        o[2]=k[1];
        o[3]=a*a;
        o[4]=o[2]*o[2];
        s+=(0.333333333333333*a*((3.+(-3.+a)*a)*o[1]+(3.-2.*a)*a*o[0]*o[2]+o[3]*o[4]))*dt; 
        
        return s;
    }
    else if(ac_0.ipoi == ac_1.ipoi)
    {
        int i0 = ac_0.ipoi;
        int i1 = i0+1;
        T a1=ac_0.a;
        T a2=ac_1.a;
        T t0=t[i0];
        T t1=t[i1];
        T k0=sp.data[i0];
        T k1=sp.data[i1];
        o[0]=k0*k0;
        o[1]=a1*a1;
        o[2]=-1.*k1;
        o[3]=k0+o[2];
        o[4]=o[3]*o[3];
        o[5]=a2*a2;
        o[6]=k1*k1;
        s=0.333333333333333*(t0-1.*t1)*(3.*a1*o[0]-3.*k0*o[1]*o[3]+a1*o[1]*o[4]\
        -1.*a2*((3.-2.*a2)*a2*k0*k1+o[0]*(3.-3.*a2+o[5])+o[5]*o[6]));
        return s;
    }
    else
    {
        //std::cout<<"integral_pow2: integration limits swap\n";
        return integral_pow2(sp,ac_1,ac_0);
    }
}

//! Вычисление интеграла I=\int_{x1}^{x2}x^Nf(x)dx
template <class T, int N>
struct integral_xN
{
    T operator()(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1){throw std::string("integral_xN: operator() is not implemented!");}
};

template <class T>
struct integral_xN<T,1>
{
    T operator()(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1);
};

//! Вычисление интеграла I=\int_{x1}^{x2}xf(x)dx
template <class T>
inline T integral_xN<T,1>::operator()(const lin_int_ref<T,spline_grid_v_ref<T> >& sp,Tspl_acsel<T> ac_0,Tspl_acsel<T> ac_1)
{
    const T* t=sp.t;
    T s=0;
    T o[3];
    T x;
    if(ac_0.ipoi<ac_1.ipoi)
    {
        int i=ac_0.ipoi+1,j;
        const T* k;
        //Сначала суммируем интегралы по интервалам сетки, целиком попадающим в область интегрирования
        for(k=sp.data+i;i<ac_1.ipoi;i++,k+=1)
        {
            s += (t[i+1] - t[i])*(k[0]*(2.*t[i] + t[i+1]) + k[1]*(t[i] + 2.*t[i+1]));
        }
        //К сумме добавляем куски от интервалов сетки слева ...
        i=ac_0.ipoi;
        k=sp.data+i;
        x = ac_0.a;
        o[0] = -1. + x;
        o[1] = x*x;
        s += (t[i] - t[i+1])*o[0]*(k[0]*o[0]*(-t[i+1]*(1. + 2.*x) + 2.*t[i]*o[0]) + k[1]*(t[i]*(1. + x - 2.*o[1]) + 2.*t[i+1]*(1. + x + o[1])));
        //... и справа
        i=ac_1.ipoi;
        k=sp.data+i;
        x = ac_1.a;
        o[0] = o[1];
        s += (t[i+1] - t[i])*x*(6.*k[0]*t[i] + 3.*(k[1]*t[i] + k[0]*(-2.*t[i] + t[i+1]))*x + 2.*(k[0] - k[1])*(t[i] - t[i+1])*o[0]);
        //Экономим на умножении
        s *= 0.166666666666667;
        return s;
    }
    else if(ac_0.ipoi==ac_1.ipoi)
    {
        int i0 = ac_0.ipoi;
        int i1 = i0+1;
        T a0 = ac_0.a;
        T a1 = ac_1.a;
        T t0 = t[i0];
        T t1 = t[i1];
        T k0=sp.data[i0];
        T k1=sp.data[i1];
        o[0] = a0*a0;
        o[1] = -2.*a1;
        o[2] = 3. + o[1];
        s = 0.166666666666667*(t1 - t0)*(-6.*a0*k0*t0 - 2.*a0*(k0 - k1)*(t0 - t1)*o[0] + (6.*k0*t0 - 3.*(k1*t0 + k0*t1))*o[0] + a1*(a1*k1*(2.*a1*t1 + t0*o[2]) + 
             k0*(2.*(3. + (a1 - 3.)*a1)*t0 + a1*t1*o[2])));
        return s;
    }
    else
    {
        return operator()(sp,ac_1,ac_0);
    }
}


template <class Tspl,class T>
inline T integral(const Tspl& sp,T t0,T t1)
{
 return integral(sp,sp.acsel(t0),sp.acsel(t1));
}

template <class T,class Tspl>
inline T integral_pow2(const Tspl& sp,T t0,T t1)
{
 return integral_pow2(sp,sp.acsel(t0),sp.acsel(t1));
}

template <class T,class Tspl>
inline T integral_fdl(const Tspl& sp,T t0,T t1)
{
 return integral_fdl(sp,sp.acsel(t0),sp.acsel(t1));
}

template <class T,class Tspl>
inline T integral_xF(const Tspl& sp,T t0,T t1)
{
 return integral_xN<T,1>()(sp,sp.acsel(t0),sp.acsel(t1));
}

#endif