#ifndef dot_h
#define dot_h
#if defined __GNUC__ || defined __STDC__
#define _hypot
#endif
#include <math.h>
template <class T>
T Dot(int Dim,const T* p1,const T* p2)
{
 T sum=0;
 int i;
 for(i=0;i<Dim;i++)
 {
  sum+=p1[i]*p2[i];
 }
 return sum;
}
template <class T>
void Normir(int Dim,T* p1)
{
 T sum=0;
 int i;
 for(i=0;i<Dim;i++)
 {
  sum+=p1[i]*p1[i];
 }
 sum=1./sqrt(sum);
 for(i=0;i<Dim;i++)
 {
  p1[i]*=sum;
 }
}
template <class T>
T Norm(int Dim,const T* p1)
{
 T sum=0;
 int i;
 for(i=0;i<Dim;i++)
 {
  sum+=p1[i]*p1[i];
 }
 return sqrt(sum);
}
template <class T>
T Norm(int Dim,const T* p1,const T* p2)
{
 T sum=0,v;
 int i;
 for(i=0;i<Dim;i++)
 {
  v=(p1[i]-p2[i]);
  sum+=v*v;
 }
 return sqrt(sum);
}
template <class T>
T Dot(const T* p1,const T* p1last,const T* p2)
{
 T sum=0;
 for(;p1!=p1last;p1++,p2++)
 {
  sum+=*p1**p2;
 }
 return sum;
}
// r=a*x+b*y
template <class T>
void raxpby(int dim,T a,const T* x,T b,const T* y,T* r)
{
 int i;
 for(i=0;i<dim;i++){r[i]=a*x[i]+b*y[i];}
}
// x+=a*y
template <class T>
void xpeay(int dim,T* x,T a,const T* y)
{
 int i;
 for(i=0;i<dim;i++){x[i]+=a*y[i];}
}
// x+=y;
template <class T>
void xpey(int dim,T* x,const T* y)
{
 int i;
 for(i=0;i<dim;i++){x[i]+=y[i];}
}
template <class T>
void xmey(int dim,T* x,const T* y)
{
 int i;
 for(i=0;i<dim;i++){x[i]-=y[i];}
}
template <class T>
void xey(int dim,T* x,const T* y)
{
 int i;
 for(i=0;i<dim;i++){x[i]=y[i];}
}
template <class T>
void xey(int dim,T* x,T y)
{
 int i;
 for(i=0;i<dim;i++){x[i]=y;}
}
#endif