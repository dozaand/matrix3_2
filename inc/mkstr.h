#pragma once
#include <sstream>
#include <string>

class mkstr {
public:
    template<class T>
    mkstr& operator<< (const T& arg) {
        m_stream << arg;
        return *this;
    }
    operator std::string() const {
        return m_stream.str();
    }
protected:
    std::stringstream m_stream;
};