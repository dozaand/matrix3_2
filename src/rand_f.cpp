#include <matrix3_2/inc/rand_f.h>
#include <math.h>

double gasdev()
{
float fac, gasdev_v, rsq, v1, v2;
static float gset;
static int iset = 0;
if( iset == 0 ) {
L_1:
        v1 = 2.*random_e() - 1.;
        v2 = 2.*random_e() - 1.;
        rsq = v1*v1 + v2*v2;
        if( rsq >= 1. || rsq == 0. )
                goto L_1;
        fac = sqrt( -2.*log( rsq )/rsq );
        gset = v1*fac;
        gasdev_v = v2*fac;
        iset = 1;
        }
else {
        gasdev_v = gset;
        iset = 0;
        }
return gasdev_v;
}
