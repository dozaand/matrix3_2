#include <matrix3_2/inc/matr_umf.h>
#include <umfpack.h>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>

double Control[UMFPACK_CONTROL];

/*int n = 5 ;
int Ap [ ] = {0, 2, 5, 9, 10, 12} ;
int Ai [ ] = { 0, 1, 0, 2, 4, 1, 2, 3, 4, 2, 1, 4} ;

double Ax [ ] = {2., 3., 3., -1., 4., 4., -3., 1., 2., 2., 6., 1.} ;
double b [ ] = {8., 45., -3., 3., 19.} ;
double x [5] ;*/

/*int main (void)
{
double *null = (double *) NULL ;
int i ;
void *Symbolic, *Numeric ;
(void) umfpack_di_symbolic (n, n, Ap, Ai, Ax, &Symbolic, null, null) ;
(void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, null) ;
umfpack_di_free_symbolic (&Symbolic) ;
(void) umfpack_di_solve (UMFPACK_A, Ap, Ai, Ax, x, b, Numeric, null, null) ;
umfpack_di_free_numeric (&Numeric) ;
for (i = 0 ; i < n ; i++) printf ("x [%d] = %g\n", i, x [i]) ;
return (0) ;
}*/

template class matr_umf_lu<double>;
template class matr_umf<double>;
//template class matr_umf_lu<float>;
//template class matr_umf<float>;
//template<> class matr_umf_lu<float>;

/*matr_umf_lu<double>::umfpack_symbolic = &umfpack_di_symbolic;
matr_umf_lu<double>::umfpack_numeric = &umfpack_di_numeric;
matr_umf_lu<double>::umfpack_free_symbolic = &umfpack_di_free_symbolic;
matr_umf_lu<double>::umfpack_free_numeric = &umfpack_di_free_numeric;*/

/*matr_umf_lu<float>::umfpack_symbolic = &umfpack_si_symbolic;
matr_umf_lu<float>::umfpack_numeric = &umfpack_si_numeric;
matr_umf_lu<float>::umfpack_free_symbolic = &umfpack_si_free_symbolic;
matr_umf_lu<float>::umfpack_free_numeric = &umfpack_si_free_numeric;*/

template<class TReal>
matr_umf_lu<TReal>::matr_umf_lu(int d):matrix<TReal>(d)
{
    Numeric = nullptr;
    Symbolic = nullptr;
    Matr = nullptr;
}

template<class TReal>
matr_umf_lu<TReal>::~matr_umf_lu()
{
    clear();
}

template<class TReal>
void matr_umf_lu<TReal>::clear()
{
    if(Symbolic != nullptr)
    {
       (*umfpack_di_free_symbolic)(&Symbolic) ;
    }
    if(Numeric != nullptr)
    {
        (*umfpack_di_free_numeric)(&Numeric) ;
    }
    Numeric = nullptr;
    Symbolic = nullptr;
    Matr = nullptr;
}

template<class TReal>
void matr_umf_lu<TReal>::update(matr_t& ma)
{
    ma.to_crf();
    int n=Dim;
    int *Ap = ma.Ap.data();
    int *Ai = ma.Ai.data();
    TReal *Ax = ma.Ax.data();
    Control[UMFPACK_STRATEGY]=UMFPACK_STRATEGY_UNSYMMETRIC;
    Control [UMFPACK_DENSE_COL]=0.5;
    Control [UMFPACK_DENSE_ROW]=0.5;
    Control [UMFPACK_AMD_DENSE]=10;
    Control [UMFPACK_BLOCK_SIZE]=32;
    Control[UMFPACK_PIVOT_TOLERANCE]=1e-1;
    Control [UMFPACK_SCALE]=UMFPACK_SCALE_MAX;
    if(&ma != Matr)
    {
        Matr = &ma;
        if(Symbolic != nullptr)
        {
            (*umfpack_di_free_symbolic)(&Symbolic) ;
        }
        Symbolic = nullptr;
    }
    if(Symbolic == nullptr)
    {
        (*umfpack_di_symbolic)(n, n, Ap, Ai, Ax, &Symbolic, (TReal*)nullptr, (TReal*)nullptr) ;
    }
    if(Numeric != nullptr)
    {
        (*umfpack_di_free_numeric)(&Numeric) ;
        Numeric = nullptr;
    }
    int status = umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, (TReal*)nullptr, (TReal*)nullptr) ;
}

template<class TReal>
TReal* matr_umf_lu<TReal>::mul(const TReal* b, TReal* x)
{
    int *Ap = ((matr_t*)Matr)->Ap.data();
    int *Ai = ((matr_t*)Matr)->Ai.data();
    TReal *Ax = ((matr_t*)Matr)->Ax.data();
    Control[UMFPACK_IRSTEP] = 0;
    int status=umfpack_di_solve (UMFPACK_At, Ap, Ai, Ax, x, b, Numeric, nullptr, nullptr) ;
    if(status>0)
    {
        std::ofstream s("umf_fail_matrix.dat");
        int pmode=((matr_t*)Matr)->print_mode;
        ((matr_t*)Matr)->print_mode=1;
        s << ((matr_t*)Matr)->repr();
        ((matr_t*)Matr)->print_mode=pmode;
        s.flush();
        s.close();
        //throw "Error was fatal";
    }
    return x;
}

template<class TReal>
void matr_umf_lu<TReal>::mul_py(long b, long x)
{
    mul((const TReal*)b,(TReal*)x);
}

template<class TReal>
TReal& matr_umf_lu<TReal>::operator()(int,int){static TReal x(0); return x;}

template <class TReal>
TReal& matr_umf<TReal>::operator()(int i_row,int i_col)
{
  if(matr_filled)
  {
   int Api = Ap[i_row];
   int len = Ap[i_row+1]-Api;
   int pos = int_bsearch(len+1, (Ai.data()+Api), i_col);
   if (pos<len)
   {
    pos+=Api;
    return Ax[pos];
   }
   else
   {
       throw std::runtime_error("matr_umf: No data in sparse matrix\n");
   }
  }
  else
  {
    if(i_row<Dim || i_col<Dim)
       return first_matrix[i_row][i_col];
    else
       throw std::runtime_error("matr_umf: Index out of range\n");
  }
}

template<class TReal>
void matr_umf<TReal>::clear()
{
    Ax.clear();
    Ap.clear();
    Ap.resize(matrix<TReal>::dim()+1);
    Ai.clear();
    first_matrix.clear();
    matr_filled = false;
}

template <class TReal>
TReal& matr_umf<TReal>::operator()(Tcrf_accel& ac)
{
  if(matr_filled)
  {
   if(ac.is_ready)
   {
     int pos = ac;
     ++ac;
     return Ax[pos];
   }
   else
   {
    int Api = Ap[ac.iY];
    int len = Ap[ac.iY+1]-Api;
    int pos = int_bsearch(len+1, (Ai.data()+Api), ac.iX);
    if (pos<len)
    {
     pos+=Api;
     ac+=pos;
     ++ac;
     return Ax[pos];
    }
    else
    {
        throw std::runtime_error("matr_umf: No data in sparse matrix\n");
    }
   }
  }
  else
  {
    if(ac.iY<Dim || ac.iX<Dim)
       return first_matrix[ac.iY][ac.iX];
    else
       throw std::runtime_error("matr_umf: Index out of range\n");
  }
}


template <class TReal>
TReal matr_umf<TReal>::operator()(int i_row,int i_col)const
{
  if(matr_filled)
  {
   int Api = Ap[i_row];
   int len = Ap[i_row+1]-Api;
   int pos = int_bsearch(len+1, Ai.data()+Api, i_col);
   if(pos<len )
   {
    if(pos == len - 1 || pos == 0)
    {
        if(i_col != Ai[pos+Api])
        {
            return TReal(0);
        }
    }
    pos+=Api;
    return Ax[pos];
   }
   else
   {
       return 0;
   }
  }
  else
  {
   throw std::runtime_error("matr_umf: matrix is not filled\n");
  }
}


template<class TReal>
void matr_umf<TReal>::to_crf()
{
   if(!matr_filled)
   {
    matr_filled = true;
    int NNZ=0;
    int i=0;
    for(auto row = first_matrix.begin(); row != first_matrix.end(); ++row)
    {
        Ap[i]=NNZ;
        NNZ+=row->second.size();
        ++i;
    }
    Ap[i]=NNZ;
    Ax.resize(NNZ);
    Ai.resize(NNZ);
    i=0;
    for(auto row = first_matrix.begin(); row != first_matrix.end(); ++row)
    {
        for(auto data=row->second.begin();data!=row->second.end();++data)
        {
            Ai[i]=data->first;
            Ax[i]=data->second;
            ++i;
        }
    }
    //first_matrix.clear();
   }
}

template<class TReal>
std::string matr_umf<TReal>::repr()const
{
   using namespace std;
   int i_row,i_col;
   std::stringstream out;
   out.precision(20);
   TReal val;
   if(print_mode)
   {
       out<<"[\n";
       for(i_row=0;i_row<Dim;++i_row)
       {
           out<<"[\n";
           for(i_col=0;i_col<Dim;++i_col)
           {
               val = (*this)(i_row, i_col);
               out << setw(15) << val;
               if(i_col<Dim-1)
               {
                   out<<",";
               }
           }
          out<<"]\n";
           if(i_col<Dim-1)
           {
               out<<",";
           }
        }
       out<<"]\n";
   }
   else
   {
       int i;
       int NNZ=Ax.size();
       out<<"[[";
           for(i=0;i<Dim;++i){out<<Ap[i]<<",";}
           out<<Ap[i];
       out<<"],\n";
       out<<"[";
           for(i=0;i<NNZ-1;++i){out<<Ai[i]<<",";}
               out<<Ai[i];
       out<<"],\n";
       out<<"[";
           for(i=0;i<NNZ-1;++i){out<<Ax[i]<<",";}
               out<<Ax[i];
       out<<"]]\n"<<ends;
   }
   return out.str();
}
/*template<class TReal>
void matr_umf<TReal>::pt()const
{
  int w=15;
  if(matr_filled)
  {

  }
  else
  {
    int dim=first_matrix.size();
    int i,j;
    for(j=0;j<dim;++j)
    {
      auto it_row=first_matrix.find(j);
      if(it_row==first_matrix.end())
      {
        for(i=0;i<dim;++i)cout<<setw(w)<<0;
      }
      else
      {
        auto end=it_row->second.end(i);
        for(i=0;i<dim;++i)
        {
          auto itcol=it_row->second.find(i);
          double val;
          if(itcol!=end){val=itcol->second;}
          else{val=0;}
          cout<<setw(w)<<0;
        }
      }
      cout<<endl;
    }
  }
}*/

template<class TReal>
matr_umf<TReal>& matr_umf<TReal>::operator*=(TReal q)
{
    TReal *d=Ax.data();
    int n = Ax.size();
    for(int i=0;i<n;++i){d[i]*=q;}
    return *this;
}

Tcrf_accel::Tcrf_accel():
    is_ready(false),
    finished(false),
    iX(0),
    iY(0),
    curr_p(0)
{
}

void Tcrf_accel::start()
{
    finished = false;
    curr_p = 0;
}

void Tcrf_accel::finish()
{
    finished = true;
    if(posbuf.size()) is_ready = true;
}

Tcrf_accel& Tcrf_accel::operator+=(int p)
{
    if(!is_ready)
    {
        posbuf.push_back(p);
    }
    return *this;
}

Tcrf_accel& Tcrf_accel::operator++()
{
    curr_p++;
    return *this;
}

Tcrf_accel::operator int()
{
    return posbuf[curr_p];
}

Tcrf_accel& Tcrf_accel::operator()(int y, int x)
{
    iX=x;
    iY=y;
    return *this;
}