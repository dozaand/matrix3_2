#include "../inc/s_tab.h"
#include <string.h>
#include <cassert>
//#include <boost/filesystem.hpp>

//using namespace boost::filesystem;
using namespace std;
namespace s_tab
{
//------------------------------------------------------
typedef unsigned int uint;
#ifdef NOT_USE_YAML_DSC
void Node::save(ostream& s)
{
    put(s,beg);
    put(s,end);
    uint nitem=items.size();
    put(s,nitem);
    for(auto it=items.begin();it!=items.end();++it)
    {
     unsigned char siz=(unsigned char)it->first.size();
     put(s,siz);
     s.write(it->first.c_str(),siz);
     it->second.save(s);
    }
}
void Node::load(istream& s)
{
    char buf[256];
    get(s,beg);
    get(s,end);
    uint nitem,i;
    unsigned char siz;
    get(s,nitem);
    for(i=0;i<nitem;++i)
    {
        get(s,siz);
        s.read(buf,siz);
        buf[siz]=0;
        Node& subnode=items[buf];
        subnode.load(s);
    }
}
#else
YAML::Emitter& operator << (YAML::Emitter& out, const Node& v) 
{
    out << YAML::BeginMap;
    out << YAML::Key << "rg";
    out << YAML::Value;
    out << YAML::BeginSeq;
    out << v.beg << v.end;
    out << YAML::EndSeq;
    if(v.items.size()>0)
    {
       out << YAML::Key << "items" << YAML::Value << v.items;
    }
    out << YAML::EndMap;
    return out;
}
const YAML::Node& operator >> (const YAML::Node& node, Node& v)
{
    YAML::Node snode;
    YAML::const_iterator it;
    snode = node["rg"];
    it=snode.begin();
    v.beg=it->as<int>();++it;v.end=it->as<int>();
    if(node["items"])
    {
      snode = node["items"];// may be checked for no node
      std::string key;
      for(YAML::const_iterator it=snode.begin();it!=snode.end();++it) 
      {
          Node& subnode=v.items[it->first.as<string>()];
          it->second >> subnode;
      }
    }
    return node;
}

#endif

tstream::tstream(const char* nm)
{
 root.beg=root.end=0;
 marker_nm=nm;
 marker_nm+=".yaml";
}

tostream::tostream(const char* nm):tstream(nm),ofstream(nm, ios::binary| ios::out)
{
}
tistream::tistream(const char* nm):tstream(nm),ifstream(nm, ios::binary| ios::in)
{
 stk.push_back(&root);
 #ifdef NOT_USE_YAML_DSC
 ifstream ss(marker_nm.c_str());
 root.load(ss);
 #else
 YAML::Node node = YAML::LoadFile(marker_nm.c_str());
 assert( node["format"].as<string>()==ver() );
 YAML::Node node_tabs = node["tabs"];
 node_tabs >> root;
 type = node["type"].as<map<string,int> >();
 #endif
}
void tostream::flush()
{
 ofstream ss(marker_nm.c_str());

 #ifdef NOT_USE_YAML_DSC
 root.save(ss);
 #else
 YAML::Emitter out;
 out << YAML::BeginMap;
 out << YAML::Key << "format" << YAML::Value << ver();
 out << YAML::Key << "tabs" << YAML::Value << root;
 out << YAML::Key << "type" << type;
 out << YAML::EndMap;
 ss.write(out.c_str(),out.size());
 #endif
 ostream::flush();
}

tostream::~tostream()
{
 root.end=tellp();
 flush();
}

tistream::~tistream()
{
}

void tistream::cd(const char * pos)
{
 const char * b=pos,*e=pos;
 try
 {
   if(b[0]=='/')
   {
       stk.clear();
       stk.push_back(&root);
       ++b;
       ++e;
   }
   for(;;)
   {
    for(;*b!=0;++e)
    {
      switch(*e)
      {
          case 0:
          case '/':
          {
              string s(b,e);
              if(s=="..")
              {
                  stk.pop_back();
              }
              else if(s==".")
              {
                  stk.clear();
                  stk.push_back(&root);
              }
              else
              {
                  Node* nd=&stk.back()->items.at(s);
                  stk.push_back(nd);
              }
              if(*e==0)
              {
                seekg(stk.back()->beg);
                return;
              }
              b=e+1;
              break;
          }
          default:{}// no action
      }
    }
   }
 }
 catch(...)
 {
    cout<<"fail cd to "<<pos;
    throw;
 }
}

void tistream::extract(const char * file_name,const char * pos) const
{

}

//------------------------------------------------------
tab::tab(tostream& s_,const char * nm):s(s_)
{
 if (strlen(nm)>255) throw "to long name";
 nd=&(s.root.items[nm]);
 nd->beg=s.tellp();
}
tab::tab(tab& mrk,const char * nm):s(mrk.s)
{
 if (strlen(nm)>255) throw "to long name";
 nd=&(mrk.nd->items[nm]);
 nd->beg=s.tellp();
}

tab::~tab()
{
 nd->end=s.tellp();
}

}
