#include <matrix3_2/inc/matr_umf.h>
#include <matrix3_2/inc/matr_superlu.h>
#include <slu_mt_ddefs.h>
#include <slu_mt_util.h>

extern void
sp_colorder(SuperMatrix *A, int_t *perm_c, superlumt_options_t *options,
    SuperMatrix *AC);


template class matr_superlu<double>;

template<typename TReal>
void matr_superlu<TReal>::destroy_all()
{
        if(A.Store != nullptr)
        {
            Destroy_SuperMatrix_Store(&A);
            A.Store = nullptr;
        }
        if(Atr.Store != nullptr)
        {
            Destroy_SuperMatrix_Store(&Atr);
            Atr.Store = nullptr;
        }
        if(Aperm.Store != nullptr)
        {
            Destroy_CompCol_Permuted(&Aperm);
            Aperm.Store = nullptr;
        }
        if(L.Store != nullptr)
        {
            Destroy_SuperNode_Matrix(&L);
            L.Store = nullptr;
        }
        if(U.Store != nullptr)
        {
            Destroy_CompCol_Matrix(&U);
            U.Store = nullptr;
        }
}

template<typename TReal>
matr_superlu<TReal>::matr_superlu(int x):matrix(x)
{
    A.Store = nullptr;
    L.Store = nullptr;
    U.Store = nullptr;
    B.Store = nullptr;
    Atr.Store = nullptr;
    Aperm.Store = nullptr;
    perm_c_v.resize(x);
    perm_r_v.resize(x);
    etree_v.resize(x);
    permutated = false;
    nprocs = 4;
    solving_first_time = true;
    Gstat_allocated = false;
}

void set_default_options(superlumt_options_t *options)
{
    options->fact = DOFACT;
    //options->Equil = YES;
    //options->ColPerm = COLAMD;
    options->trans = NOTRANS;
    //options->IterRefine = NOREFINE;
    //options->DiagPivotThresh = 1.0;
    //options->SymmetricMode = NO;
    //options->PivotGrowth = NO;
    //options->ConditionNumber = NO;
    //options->PrintStat = YES;
}
template<typename TReal>
void matr_superlu<TReal>::update(matr_t& ma)
{
    ma.to_crf();
    if(&ma != Matr)
    {
        Dim = ma.dim();
        Matr = &ma;
        destroy_all();
        permutated = false;
        perm_c_v.resize(Dim);
        perm_r_v.resize(Dim);
        etree_v.resize(Dim);
        colcnt_h_v.resize(Dim);
        part_super_h_v.resize(Dim);
    }
    int n=Dim;
    TReal* a = ma.Ax.data();
    int *xa = ma.Ap.data();
    int nnz = ma.Ai.size();
    int *asub = ma.Ai.data();
    int* perm_c = perm_c_v.data();
    int* perm_r = perm_r_v.data();
    int* etree  = etree_v.data();
    int permc_spec = ordering::UNSYMMETRIC;
    if(A.Store == nullptr)
    {
        dCreate_CompCol_Matrix(&A, n, n, nnz, a, asub, xa, SLU_NR, SLU_D, SLU_GE);    
    }
    if(Atr.Store == nullptr)
    {
        dCreate_CompCol_Matrix(&Atr, n, n, nnz, a, asub, xa, SLU_NC, SLU_D, SLU_GE);    
    }
    options.refact = solving_first_time ? NO : YES;
    options.usepr = solving_first_time ? NO : YES;
    if(!permutated)
    {
        get_perm_c(permc_spec, &A, perm_c);

        /* ------------------------------------------------------------
        Initialize the option structure superlumt_options using the
        user-input parameters;
        Apply perm_c to the columns of original A to form AC.
        ------------------------------------------------------------*/
        options.trans = TRANS;
        options.nprocs = nprocs;
        options.refact = NO;
        options.panel_size = sp_ienv(1);
        options.relax = sp_ienv(2);
        options.diag_pivot_thresh = 1.0;
        options.usepr = NO;
        options.drop_tol = 0.0;
        options.SymmetricMode = NO;
        options.PrintStat = NO;
    
        /* 
        * The following should be retained for repeated factorizations.
        */
        options.perm_c = perm_c;
        options.perm_r = perm_r;
        options.work = nullptr;
        options.lwork = 0;
        options.etree = etree_v.data();
        options.colcnt_h = colcnt_h_v.data();
        options.part_super_h = part_super_h_v.data();

        if(!Gstat_allocated)
        {
            StatAlloc(n, nprocs, options.panel_size, options.relax, &Gstat);
            Gstat_allocated = true;
        }
        else
        {
            StatFree(&Gstat);
            Gstat_allocated = false;
        }
        StatInit(n, nprocs, &Gstat);
        
    
        //t = SuperLU_timer_();
        sp_colorder(&Atr, perm_c, &options, &Aperm);
        //Gstat->utime[ETREE] = SuperLU_timer_() - t;
        
        permutated = true;
    }
    
    //set_default_options(&options);
    //StatInit(0,0,&stat);
    return;
}

template<class TReal>
TReal* matr_superlu<TReal>::mul(const TReal* b, TReal* x)
{
    int n = Dim;
    memcpy((void*)x,b,sizeof(TReal)*n);
    int nrhs = 1;
    int info;
    int* perm_c = perm_c_v.data();
    int* perm_r = perm_r_v.data();
    dCreate_Dense_Matrix(&R, n, nrhs, x, n, SLU_NR, SLU_D, SLU_GE);
    
//    sp_colorder(permc_spec, &A, perm_c, etree, &B);
//    options.Fact = FACTORED;
//    dgstrf(&options, &B, drop_tol, relax, panel_size, etree, NULL, 0, perm_c, perm_r, L, U, &stat, &info);
//    pdgssv(nproc, &A, perm_c_v.data(), perm_r_v.data(), &L, &U, &R, &info );

    /* ------------------------------------------------------------
       Compute the LU factorization of A.
       The following routine will create nprocs threads.
       ------------------------------------------------------------*/
    pdgstrf(&options, &Aperm, perm_r, &L, &U, &Gstat, &info);
    dgstrs (options.trans, &L, &U, perm_r, perm_c, &R, &Gstat, &info);

    solving_first_time = false;
    Destroy_SuperMatrix_Store(&R);
    return x;
}

template<class TReal>
matr_superlu<TReal>::~matr_superlu()
{
    destroy_all();
    if(Gstat_allocated)
    {
        StatFree(&Gstat);
    }
//    Destroy_SuperNode_Matrix(&L);
//    Destroy_CompCol_Matrix(&U);
//    Destroy_CompCol_Matrix(&B);
}


template<class TReal>
void matr_superlu<TReal>::set_nproc(int n)
{
    if(n<1)
    {
        n=1;
    }
    nprocs=n;
    if(Gstat_allocated)
    {
        StatFree(&Gstat);
    }
    StatAlloc(Dim, nprocs, sp_ienv(1), sp_ienv(2), &Gstat);
}


template<class TReal>
void matr_superlu<TReal>::mul_py(long r, long x)
{
    mul((TReal*)r,(TReal*)x);
}