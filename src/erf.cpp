#include <cstdlib>
#include <math.h>
/* MERFI.F -- translated by f2c (version 20020208).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

/* Table of constant values */

static double c_b2 = 1.;

/*   IMSL ROUTINE NAME   - MERFI */
/*   LATEST REVISION     - JANUARY 1, 1978 */
/*   PURPOSE             - INVERSE ERROR FUNCTION */
/*   USAGE               - CALL MERFI (P,Y,IER) */
/*   ARGUMENTS    P      - INPUT VALUE IN THE EXCLUSIVE RANGE (-1.0,1.0) */
/*                Y      - OUTPUT VALUE OF THE INVERSE ERROR FUNCTION */
/*                IER    - ERROR PARAMETER (OUTPUT) */
/*                         TERMINAL ERROR */
/*                           IER = 129 INDICATES P LIES OUTSIDE THE LEGAL */
/*                             RANGE. PLUS OR MINUS MACHINE INFINITY IS */
/*                             GIVEN AS THE RESULT (SIGN IS THE SIGN OF */
/*                             THE FUNCTION VALUE OF THE NEAREST LEGAL */
/*                             ARGUMENT). */
/*   PRECISION/HARDWARE  - SINGLE/ALL */
/*   REQD. IMSL ROUTINES - UERTST,UGETIO */
/*   NOTATION            - INFORMATION ON SPECIAL NOTATION AND */
/*                           CONVENTIONS IS AVAILABLE IN THE MANUAL */
/*                           INTRODUCTION OR THROUGH IMSL ROUTINE UHELP */
static double r_sign(double *a, double *b)
{
double x;
x = (*a >= 0 ? *a : - *a);
return( *b >= 0 ? x : -x);
}

double  inv_erf(double p)
{
    /* Initialized data */
    double y;
    static double a1 = -.5751703;
    static double c2 = -.2368201;
    static double c3 = .05073975;
    static double d0 = -44.27977;
    static double d1 = 21.98546;
    static double d2 = -7.586103;
    static double e0 = -.05668422;
    static double e1 = .3937021;
    static double e2 = -.3166501;
    static double e3 = .06208963;
    static double f0 = -6.266786;
    static double a2 = -1.896513;
    static double f1 = 4.666263;
    static double f2 = -2.962883;
    static double g0 = 1.851159e-4;
    static double g1 = -.002028152;
    static double g2 = -.1498384;
    static double g3 = .01078639;
    static double h0 = .09952975;
    static double h1 = .5211733;
    static double h2 = -.06888301;
    static double rinfm = 1.7014e38;
    static double a3 = -.05496261;
    static double b0 = -.113773;
    static double b1 = -3.293474;
    static double b2 = -2.374996;
    static double b3 = -1.187515;
    static double c0 = -.1146666;
    static double c1 = -.1314774;

    /* Builtin functions */
//    double r_sign(double *, double *), log(doubledouble), sqrt(doubledouble);

    /* Local variables */
    static double a, b, f, w, x, z__, sigma, z2, sd, wi, sn;

    x = p;
    sigma = r_sign(&c_b2, &x);
/*                                  TEST FOR INVALID ARGUMENT */
    if (! (x > -1. && x < 1.)) {
	goto L30;
    }
    z__ = x<0?-x:x;//abs(x);
    if (z__ <= .85) {
	goto L20;
    }
    a = 1. - z__;
    b = z__;
/*                                  REDUCED ARGUMENT IS IN (.85,1.), */
/*                                     OBTAIN THE TRANSFORMED VARIABLE */
/* L5: */
    w = sqrt(-log(a + a * b));
    if (w < 2.5) {
	goto L15;
    }
    if (w < 4.) {
	goto L10;
    }
/*                                  W GREATER THAN 4., APPROX. F BY A */
/*                                     RATIONAL FUNCTION IN 1./W */
    wi = 1. / w;
    sn = ((g3 * wi + g2) * wi + g1) * wi;
    sd = ((wi + h2) * wi + h1) * wi + h0;
    f = w + w * (g0 + sn / sd);
    goto L25;
/*                                  W BETWEEN 2.5 AND 4., APPROX. F */
/*                                     BY A RATIONAL FUNCTION IN W */
L10:
    sn = ((e3 * w + e2) * w + e1) * w;
    sd = ((w + f2) * w + f1) * w + f0;
    f = w + w * (e0 + sn / sd);
    goto L25;
/*                                  W BETWEEN 1.13222 AND 2.5, APPROX. */
/*                                     F BY A RATIONAL FUNCTION IN W */
L15:
    sn = ((c3 * w + c2) * w + c1) * w;
    sd = ((w + d2) * w + d1) * w + d0;
    f = w + w * (c0 + sn / sd);
    goto L25;
/*                                  Z BETWEEN 0. AND .85, APPROX. F */
/*                                     BY A RATIONAL FUNCTION IN Z */
L20:
    z2 = z__ * z__;
    f = z__ + z__ * (b0 + a1 * z2 / (b1 + z2 + a2 / (b2 + z2 + a3 / (b3 + z2))
	    ));
/*                                  FORM THE SOLUTION BY MULT. F BY */
/*                                     THE PROPER SIGN */
L25:
    y = sigma * f;
    goto L9005;
/*                                  ERROR EXIT. SET SOLUTION TO PLUS */
/*                                     (OR MINUS) INFINITY */
L30:
    y = sigma * rinfm;
/* L9000: */
L9005:
    return y;
} /* merfi_ */

/*
	C program for floating point error function

	erf(x) returns the error function of its argument
	erfc(x) returns 1.0-erf(x)

	erf(x) is defined by
	${2 over sqrt(pi)} int from 0 to x e sup {-t sup 2} dt$

	the entry for erfc is provided because of the
	extreme loss of relative accuracy if erf(x) is
	called for large x and the result subtracted
	from 1. (e.g. for x= 10, 12 places are lost).

	There are no error returns.

	Calls exp.

	Coefficients for large x are #5667 from Hart & Cheney (18.72D).
*/

#define M 7
#define N 9
static double torp = 1.1283791670955125738961589031;
static double p1[] = {
	0.804373630960840172832162e5,
	0.740407142710151470082064e4,
	0.301782788536507577809226e4,
	0.380140318123903008244444e2,
	0.143383842191748205576712e2,
	-.288805137207594084924010e0,
	0.007547728033418631287834e0,
};
static double q1[]  = {
	0.804373630960840172826266e5,
	0.342165257924628539769006e5,
	0.637960017324428279487120e4,
	0.658070155459240506326937e3,
	0.380190713951939403753468e2,
	0.100000000000000000000000e1,
	0.0,
};
static double p2[]  = {
	0.18263348842295112592168999e4,
	0.28980293292167655611275846e4,
	0.2320439590251635247384768711e4,
	0.1143262070703886173606073338e4,
	0.3685196154710010637133875746e3,
	0.7708161730368428609781633646e2,
	0.9675807882987265400604202961e1,
	0.5641877825507397413087057563e0,
	0.0,
};
static double q2[]  = {
	0.18263348842295112595576438e4,
	0.495882756472114071495438422e4,
	0.60895424232724435504633068e4,
	0.4429612803883682726711528526e4,
	0.2094384367789539593790281779e4,
	0.6617361207107653469211984771e3,
	0.1371255960500622202878443578e3,
	0.1714980943627607849376131193e2,
	1.0,
};

double erfc(double arg){
	double n, d;
	int i;

	if(arg < 0.)
		return(2. - erfc(-arg));
/*
	if(arg < 0.5)
		return(1. - erf(arg));
*/
	if(arg >= 10.)
		return(0.);

	for(n=0,d=0,i=N-1; i>=0; i--){
		n = n*arg + p2[i];
		d = d*arg + q2[i];
	}
	return(exp(-arg*arg)*n/d);
}
double erf(double arg){
	int sign;
	double argsq;
	double d, n;
	int i;

	sign = 1;
	if(arg < 0.){
		arg = -arg;
		sign = -1;
	}
	if(arg < 0.5){
		argsq = arg*arg;
		for(n=0,d=0,i=M-1; i>=0; i--){
			n = n*argsq + p1[i];
			d = d*argsq + q1[i];
		}
		return(sign*torp*arg*n/d);
	}
	if(arg >= 10.)
		return(sign*1.);
	return(sign*(1. - erfc(arg)));
}


