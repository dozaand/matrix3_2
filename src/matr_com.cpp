#include <math.h>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <matrix3_2/inc/mtr_err.h>
#include <matrix3_2/inc/matr_com.h>
//#include <memtst1.h>
//#include <stdtempl.h>


template <class TReal>
int matr_comns<TReal>::size()const
{
  return Size;
}

template <class TReal>
 matr_comns<TReal>::matr_comns(int y,int x)
 {
  if(y<=0){y=x;}
  Size=x*y;
  arr=new TReal[Size];
  d_mem(arr);
  Dimo=y;Dim=x;
  memset(arr,0,sizeof(TReal)*Size);
 }

template <class TReal>
matr_comns<TReal>::matr_comns(istream& ist)
{
  int i;
  ist>>Dimo>>Dim;
  Size=Dimo*Dim;
  arr=new TReal[Size];
  d_mem(arr);//no space
  for(i=0;i<Size;i++){ist>>arr[i];}
}


template <class TReal>
 matr_comns<TReal>::matr_comns(int y,int x,TReal *d)
 {
  int i;
//  dbg_tst(x>180||y>180);//size to b
  Size=x*y;
  arr=new TReal[Size];
  d_mem(arr);//no space
  Dimo=y;Dim=x;
  for(i=0;i<Size;i++){arr[i]=d[i];}
 }
template <class TReal>
 matr_comns<TReal>::matr_comns(const matr_comns<TReal>& m)
 {
  memcpy(this,&m,sizeof(matr_comns));
  arr=new TReal[Size];
  memcpy(arr,m.arr,sizeof(TReal)*Dim*Dimo);
 }
template <class TReal>
 matr_comns<TReal>::matr_comns(jack<TReal>& jk)
 {
  Dim=jk.dim();
  Dimo=jk.dimo();
  Size=Dim*Dimo;
  arr=new TReal[Dim*Dimo];
  memset(arr,0,sizeof(TReal)*Size);
  jack_it<TReal> it(&jk);
  for(it=0;it();it++)(*this)[it.getj()][it.geti()]=it.getd();
 }
template <class TReal>
 matr_comns<TReal>::matr_comns(oper_iter<TReal>& it)
 {
  Dim=it.dim();
  Dimo=it.dimo();
  Size=Dim*Dimo;
  arr=new TReal[Size];
  memset(arr,0,sizeof(TReal)*Size);
  for(it=0;it();it++)(*this)[it.getj()][it.geti()]=it.getd();
 }

template <class TReal>
 TReal *matr_comns<TReal>::addmul(const TReal *x,TReal *y)
 {
  int i,j,shift;
  for(i=0,shift=0;i<Dimo;i++,shift+=Dim)
  {
   for(j=0;j<Dim;j++){y[i]+=x[j]*arr[shift+j];}
  }
  return y;
 }
template <class TReal>
 TReal *matr_comns<TReal>::addmultr (const TReal *x,TReal *y)
 {
  int i,j;
  for(i=0;i<Dim;i++)
  {
   for(j=0;j<Dimo;j++){y[i]+=x[j]*arr[j*Dim+i];}
  }
  return y;
 }
template <class TReal>
 TReal* matr_comns<TReal>::mul (const TReal *x,TReal *y)
 {
  if(y==NULL)y=new TReal[Dimo];
  memset(y,0,sizeof(TReal)*Dimo);
  return addmul(x,y);
 }
template <class TReal>
 TReal* matr_comns<TReal>::multr (const TReal *x,TReal *y)
 {
  if(y==NULL)y=new TReal[Dim];
  memset(y,0,sizeof(TReal)*Dim);
  return addmultr(x,y);
 }

template <class TReal>
 void matr_comns<TReal>::transp()
 {// transpose matrix<TReal>
  TReal* arr1;
  arr1=new TReal[Dim*Dimo];
  d_mem(arr1);
  int i,j;
  for(i=0;i<Dimo;i++)
  {
   for(j=0;j<Dim;j++)
   {
    arr1[i+j*Dimo]=arr[j+i*Dim];// it can be done better
   }
  }
  delete [] arr;arr=arr1;
  i=Dim;Dim=Dimo;Dimo=i;//swap done
 }
 //! ������� ������� � ����� ���������
 /*!
 ����������� x^j A^i_j y_i x.A.y
 \param x ����� ������ ������� ������ Dimo
 \param y ������ ������ ������� ������ Dim
 */
template <class TReal>
 TReal matr_comns<TReal>::svrt(TReal* x,TReal* y) const
 {// transpose matrix<TReal>
  int i,j;
  TReal* p,sum,sum1;
  p=arr;
  sum1=0;
  for(j=0;j<Dimo;j++)
  {
   sum=0;
   for(i=0;i<Dim;i++)
   {
    sum+=p[i]*y[i];
   }
   sum1+=sum*x[j];
   p+=Dim;
  }
  return sum1;
 }
/*
template <class TReal>
 matr_com<TReal>& matr_com<TReal>::transp()
 {
  int i,j,k=1,l,nm=Dim*Dim-1,np1=Dim+1,nm1=Dim-1;
  TReal temp;
  for(i=Dim-1;i<=nm;i+=Dim)
  {
   l=k+nm1;
   for(j=k;j<=i;j++)
   {
    temp=arr[j];arr[j]=arr[l];arr[l]=temp;l+=Dim;
   }
   k+=np1;
  }
  return *this;
 }*/
template <class TReal>
 std::ostream& matr_comns<TReal>::pt(std::ostream& s)
 {
  int i,j;
  s<<Dimo<<' '<<Dim<<std::endl;
  for(i=0;i<Dimo;i++)
  {
   for(j=0;j<Dim;j++){s<<std::setw(12)<<arr[i*Dim+j]<<'\t';}
   s<<std::endl;
  }
  s<<std::endl<<std::flush;
  return s;
 }
template <class TReal>
void matr_com<TReal>::kvadr(int times)
{
// replace A with A^2^times
//it can have less memory for mult
 TReal *dat,*tmp;
 int sh=Dim,sh_j,sh_ij,sh_ik,nt,i,j,k;
 dat=new TReal[Size];
 for(nt=0;nt<times;nt++)
 {
  for(j=0,sh_j=0;j<sh;j++,sh_j+=sh)
  {
   for(i=0,sh_ij=sh_j;i<sh;i++,sh_ij++)
   {
    dat[sh_ij]=0;
    for(k=0,sh_ik=i;k<sh;k++,sh_ik+=sh)
    {
     dat[sh_ij]+=arr[sh_ik]*arr[k+sh_j];
    }
   }
  }
  tmp=arr;arr=dat;dat=tmp;//swap memory
 }
 // this is for memory saving
 if(nt%2!=0){for(i=0;i<Size;i++){dat[i]=arr[i];}delete [] arr;arr=dat;}
 else{delete [] dat;}
}

template <class TReal>
void matr_com<TReal>::E_hA(int a)
{
 int i,n=Dim*Dimo;
 TReal *p;
 for(i=0;i<n;i++)
 {
  arr[i]=-a*arr[i];
 }
 for(i=0,p=arr;i<Dim;i++,p+=Dim)
 {
  p[i]+=1.;
 }
}

template <class TReal>
matr_com<TReal>* matr_com<TReal>::matr_exp(TReal dt,TReal eps,matr_com<TReal>* elm)
{
 // mathematical methods in medicine Richard Bellman p 80.
 int nkva,i,j;
 TReal norm=0,d;
 matr_com<TReal> *el;
 if(eps<0){eps=-eps;}
 if(eps==0){eps=TReal(1e-6);}
 if(elm==NULL){el=new matr_com(*this);}
 else{el=elm;}
 // calculate norm of A
 for(j=0;j<Dim*Dim;j+=Dim)
 {
  for(i=0,d=0;i<Dim;i++)
  {
   d+=fabs(arr[i+j]);
  }
  if(norm<d)norm=d;
//  norm=max(norm,d);
 }
 nkva=int(log((norm*dt)/sqrt(eps*2.))/log(TReal(2))); // estimate nesesary number of kvadrations err<eps;err�(norm*dt/2^nk)^2/2;
 d=dt/pow(TReal(2.),(TReal)nkva);//set stap
 for(i=0;i<Dim*Dim;i++)
 {
  el->arr[i]=arr[i]*d;
 }
 for(i=0;i<Dim*Dim;i+=Dim+1)
 {
  el->arr[i]+=1.;
 }
 el->kvadr(nkva);
 return el;
}


template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator+=(const matr_comns<TReal>& ma)
 {
  int i;
  int Dimo=dimo();
  d_bnd(Dim,Dimo,ma.Dim,ma.Dimo);
  for(i=0;i<Dim*Dimo;i++)arr[i]+=ma.arr[i];
  return *this;
 }
template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator-=(const matr_comns<TReal>& ma)
 {
  int i;
  int Dimo=dimo();
  d_bnd(Dim,Dimo,ma.Dim,ma.Dimo);
  for(i=0;i<Dim*Dimo;i++)arr[i]-=ma.arr[i];
  return *this;
 }
template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator*=(TReal d)
 {
  int i;
  for(i=0;i<Dim*Dimo;i++)arr[i]*=d;
  return *this;
 }

template <class TReal>
 void matr_comns<TReal>::set_E()
 {
  memset(arr,0,sizeof(TReal)*Dim*Dimo);
  int i,m;
  if(Dim>Dimo){m=Dimo;}else{m=Dim;}
  for(i=0;i<m;i++){arr[i*Dim+i]=1;}
 }
template <class TReal>
 void matr_comns<TReal>::set_rand()
 {// fill random numbers
  int i;
  for(i=0;i<Dim*Dimo;i++){arr[i]=(TReal)rand();}
 }
template <class TReal>
 void matr_comns<TReal>::set_rand(TReal from,TReal to)
 {// fill random numbers
  int i;
  TReal k=(to-from)/RAND_MAX;
  for(i=0;i<Dim*Dimo;i++){arr[i]=TReal(rand())*k+from;}
 }

template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator*=(const matr_comns<TReal>& ma)
 {// only for sq matrix<TReal>
  TReal sum,*buf;
  int i,j,k;
  d_ne(Dim,Dimo);// implementaton of matrixns<TReal> *= limited for sq result
  d_ne(ma.Dim,ma.Dimo);//
  d_ne(Dim,ma.Dimo);
  buf=new TReal[Dim];d_mem(buf);
  for(i=0;i<Dim;i++)
  {
   for(j=0;j<Dim;j++)
   {
    for(k=0,sum=0.;k<Dim;k++){sum+=arr[i*Dim+k]*(ma.arr)[k*ma.Dim+j];}
    buf[j]=sum;
   }
   for(k=0;k<Dim;k++){arr[i*Dim+k]=buf[k];}
  }
  delete [] buf;
  return *this;
 }
template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::prod(const matr_comns<TReal>& ma1,const matr_comns<TReal>& ma2)
 {
  d_ne(ma1.Dim,ma2.Dimo);
  d_ne(ma1.Dim,ma2.Dimo);//
  if(ma1.Dimo*ma2.Dim>Size)
  {// mem reallocation
   Size=ma1.Dimo*ma2.Dim;
   delete [] arr;
   arr=new TReal[Size];
  }
  Dimo=ma1.Dimo;
  Dim=ma2.Dim;
  TReal sum;
  int i,x,l=ma2.Dimo,p1,p2,p0,s1=Dim*Dimo;
  for(x=0;x<Dim;x++)
  {
   for(p1=0,p0=0;p0<s1;p1+=l,p0+=Dim)
   {
    sum=0;
    for(i=0,p2=0;i<l;i++,p2+=Dim)
    {
//     sum+=ma1[y][i]*ma2[i][x];            //380.0
     sum+=ma1.arr[p1+i]*ma2.arr[x+p2];   //148
    }
    arr[p0+x]=sum;
   }
  }
  return *this;
 }
template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator=(const matr_comns<TReal>& m)
 {
  if(Size<m.Dim*m.Dimo)
  {
   delete [] arr;
   Dim=m.Dim;Dimo=m.Dimo;Size=Dim*Dimo;
   arr=new TReal[Size];d_mem(arr);
  }
  else
  {
   Dim=m.Dim;Dimo=m.Dimo;
  }
  memcpy(arr,m.arr,sizeof(TReal)*Dim*Dimo);
  return *this;
 }
template <class TReal>
 matr_comns<TReal>& matr_comns<TReal>::operator=(TReal d)
 {
  int i;
  for(i=0;i<Dim*Dimo;i++){arr[i]=d;}
  return *this;
 }





template <class TReal>
void matr_com_lu<TReal>::copy(matr_com<TReal>& ma)
{
 int i,j,sh;
 for(i=0,sh=0;i<Dim;i++,sh+=Dim)
 for(j=0;j<Dim;j++)
 {
  lu[j+sh]=ma[i][j];
 }
}

template <class TReal>
void matr_com_lux<TReal>::dcmp(void)
{
int i,j,k;
 for(k=0;k<Dim-1;k++)
 {
  for(i=k+1;i<Dim;i++)
  {
   d_alg(lu[k+k*Dim]==0.,"singularyty in dcmp");
   lu[i+k*Dim]/=lu[k+k*Dim];
   for(j=k+1;j<Dim;j++)
   {
    lu[i+j*Dim]-=lu[i+k*Dim]*lu[k+j*Dim];
   }
  }
 }
}
template <class TReal>
void matr_com_lux<TReal>::bksb(TReal *b)
{
int i,j;
 for(i=0;i<Dim;i++)
 {
  for(j=0;j<i;j++){b[i]-=b[j]*lu[j+Dim*i];}
  b[i]/=lu[i+Dim*i];
 }
 for(i=Dim-1;i>=0;i--)
 {
  for(j=i+1;j<Dim;j++)
  {
   b[i]-=b[j]*lu[j+Dim*i];
  }
 }
}

template <class TReal>
TReal* matr_com_lux<TReal>::mul (const TReal *a,TReal *b)
{
int i,j;
TReal sum;
 for(i=0;i<Dim;i++)
 {
  sum=a[i];
  for(j=0;j<i;j++){sum-=b[j]*lu[j+Dim*i];}
  b[i]=sum/lu[i+Dim*i];
 }
 for(i=Dim-1;i>=0;i--)
 {
  sum=b[i];
  for(j=i+1;j<Dim;j++)
  {
   sum-=b[j]*lu[j+Dim*i];
  }
  b[i]=sum;
 }
 return b;
}


template <class TReal>
void matr_com_luo<TReal>::lubksbp(TReal *b) const
{
 int i,ii,j,ll;
 TReal sum;
 TReal *lubuf=lu;
 ii=-1;
 for(i=0;i<Dim;i++)
 {
   ll=indx[i];
   sum=b[ll];
   b[ll]=b[i];
   if(ii!=-1)
   {
    for(j=ii;j<i;j++){sum-=lubuf[i*Dim+j]*b[j];}
   }
   else if(sum!=0.){ii=i;}
   b[i]=sum;
 }
 for(i=Dim-1;i>=0;i--)
 {
   sum=b[i];
   for(j=i+1;j<Dim;j++){sum-=lubuf[i*Dim+j]*b[j];}
   b[i]=sum/lubuf[i*Dim+i];
 }
}

template <class TReal>
void matr_com_lu<TReal>::pt(std::ostream& so)
{
 so<<dim()<<'\t'<<dimo()<<endl;
 int i,j;
 for(i=0;i<dimo();i++)
 {
   for(j=0;j<dim();j++)
     so<<(*this)[i][j]<<'\t';
   so<<endl;
 }

}

template <class TReal>
TReal *matr_com_luo<TReal>::mul (const TReal *b,TReal *y)
{
int i;
 if(y==NULL){y=new TReal[Dim];}
 for(i=0;i<Dim;i++){y[i]=b[i];}
 lubksbp(y);
 return y;
}

#define TINY 1.0e-20
template <class TReal>
void matr_com_luo<TReal>::dcmp(void)
{
int i,imax,j,k;
TReal aamax,dum,sum,d,*vv;
vv=new TReal[Dim];
 d=1.;
 for(i=0;i<Dim;i++)
 {
  aamax=0.;
  for(j=0;j<Dim;j++)
  {
    TReal tmpmax=fabs(lu[i*Dim+j]);
    if(tmpmax>aamax)aamax=tmpmax;
  }
  d_alg(aamax==0.,"singular matrix");
  vv[i]=TReal(1.)/aamax;
 }
 for(j=0;j<Dim;j++)
 {
  for(i=0;i<j;i++)
  {
   sum=lu[i*Dim+j];
   for(k=0;k<i;k++){sum-=lu[i*Dim+k]*lu[k*Dim+j];}
   lu[i*Dim+j]=sum;
  }
  aamax=0.;
  for(i=j;i<Dim;i++)
  {
   sum=lu[i*Dim+j];
   for(k=0;k<j;k++){sum-=lu[i*Dim+k]*lu[k*Dim+j];}
   lu[i*Dim+j]=sum;
   dum=vv[i]*fabs(sum);
   if(dum>=aamax){imax=i;aamax=dum;}
  }
  if(j!=imax)
  {
   for(k=0;k<Dim;k++)
   {
    dum=lu[imax*Dim+k];
    lu[imax*Dim+k]=lu[j*Dim+k];
    lu[j*Dim+k]=dum;
   }
   d=-d;
   vv[imax]=vv[j];
  }
  indx[j]=imax;
  if(lu[j*Dim+j]==TReal(0.))lu[j*Dim+j]=TReal(TINY);
  if(j!=Dim)
  {
   dum=TReal(1.)/lu[j*Dim+j];
   for(i=j+1;i<Dim;i++){lu[i*Dim+j]=lu[i*Dim+j]*dum;}
  }
 }
delete [] vv;
}
#undef TINY

static int low,igh;
template <class TReal>
void balanc(int n,TReal* a,TReal* scale)
{
 //**********************************************************************;
 TReal radix=16,b2,f,c,r,g,s;
 int noconv,i,j=0,l,k,m=0,iexc=0,jj;
 // ********** radix is a machine dependet parameter specifying;
 // the base of the machine floating point representation.;
 // **********;
 b2=radix*radix;
 k=0;
 l=n;
 goto m100;
 // ********** in-line procedure for row and;
 // column exchange **********;
 m20: scale[m]=j;
 if(j != m)
 {
  for(i=0;i<l;i++)
  {
   f=a[n*(j)+i];
   a[n*(j)+i]=a[n*(m)+i];
   a[n*(m)+i]=f;
  }
  for(i=k;i<n;i++)
  {
   f=a[n*(i)+j];
   a[n*(i)+j]=a[n*(i)+m];
   a[n*(i)+m]=f;
  }
 }
 if(iexc==2) goto m130;
 // ********* search for rows isolating an eigenvalue;
 // and push them down **********;
 if (l == 1) goto m280;
 l=l-1;
 // ********** for j=l step -1 until 1 do -- **********;
 //;
 m100:
 for(jj=1;jj<=l;jj++)
 {
  j=l-jj;
  for(i=0;i<l;i++)
  {
   if (i != j){if(a[n*(i)+j]!=0.e0) goto m120;}
  }
  m=l-1;
  iexc=1;
  goto m20;
  m120: ;
 }
 goto m140;
 // ********** search for columns isolating an eigenvalue;
 // and push them left **********;
 m130: k=k+1;
 m140:
 for(j=k;j<l;j++)
 {
  for(i=k;i<l;i++)
  {
   if (i != j){if(a[n*(j)+i]!=0.e0) goto m170;}
  }
  m=k;
  iexc=2;
  goto m20;
  m170: ;
 }
// ********** now balance the submatrix<TReal> in rows k to l **********;
 for(i=k;i<l;i++){scale[i]=1.e0;}
// ********** iterative loop for norm reduction **********;
 noconv=1;
 while(noconv)
 {
  noconv=0;
  for(i=k;i<l;i++)
  {
   c=0.e0;
   r=0.e0;
   for(j=k;j<l;j++)
   {
    if (j != i)
    {
     c=c+fabs(a[n*(i)+j]);
     r=r+fabs(a[n*(j)+i]);
    }
   }
   g=r/radix;
   f=1.e0;
   s=c+r;
   while(c < g){f=f*radix;c=c*b2;}
   g=r*radix;
   while(c >= g){f=f/radix;c=c/b2;}
   // ********** now balance **********;
   if((c+r)/f < 9.5e-1*s)
   {
    g=1.e0/f;
    scale[i]=scale[i]*f;
    noconv=1;
    for(j=k;j<n;j++){a[n*(j)+i]=a[n*(j)+i]*g;}
    for(j=0;j<l;j++){a[n*(i)+j]=a[n*(i)+j]*f;}
   }
  }
 }
 m280: low=k;
 igh=l-1;
 //===>;
}
template <class TReal>
void elmhes(int n,TReal* a,int* inte)
{
 //**********************************************************************;
 int la,kp1,m,mm1,i,j,mp1;
 TReal x,y;
 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 la=igh-1;
 kp1=low+1;
 if(la<kp1) return;
 for(m=kp1;m<=la;m++)
 {
  mm1=m-1;
  x=0.e0;
  i=m;
  for(j=m;j<=igh;j++)
  {
   if(fabs(a[n*(mm1)+j])<=fabs(x)) goto m100;
   x=a[n*(mm1)+j];
   i=j;
   m100: ;
  }
  inte[m]=i;
  if(i==m) goto m130;
  // ********** interchange rows and columns of a **********;
  for(j=mm1;j<n;j++)
  {
   y=a[n*(j)+i];
   a[n*(j)+i]=a[n*(j)+m];
   a[n*(j)+m]=y;
  }
  for(j=0;j<=igh;j++)
  {
   y=a[n*(i)+j];
   a[n*(i)+j]=a[n*(m)+j];
   a[n*(m)+j]=y;
  }
  // ********** end interchange **********;
  m130: if(x==0.e0) goto m180;
  mp1=m+1;
  for(i=mp1;i<=igh;i++)
  {
   y=a[n*(mm1)+i];
   if(y==0.e0) goto m160;
   y=y/x;
   a[n*(mm1)+i]=y;
   for(j=m;j<n;j++){a[n*(j)+i]-=y*a[n*(j)+m];}
   for(j=0;j<=igh;j++){a[n*(m)+j]+=y*a[n*(i)+j];}
   m160: ;
  }
  m180: ;
 }
 return;
}

template <class TReal>
int hqr(int n,TReal *h,TReal *wr,TReal *wi)
{
 //**********************************************************************;
 int en,enm2,notlas;
 int i,j,k,its,na,ll,l,mm,m,mp2;
 TReal machep=4.50e-16,t,x,y,w,s,zz,r,p,q;
 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 for(i=0;i<n;i++)
 {
  if(i<low||i>igh)
  {
   wr[i]=h[n*(i)+i];
   wi[i]=0.e0;
  }
 }
 en=igh;
 t=0.e0;
 m60: if(en<low)
 {
  return 0;
 }
 its=0;
 na=en-1;
 enm2=na-1;
 m70:
 for(ll=low;ll<=en;ll++)
 {
  l=en+low-ll;
  if(l==low)break;
  if(fabs(h[n*(l-1)+l])<=machep*(fabs(h[n*(l-1)+l-1])
  +fabs(h[n*(l)+l])))break;
 }
 //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 x=h[n*(en)+en];
 if(l==en) goto m270;
 y=h[n*(na)+na];
 w=h[n*(na)+en]*h[n*(en)+na];
 if(l==na) goto m280;
 if(its==30) goto m1000;
 if(its!=10&&its!=20) goto m130;
 t=t+x;
 for(i=low;i<=en;i++){h[n*(i)+i]-=x;}
 s=(fabs(h[n*(na)+en])+fabs(h[n*(enm2)+na]));
 x=7.5e-1*s;
 y=x;
 w=-4.375e-1*s*s;
 m130: its=its+1;
 for(mm=l;mm<=enm2;mm++)
 {
  m=enm2+l-mm;
  zz=h[n*(m)+m];
  r=x-zz;
  s=y-zz;
  p=(r*s-w)/h[n*(m)+m+1]+h[n*(m+1)+m];
  q=h[n*(m+1)+m+1]-zz-r-s;
  r=h[n*(m+1)+m+2];
  s=fabs(p)+fabs(q)+fabs(r);
  p=p/s;
  q=q/s;
  r=r/s;
  if(m==l)break;
  if(fabs(h[n*(m-1)+m])*(fabs(q)+fabs(r))<=machep*fabs(p)
  *(fabs(h[n*(m-1)+m-1])+fabs(zz)+fabs(h[n*(m+1)+m+1])))break;
 }
 mp2=m+2;
 for(i=mp2;i<=en;i++)
 {
  h[n*(i-2)+i]=0.e0;
  if(i!=mp2){h[n*(i-3)+i]=0.e0;}
 }
 for(k=m;k<=na;k++)
 {
  notlas=(k!=na);
  if(k==m) goto m170;
  p=h[n*(k-1)+k];
  q=h[n*(k-1)+k+1];
  r=0.e0;
  if (notlas) r=h[n*(k-1)+k+2];
  x=fabs(p)+fabs(q)+fabs(r);
  if(x==0.e0) goto m260;
  p=p/x;
  q=q/x;
  r=r/x;
  m170: s=(p>=0)?sqrt(p*p+q*q+r*r):-sqrt(p*p+q*q+r*r);
  if(k==m) goto m180;
  h[n*(k-1)+k]=-s*x;
  goto m190;
  m180: if(l!=m) h[n*(k-1)+k]=-h[n*(k-1)+k];
  m190: p=p+s;
  x=p/s;
  y=q/s;
  zz=r/s;
  q=q/p;
  r=r/p;
  for(j=k;j<=en;j++)
  {
   p=h[n*(j)+k]+q*h[n*(j)+k+1];
   if(!notlas) goto m200;
   p=p+r*h[n*(j)+k+2];
   h[n*(j)+k+2]=h[n*(j)+k+2]-p*zz;
   m200: h[n*(j)+k+1]=h[n*(j)+k+1]-p*y;
   h[n*(j)+k]=h[n*(j)+k]-p*x;
  }
  if(en<k+3){j=en;}else{j=k+3;}
//  j=min(en,k+3);
  for(i=l;i<=j;i++)
  {
   p=x*h[n*(k)+i]+y*h[n*(k+1)+i];
   if(!notlas) goto m220;
   p=p+zz*h[n*(k+2)+i];
   h[n*(k+2)+i]=h[n*(k+2)+i]-p*r;
   m220: h[n*(k+1)+i]=h[n*(k+1)+i]-p*q;
   h[n*(k)+i]=h[n*(k)+i]-p;
  }
  m260: ;
 }
 goto m70;
 m270: wr[en]=x+t;
 wi[en]=0.e0;
 en=na;
 goto m60;
 m280: p=(y-x)/2.e0;
 q=p*p+w;
 zz=sqrt(fabs(q));
 x=x+t;
 if(q<0.e0) goto m320;
 zz=p+((p>=0)?(zz):(-zz));
 wr[na]=x+zz;
 wr[en]=wr[na];
 if(zz!=0.e0) wr[en]=x-w/zz;
 wi[na]=0.e0;
 wi[en]=0.e0;
 goto m330;
 m320: wr[na]=x+p;
 wr[en]=x+p;
 wi[na]=zz;
 wi[en]=-zz;
 m330: en=enm2;
 goto m60;
 m1000:
 return en;
 //===>;
}

template <class TReal>
void spectr(matr_com<TReal>& mat,TReal ** re,TReal ** im)
{
 TReal * sp,*realp,*imagp,*arr=mat[0];
 int i=-1,Dim=mat.Dim;
 if(arr!=NULL)
 {
  sp=new TReal[Dim*Dim];
  realp=new TReal[Dim];
  imagp=new TReal[Dim];
  for(i=0;i<Dim*Dim;i++){sp[i]=arr[i];}
  balanc(Dim,sp,imagp);
  elmhes(Dim,sp,(int*)imagp);
  i=hqr(Dim,sp,realp,imagp);
  if(i!=0)
  {
   delete [] realp;
   delete [] imagp;
   delete [] sp;
   d_alg_msg("hqr error in spectr calculations");
  }
  delete [] sp;
  *re=realp;
  *im=imagp;
 }
}
template void spectr<float>(matr_com<float>& mat,float ** re,float ** im);
template void spectr<double>(matr_com<double>& mat,double ** re,double ** im);

// begin of SVD decomposition part

template <class TReal>
 matr_com_svd<TReal>::matr_com_svd(matr_com_svd<TReal>& el)
 {
  int i;
  Size=el.Size;Dim=el.Dim;alloc();
  for(i=0;i<Size*Size;i++){u[i]=el.u[i];}
  for(i=0;i<Size*Size;i++){v[i]=el.v[i];}
  for(i=0;i<Size;i++){w[i]=el.w[i];}
 }
template <class TReal>
 matr_com_svd<TReal>::matr_com_svd(matr_com<TReal>& el)
 {
  int i,j;
  TReal *poi;
  Dim=Size=el.dim();alloc();
  poi=el[0];
  // transpose it
  for(j=0;j<Dim;j++)
  {
   for(i=0;i<Dim;i++){u[j+i*Dim]=poi[i+j*Dim];}
  }
  svdcmp();
 }
template <class TReal>
 void matr_com_svd<TReal>::update(matr_com<TReal>& el)
 {
  int i,j;
  TReal *poi;
  if(Size<el.dim()){free_all();Dim=Size=el.dim();alloc();}
  Dim=el.dim();
  poi=el[0];
  for(j=0;j<Dim;j++)
  {
   for(i=0;i<Dim;i++){u[j+i*Dim]=poi[i+j*Dim];}
  }
  svdcmp();
 }

template <class TReal>
TReal* matr_com_svd<TReal>::addmul (const TReal *b,TReal *x)
{
 int i,j,jj;
 TReal s;
 for(j=0;j<Dim;j++)
 {
  int n_j;
  n_j=Dim*j;
  s=0.;
  if(w[j]!=0.)
  {
   for(i=0;i<Dim;i++)
   {
    s+=u[n_j+i]*b[i];
   }
   s/=w[j];
  }
  rv1[j] =s;
 }
 for(j=0;j<Dim;j++)
 {
  s=0.;
  for(jj=0;jj<Dim;jj++)
  {
   s+=v[Dim*jj+j]*rv1[jj];
  }
  x[j]+=s;
 }
 return x;
}

template <class TReal>
TReal* matr_com_svd<TReal>::addmultr (const TReal *b,TReal *x)
{// not correct
 int i,j,jj;
 TReal s;
 for(j=0;j<Dim;j++)
 {
  int n_j;
  n_j=Dim*j;
  s=0.;
  if(w[j]!=0.)
  {
   for(i=0;i<Dim;i++)
   {
    s+=u[n_j+i]*b[i];
   }
   s/=w[j];
  }
  rv1[j]=s;
 }
 for(j=0;j<Dim;j++)
 {
 s=0.;
  for(jj=0;jj<Dim;jj++)
  {
   s+=v[Dim*jj+j]*rv1[jj];
  }
  x[j]+=s;
 }
 return x;
}
template <class TReal>
 TReal* matr_com_svd<TReal>::mul (const TReal *x,TReal *y)
 {
  if(y==NULL)y=new TReal[Dim];
  for(int i=0;i<Dim;i++){y[i]=0.;}
  svbksb(x,y);
  return y;
 }
template <class TReal>
 TReal* matr_com_svd<TReal>::multr (const TReal *x,TReal *y)
 {
  if(y==NULL)y=new TReal[Dim];
  for(int i=0;i<Dim;i++){y[i]=0.;}
  return addmultr(x,y);
 }

template <class TReal>
 void matr_com_svd<TReal>::pt(std::ostream& s)
 {
  int i,j;
  s<<"svd matrix\n"<<Dim<<std::endl;
  s<<"u=\n";
  for(j=0;j<Dim;j++)
  {
   for(i=0;i<Dim;i++)
   {
    s<<std::setw(12)<<u[i+j*Dim];
   }
   s<<std::endl;
  }
  s<<"w=\n";
  for(i=0;i<Dim;i++)
  {
   s<<std::setw(12)<<w[i];
  }
  s<<"\nv=\n";
  for(j=0;j<Dim;j++)
  {
   for(i=0;i<Dim;i++)
   {
    s<<std::setw(12)<<v[i+j*Dim];
   }
   s<<std::endl;
  }
 }
template <class TReal>
 void matr_com_svd<TReal>::regul(TReal eps)
 {
  TReal ma=w[0];
  int i;
  for(i=1;i<Dim;i++){if(w[i]>ma)ma=w[i];}
  eps*=ma;
  for(i=0;i<Dim;i++){if(w[i]<eps){w[i]=0.;}}
 }

template <class TReal>
static TReal pythag(TReal a,TReal b)
{
 TReal absa, absb, pythag_v;
 absa = fabs( a );
 absb = fabs( b );
 if( absa > absb )
 {
  pythag_v = absa*sqrt( 1. + absb*absb/(absa*absa) );
 }
 else
 {
  if( absb == 0. )
  {
   pythag_v = 0.;
  }
  else
  {
   pythag_v = absb*sqrt( 1. + absa*absa/(absb*absb) );
  }
 }
 return  pythag_v;
}

template <class TReal>
void matr_com_svd<TReal>::svbksb(const TReal * b,TReal *x) const
{
 int i,j,jj;
 TReal s;
 for(j=0;j<Dim;j++)
 {
  int n_j;
  n_j=Dim*j;
  s=0.;
  if(w[j]!=0.)
  {
   for(i=0;i<Dim;i++)
   {
    s+=u[n_j+i]*b[i];
   }
   s/=w[j];
  }
  rv1[j]=s;
 }
 for(j=0;j<Dim;j++)
 {
  s=0.;
  for(jj=0;jj<Dim;jj++)
  {
   s+=v[Dim*jj+j]*rv1[jj];
  }
  x[j]=s;
 }
}

template <class TReal>
inline TReal sign(TReal a,TReal b){if(b>=0){return fabs(a);}else{return -fabs(a);}}

template <class TReal>
void matr_com_svd<TReal>::svdcmp()
{
 int i,its,j,jj,k,l,nm,m_i,m_j;
 TReal anorm,c,f,g,h,s,scale,x,y,z;
 g=0.0;
 scale=0.0;
 anorm=0.0;
 for(i=0;i<Dim;i++)
 {
  l=i+1;
  m_i=Dim*i;
  rv1[i]=scale*g;
  g=0.0;
  s=0.0;
  scale=0.0;
  if(i<Dim)
  {
   for(k=i;k<Dim;k++)
   {
    scale=scale+fabs(u[m_i+k]);
   }
   if(scale!=0.0)
   {
    for(k=i;k<Dim;k++)
    {
     u[m_i+k]=u[m_i+k]/scale;
     s+=u[m_i+k]*u[m_i+k];
    }
    f=u[m_i+i];
    g=-sign((TReal)sqrt(s),f);
    h=f*g-s;
    u[m_i+i]=f-g;
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     m_j=Dim*j;
     for(k=i;k<Dim;k++)
     {
      s+=u[m_i+k]*u[m_j+k];
     }
     f=s/h;
     for(k=i;k<Dim;k++)
     {
      u[m_j+k]+=f*u[m_i+k];
     }
    }
    for(k=i;k<Dim;k++)
    {
     u[m_i+k]=scale*u[m_i+k];
    }
   }
  }
  w[i] =scale*g;
  g=0.0;
  s=0.0;
  scale=0.0;
  if((i<Dim)&&(i!=Dim-1))
  {
   int m_k;
   for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
   {
    scale+=fabs(u[m_k]);
   }
   if(scale!=0.0)
   {
    for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
    {
     u[m_k]/=scale;
     s+=u[m_k]*u[m_k];
    }
    f=u[Dim*l+i];
    g=-sign((TReal)sqrt(s),f);
    h=f*g-s;
    u[Dim*l+i]=f-g;
    for(k=l,m_k=Dim*l;m_k<Dim*Dim;k++,m_k+=Dim)
    {
     rv1[k]=u[m_k+i]/h;
    }
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     for(m_k=Dim*l;m_k<Dim*Dim;m_k+=Dim)
     {
      s+=u[m_k+j]*u[m_k+i];
     }
     for(k=l,m_k=Dim*l;m_k<Dim*Dim;k++,m_k+=Dim)
     {
      u[m_k+j]+=s*rv1[k];
     }
    }
    for(m_k=Dim*l+i;m_k<Dim*Dim+i;m_k+=Dim)
    {
     u[m_k]*=scale;
    }
   }
  }
  TReal mmem;
  mmem=fabs(w[i])+fabs(rv1[i]);
  if(anorm<mmem)anorm=mmem;
 }

 for(i=Dim-1;i>=0;i--)
 {
  if(i<Dim-1)
  {
   if(g!=0.0)
   {
    for(j=l;j<Dim;j++)
    {
     v[Dim*i+j]=(u[Dim*j+i]/u[Dim*l+i])/g;
    }
    for(j=l;j<Dim;j++)
    {
     s=0.0;
     for(k=l;k<Dim;k++)
     {
      s+=u[Dim*k+i]*v[Dim*j+k];
     }
     for(k=l;k<Dim;k++)
     {
      v[Dim*j+k]+=s*v[Dim*i+k];
     }
    }
   }
   for(j=l;j<Dim;j++)
   {
    v[Dim*j+i]=0.0;
    v[Dim*i+j]=0.0;
   }
  }
  v[Dim*i+i]=1.0;
  g=rv1[i];
  l=i;
 }
 for(i=Dim-1;i>=0;i--)
 {
  l=i+1;
  g=w[i];
  for(j=l;j<Dim;j++)
  {
   u[Dim*j+i]=0.0;
  }
  if(g!=0.0)
  {
   g=TReal(1.0)/g;
   for(j=l;j<Dim;j++)
   {
    s=0.0;
    for(k=l;k<Dim;k++)
    {
     s+=u[Dim*i+k]*u[Dim*j+k];
    }
    f=(s/u[Dim*i+i])*g;
    for(k=i;k<Dim;k++)
    {
     u[Dim*j+k]=u[Dim*j+k]+f*u[Dim*i+k];
    }
   }
   for(j=i;j<Dim;j++)
   {
    u[Dim*i+j]=u[Dim*i+j]*g;
   }
  }
  else
  {
   for(j=i;j<Dim;j++)
   {
    u[Dim*i+j]=0.0;
   }
  }
  u[Dim*i+i]=u[Dim*i+i]+TReal(1.0);
 }
 for(k=Dim-1;k>=0;k--)
 {
  for(its=1;its<=30;its++)
  {
   for(l=k;l>=0;l--)
   {
    nm=l-1;
    if((fabs(rv1[l])+anorm)==anorm)goto L_2;
    if((fabs(w[nm])+anorm)==anorm)goto L_1;
   }
   L_1:
   c=0.0;
   s=1.0;
   for(i=l;i<=k;i++)
   {
    f=s*rv1[i];
    rv1[i]=c*rv1[i];
    if((fabs(f)+anorm)==anorm)
    goto L_2;
    g=w[i];
    h=pythag(f,g);
    w[i]=h;
    h=TReal(1.0)/h;
    c=g*h;
    s=-(f*h);
    for(j=0;j<Dim;j++)
    {
     y=u[Dim*nm+j];
     z=u[Dim*i+j];
     u[Dim*nm+j]=(y*c)+(z*s);
     u[Dim*i+j]=-(y*s)+(z*c);
    }
   }
   L_2:
   z=w[k];
   if(l==k)
   {
    if(z<0.0)
    {
     w[k]=-z;
     for(j=0;j<Dim;j++)
     {
      v[Dim*k+j]=-v[Dim*k+j];
     }
    }
    goto L_3;
   }
   d_alg(its==30,"no convergence in svdcmp number of iterations ==30");
   x=w[l];
   nm=k-1;
   y=w[nm];
   g=rv1[nm];
   h=rv1[k];
   f=((y-z)*(y+z)+(g-h)*(g+h))/(TReal(2.0)*h*y);
   g=pythag(f,(TReal)1.0);
   f=((x-z)*(x+z)+h*((y/(f+sign(g,f)))-h))/x;
   c=1.0;
   s=1.0;
   for(j=l;j<=nm;j++)
   {
    i=j+1;
    g=rv1[i];
    y=w[i];
    h=s*g;
    g=c*g;
    z=pythag(f,h);
    rv1[j]=z;
    c=f/z;
    s=h/z;
    f=(x*c)+(g*s);
    g=-(x*s)+(g*c);
    h=y*s;
    y=y*c;
    for(jj=0;jj<Dim;jj++)
    {
     x=v[Dim*j+jj];
     z=v[Dim*i+jj];
     v[Dim*j+jj]=(x*c)+(z*s);
     v[Dim*i+jj]=-(x*s)+(z*c);
    }
    z=pythag(f,h);
    w[j]=z;
    if(z!=0.0)
    {
     z=TReal(1.0)/z;
     c=f*z;
     s=h*z;
    }
    f=(c*g)+(s*y);
    x=-(s*g)+(c*y);
    for(jj=0;jj<Dim;jj++)
    {
     y=u[Dim*j+jj];
     z=u[Dim*i+jj];
     u[Dim*j+jj]=(y*c)+(z*s);
     u[Dim*i+jj]=-(y*s)+(z*c);
    }
   }
   rv1[l]=0.0;
   rv1[k]=f;
   w[k]=x;
  }
  L_3:;
 }
}
template <class TReal>
int invert(matr_com_lu<TReal>& lu,matr_com<TReal>& m)
{// do inplace calculations of m^-1
 int i,j,n=m.dim();
 TReal * x,*y;
 if(m.dim()==1){m[0][0]=TReal(1.)/m[0][0];return 0;}
 x=new TReal[n];
 y=new TReal[n];
 lu.update(m);
 memset(x,0,sizeof(TReal)*n);
 for(i=0;i<n;i++)
 {
  x[i]=1;
  lu.mul(x,y);
  for(j=0;j<n;j++)
  {
   m[j][i]=y[j];
  }
  x[i]=0;
 }
 delete [] x;
 delete [] y;
 return 0;
}
template int invert<float>(matr_com_lu<float>& lu,matr_com<float>& m);
template int invert<double>(matr_com_lu<double>& lu,matr_com<double>& m);

template <class TReal>
matr_comns<TReal>& matr_comns<TReal>::AB(matr_comns<TReal>& B,TReal * buf)
{// A=A.B
 int i,j,r_row,r_col;
 d_ne(Dim,B.Dimo);//"inproper size in A*B multiplication"
 d_lt(Size,Dimo*B.Dim);//" insuffisient storage in target matrix"
 TReal sum;
 TReal *row1; // pointer to current row in first matrix<TReal>
 TReal *row1out; // pointer to current row in out matrix<TReal>
 TReal *col; // pointer to current column begin  in B matrix<TReal>
 if(Dim>=B.Dim)
 {// multiplication from the head
  row1=row1out=arr;
  for(r_row=0;r_row<Dimo;r_row++,row1+=Dim,row1out+=B.Dim)
  {
   for(r_col=0,col=B.arr;r_col<B.Dim;r_col++)
   {
    for(i=0,j=0,sum=0;i<Dim;i++,j+=B.Dim)
    {
     sum+=row1[i]*col[j];
    }
    buf[r_col]=sum;
    col++;
   }
   memcpy(row1out,buf,B.Dim*sizeof(TReal));
  }
 }
 else
 {// multiplication from the tail
  row1=arr+Dim*(Dimo-1);
  row1out=arr+B.Dim*(Dimo-1);
  for(r_row=Dimo-1;r_row>=0;r_row--,row1-=Dim,row1out-=B.Dim)
  {
   for(r_col=0,col=B.arr;r_col<B.Dim;r_col++)
   {
    for(i=0,j=0,sum=0;i<Dim;i++,j+=B.Dim)
    {
     sum+=row1[i]*col[j];
    }
    buf[r_col]=sum;
    col++;
   }
   memcpy(row1out,buf,B.Dim*sizeof(TReal));
  }
 }
 Dim=B.Dim;
 return *this;
}

template <class TReal>
matr_comns<TReal>& matr_comns<TReal>::ABT(matr_comns<TReal>& B,TReal * buf)
{// A=A.B
 int i,r_row,r_col;
 d_ne(Dim,B.Dim);//"inproper size in A*B multiplication"
 d_lt(Size,Dimo*B.Dimo);//" insuffisient storage in target matrix"
 TReal sum;
 TReal *row1; // pointer to current row in first matrix<TReal>
 TReal *row1out; // pointer to current row in out matrix<TReal>
 TReal *col; // pointer to current column begin  in B matrix<TReal>
 if(Dim>=B.Dimo)
 {// multiplication from the head
  row1=row1out=arr;
  for(r_row=0;r_row<Dimo;r_row++,row1+=Dim,row1out+=B.Dimo)
  {
   for(r_col=0,col=B.arr;r_col<B.Dimo;r_col++)
   {
    for(i=0,sum=0;i<Dim;i++)
    {
     sum+=row1[i]*col[i];
    }
    buf[r_col]=sum;
    col+=B.Dim;
   }
   memcpy(row1out,buf,B.Dim*sizeof(TReal));
  }
 }
 else
 {// multiplication from the tail
  row1=arr+Dim*(Dimo-1);
  row1out=arr+B.Dimo*(Dimo-1);
//  for(r_row=Dimo-1;r_row>=0;r_row--,row1-=Dim,row1out-=B.Dimo)
  for(r_row=Dimo-1;;)
  {
   for(r_col=0,col=B.arr;r_col<B.Dimo;r_col++)
   {
    for(i=0,sum=0;i<Dim;i++)
    {
     sum+=row1[i]*col[i];
    }
    buf[r_col]=sum;
    col+=B.Dim;
   }
   memcpy(row1out,buf,B.Dimo*sizeof(TReal));
   r_row--;
   if(r_row<0)break;
   row1-=Dim;row1out-=B.Dimo;
  }
 }
 Dim=B.Dimo;
 return *this;
}
//template <class TReal>
//matr_comns<TReal>& matr_comns<TReal>::BA(matr_comns<TReal>& B,TReal * buf){return *this;}
//template <class TReal>
//matr_comns<TReal>& matr_comns<TReal>::BTA(matr_comns<TReal>& B,TReal * buf){return *this;}

template <class TReal>
TReal matr_comns<TReal>::norm() const
{
 TReal nrm=0;
 for(int i=0;i<Dim*Dimo;i++){nrm+=arr[i]*arr[i];}
 return nrm/Dim*Dimo;
}

template <class TReal>
TReal *matr_comns<TReal>::residual(const TReal * x,TReal * y)
{
 int i,j,shift;
 for(i=0,shift=0;i<Dimo;i++,shift+=Dim)
 {
  for(j=0;j<Dim;j++){y[i]-=x[j]*arr[shift+j];}
  y[i]=-y[i];//Ax-b
 }
 return y;
}
template <class T> T mabs(T x){return x>=0?x:-x;}
template <class T> int mabsv(T x,T delt){T ax=mabs(x);return (ax+delt)==ax;}
template <class TReal>
int eigensystem<TReal>::jacobi(int n,TReal * an)
{
    /* System generated locals */
    long int a_dim1, a_offset, v_dim1, v_offset, i__1, i__2, i__3;
    TReal r__1;
//    TReal r__3,r__2,r__4;

    /* Local variables */
    static TReal c__, g, h__;
    static long int i__, j;
    static TReal s, t,  theta, tresh;
    static long int ip, iq;
    static TReal sm, tau;
    memcpy(a,an,n*n*sizeof(TReal));

    /* Parameter adjustments */
    v_dim1 = n;
    v_offset = 1 + v_dim1 * 1;
    v -= v_offset;
    --d;
    a_dim1 = n;
    a_offset = 1 + a_dim1 * 1;
    a -= a_offset;

    /* Function Body */
    for (ip = 1; ip <= n; ++ip) {
	for (iq = 1; iq <= n; ++iq) {
	    v[ip + iq * v_dim1] = (TReal)0.;
	}
	v[ip + ip * v_dim1] = (TReal)1.;
    }
    for (ip = 1; ip <= n; ++ip) {
	b[ip - 1] = a[ip + ip * a_dim1];
	d[ip] = b[ip - 1];
	z__[ip - 1] = (TReal)0.;
/* L13: */
    }
    nrot = 0;
    for (i__ = 1; i__ <= 50; ++i__) {
	sm = (TReal)0.;
	i__1 = n - 1;
	for (ip = 1; ip <= i__1; ++ip) {
	    i__2 = n;
	    for (iq = ip + 1; iq <= i__2; ++iq) {
		sm += mabs(a[ip + iq * a_dim1]);
/* L14: */
	    }
/* L15: */
	}
	if (sm == (TReal)0.) {bac(n);
	    return 0;
	}
	if (i__ < 4) {
/* Computing 2nd power */
	    i__1 = n;
	    tresh = sm * (TReal).2 / (i__1 * i__1);
	} else {
	    tresh = (TReal)0.;
	}
	i__1 = n - 1;
	for (ip = 1; ip <= i__1; ++ip) {
	    i__2 = n;
	    for (iq = ip + 1; iq <= i__2; ++iq) {
		g = mabs(a[ip + iq * a_dim1])*(TReal)100.;
		if (i__>4 && mabsv(d[ip],g) && mabsv(d[iq],g))
                {
	          a[ip + iq * a_dim1] = (TReal)0.;
                }
                else if (mabs(a[ip + iq * a_dim1])> tresh)
                {
	         h__ = d[iq] - d[ip];
 	         if(mabsv(h__,g))
                 {
	          t = a[ip + iq * a_dim1] / h__;
	         }
                 else
                 {
		  theta = h__ * (TReal).5 / a[ip + iq * a_dim1];
		  r__1 = theta;
		  t = (TReal)1. / (mabs(theta) + sqrt(r__1 * r__1 + TReal(1.)));
		  if (theta < (TReal)0.){t = -t;}
	         }
/* Computing 2nd power */
		    r__1 = t;
		    c__ = (TReal)1. / sqrt(r__1 * r__1 + 1);
		    s = t * c__;
		    tau = s / (c__ + (TReal)1.);
		    h__ = t * a[ip + iq * a_dim1];
		    z__[ip - 1] -= h__;
		    z__[iq - 1] += h__;
		    d[ip] -= h__;
		    d[iq] += h__;
		    a[ip + iq * a_dim1] = (TReal)0.;
		    i__3 = ip - 1;
		    for (j = 1; j <= i__3; ++j) {
			g = a[j + ip * a_dim1];
			h__ = a[j + iq * a_dim1];
			a[j + ip * a_dim1] = g - s * (h__ + g * tau);
			a[j + iq * a_dim1] = h__ + s * (g - h__ * tau);
/* L16: */
		    }
		    i__3 = iq - 1;
		    for (j = ip + 1; j <= i__3; ++j) {
			g = a[ip + j * a_dim1];
			h__ = a[j + iq * a_dim1];
			a[ip + j * a_dim1] = g - s * (h__ + g * tau);
			a[j + iq * a_dim1] = h__ + s * (g - h__ * tau);
/* L17: */
		    }
		    i__3 = n;
		    for (j = iq + 1; j <= i__3; ++j) {
			g = a[ip + j * a_dim1];
			h__ = a[iq + j * a_dim1];
			a[ip + j * a_dim1] = g - s * (h__ + g * tau);
			a[iq + j * a_dim1] = h__ + s * (g - h__ * tau);
/* L18: */
		    }
		    i__3 = n;
		    for (j = 1; j <= i__3; ++j) {
			g = v[j + ip * v_dim1];
			h__ = v[j + iq * v_dim1];
			v[j + ip * v_dim1] = g - s * (h__ + g * tau);
			v[j + iq * v_dim1] = h__ + s * (g - h__ * tau);
/* L19: */
		    }
		    ++(nrot);
		}
/* L21: */
	    }
/* L22: */
	}
	i__1 = n;
	for (ip = 1; ip <= i__1; ++ip) {
	    b[ip - 1] += z__[ip - 1];
	    d[ip] = b[ip - 1];
	    z__[ip - 1] = (TReal)0.;
/* L23: */
	}
/* L24: */
    }
    bac(n);
    return 0;
} /* jacobi_ */

template <class TReal>
TReal matr_com_lu<TReal>::svrt(TReal* x,TReal* y) const
 {// transpose matrix<TReal>
  int i,j;
  TReal* p,sum,sum1;
  p=lu;
  sum1=0;
  for(j=0;j<Dim;j++)
  {
   sum=0;
   for(i=0;i<Dim;i++)
   {
    sum+=p[i]*y[i];
   }
   sum1+=sum*x[j];
   p+=Dim;
  }
  return sum1;
 }

template class matr_comns_it<float>;
template class matr_comns_it<double>;
template class matr_comns<double>;
template class matr_comns<float>;
template class matr_com<float>;
template class matr_com<double>;
template class matr_com_lu<float>;
template class matr_com_lu<double>;
template class matr_com_luo<float>;
template class matr_com_luo<double>;
template class matr_com_svd<float>;
template class matr_com_svd<double>;
template class matr_com_lux<float>;
template class matr_com_lux<double>;
template class eigensystem<float>;
template class eigensystem<double>;
