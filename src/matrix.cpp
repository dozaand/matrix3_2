#include <iomanip>
#include <string.h>
#include <matrix3_2/inc/matrix.h>
//template <class TReal>
//template double oper_iter<double>::tmpval;// if operator do not allow write data
//template float oper_iter<float>::tmpval;// if operator do not allow write data
template class oper_iter<double>;
template class oper_iter<float>;
template class matrix<double>;
template class matrix<float>;
template class matrixns<double>;
template class matrixns<float>;
template class jackd<double>;
template class jackd<float>;
template class jack<double>;
template class jack<float>;
template <class TReal>
TReal* oper_iter<TReal>::mul (const TReal* x,TReal* out)
{
 memset(out,0,sizeof(TReal)*dimo());
 for(operator=(0);operator()();operator++(0))
 {
  out[getj()]+=x[geti()]*getd();
 }
 return out;
}
template <class TReal>
TReal* oper_iter<TReal>::multr (const TReal* x,TReal* out)
{
 memset(out,0,sizeof(TReal)*dim());
 for(operator=(0);operator()();operator++(0)){out[geti()]+=x[getj()]*getd();}
 return out;
}
template <class TReal>
TReal* oper_iter<TReal>::addmul (const TReal* x,TReal* out)
{
 for(operator=(0);operator()();operator++(0)){out[getj()]+=x[geti()]*getd();}
 return out;
}
template <class TReal>
TReal* oper_iter<TReal>::addmultr (const TReal* x,TReal* out)
{
 for(operator=(0);operator()();operator++(0)){out[geti()]+=x[getj()]*getd();}
 return out;
}

template <class TReal>
void oper_iter<TReal>::mk_count()
{
 for(count=0,(*this)=0;(*this)();(*this)++,count++);
}
template <class TReal>
void oper_iter<TReal>::testran()
{
 int Dim=dim(),Dimo=dimo();
 for((*this)=0;(*this)();(*this)++)
 {
  d_range(0,geti(),Dim);
  d_range(0,getj(),Dimo);
 }
}


template <class TReal>
int defjak(operat<TReal>& op,TReal *x0,TReal * ma,int length,TReal deviat)
{
 int i,j,sh,len=op.dim()*op.dimo();
 TReal ed=(TReal)1./deviat,edd=(TReal)1./(deviat-1),*out,*out1;
 memset(ma,0,sizeof(TReal)*length);
 if(length<len) return -1;
// allocate temporary array's
 out=new TReal[op.dimo()];d_mem(out);
 out1=new TReal[op.dim()];d_mem(out1);
 op.mul(x0,out);
 for(i=0;i<op.dim();i++)
 {
  x0[i]*=deviat;
  op.mul(x0,out1);
  for(sh=i,j=0;j<op.dimo();j++,sh+=op.dim())ma[sh]=(out1[j]-out[j])*edd;
  x0[i]*=ed;
 }
 return 0;
}

template <class TReal>
TReal* jack<TReal>::mul (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[ii[i]];
  if(d!=0)out[jj[i]]+=d;
 }
 return out;
}
template <class TReal>
TReal* jack<TReal>::multr (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[jj[i]];
  if(d!=0)out[ii[i]]+=d;
 }
 return out;
}
template <class TReal>
TReal* jack<TReal>::addmul (const TReal* x,TReal* out)
{
 TReal d;
 for(int i=0;i<ntot;i++)
 {
  d=x[ii[i]];
  if(d!=0)out[jj[i]]+=d;
 }
 return out;
}
template <class TReal>
TReal* jack<TReal>::addmultr (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[jj[i]];
  if(d!=0)out[ii[i]]+=d;
 }
 return out;
}

using namespace std;

template <class TReal>
 void jack<TReal>::rd(istream& s)
 {
  int ia,ja,id,k;
  TReal d;
  s>>id;
  if(id>max_ntot)
  {
   delete [] ii;delete [] jj;
   ii=new int[id];d_mem(ii);
   jj=new int[id];d_mem(jj);
  }
  flush();
  for(k=0;k<id;k++)
  {
   s>>ja>>ia>>d;
   d_eof(s);
   add(ja,ia);
  }
 }
template <class TReal>
 void jack<TReal>::pt(ostream& s)
 {
  s<<ntot<<endl;
  for(int k=0;k<ntot;k++)s<<jj[k]<<" "<<ii[k]<<" "<<k<<endl;
 }
template <class TReal>
 void jack<TReal>::correct()
 {
  int si,sj,k;
  si=ii[0];sj=jj[0];
  for(k=1;k<ntot;k++)
  {
   if(si>ii[k])si=ii[k];
   if(sj>jj[k])sj=jj[k];
  }
  for(k=0;k<ntot;k++)
  {
   ii[k]-=si;
   jj[k]-=sj;
  }
  dimx-=si;
  dimy-=sj;
  Dim=0;
  if(Dim<dimx){Dim=dimx;}
  if(Dim<dimy){Dim=dimy;}
 }

template <class TReal>
 void jackd<TReal>::rd(istream& s)
 {
  int ia,ja,id,k;
  TReal d;
  s>>id;
  if(id>max_ntot)
  {
   delete [] ii;delete [] jj;delete [] dat;
   ii=new int[id];d_mem(ii);
   jj=new int[id];d_mem(jj);
   dat=new TReal[id];d_mem(dat);
  }
  flush();
  for(k=0;k<id;k++)
  {
   s>>ja>>ia>>d;
   d_eof(s);
   add(ja,ia,d);
  }
 }
template <class TReal>
 void jackd<TReal>::copy(const jackd<TReal>& el)
 {
  if(max_ntot<el.ntot)
  {
   delete [] ii;delete [] jj;delete [] dat;
   ntot=max_ntot=el.ntot;
   ii=new int[ntot];
   jj=new int[ntot];
   dat=new TReal[ntot];
  }
  ntot=el.ntot;
  memcpy(ii,el.ii,sizeof(int)*ntot);
  memcpy(jj,el.jj,sizeof(int)*ntot);
  memcpy(dat,el.dat,sizeof(TReal)*ntot);
  Dim=el.Dim;
  Dimo=el.Dimo;
  dimx=el.dimx;
  dimy=el.dimy;
  pos=el.pos;
 }

template <class TReal>
 void jackd<TReal>::pt(ostream& s)
 {
  s<<ntot<<endl;
  for(int k=0;k<ntot;k++)s<<jj[k]<<" "<<ii[k]<<" "<<dat[k]<<endl;
 }
template <class TReal>
 void jackd<TReal>::operator*=(TReal d)
 {
  for(int i=0;i<ntot;i++)
  {
   dat[i]*=d;
  }
 }
template <class TReal>
 void jackd<TReal>::operator+=(TReal d)
 {
  for(int i=0;i<ntot;i++)
  {
   dat[i]+=d;
  }
 }
template <class TReal>
 void jackd<TReal>::shdiag(TReal d)
 {
  for(int i=0;i<ntot;i++)
  {
   if(ii[i]==jj[i])dat[i]+=d;
  }
 }

template <class TReal>
TReal* jackd<TReal>::mul (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[ii[i]];
  if(d!=0)out[jj[i]]+=d*dat[i];
 }
 return out;
}

template <class TReal>
TReal* jackd<TReal>::multr (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[jj[i]];
  if(d!=0)out[ii[i]]+=d*dat[i];
 }
 return out;
}
template <class TReal>
TReal* jackd<TReal>::addmul (const TReal* x,TReal* out)
{
 TReal d;
 for(int i=0;i<ntot;i++)
 {
  d=x[ii[i]];
  if(d!=0)out[jj[i]]+=d*dat[i];
 }
 return out;
}
template <class TReal>
TReal* jackd<TReal>::addmultr (const TReal* x,TReal* out)
{
 TReal d;
 memset(out,0,sizeof(TReal)*Dim);
 for(int i=0;i<ntot;i++)
 {
  d=x[jj[i]];
  if(d!=0)out[ii[i]]+=d*dat[i];
 }
 return out;
}

template <class TReal>
void pt_matr(operat<TReal>& ma,int ib,int jb,ostream& s,int di,int dj)
{
 int ydim=ma.dimo(),xdim=ma.dim(),i,j;
 TReal *y,*x;
 y=new TReal[ydim];x=new TReal[xdim];
 if(ib<0){ib=0;}if(ib+di>xdim){di=xdim-ib;}
 if(jb<0){jb=0;}if(jb+dj>ydim){dj=ydim-jb;}
 for(i=0;i<ydim;i++){y[i]=0;}
 s<<"matr "<<setw(5)<<ib<<setw(5)<<jb<<setw(5)<<(ib+di)<<setw(5)<<(ib+dj)<<endl;
 for(j=jb;j<jb+dj;j++)
 {
  y[j]=(TReal)1.;
  ma.multr(y,x);
  for(i=ib;i<(ib+di);i++){s<<setw(10)<<x[i]<<" ";}s<<endl;
  y[j]=(TReal)0.;
 }
 delete [] x;delete [] y;
}
template void pt_matr<double>(operat<double>& ma,int ib,int jb,ostream& s,int di,int dj);
template void pt_matr<float>(operat<float>& ma,int ib,int jb,ostream& s,int di,int dj);

template <class TReal>
void pt_matr_t(operat<TReal>& ma,int ib,int jb,ostream& s,int di,int dj)
{
 int ydim=ma.dim(),xdim=ma.dimo(),i,j;
 TReal *y,*x;
 y=new TReal[ydim];x=new TReal[xdim];
 if(ib<0){ib=0;}if(ib+di>xdim){di=xdim-ib;}
 if(jb<0){jb=0;}if(jb+dj>ydim){dj=ydim-jb;}
 for(i=0;i<ydim;i++){y[i]=0;}
 s<<"matr_tr "<<setw(5)<<ib<<setw(5)<<jb<<setw(5)<<(ib+di)<<setw(5)<<(ib+dj)<<endl;
 for(j=jb;j<jb+dj;j++)
 {
  y[j]=(TReal)1.;
  ma.mul(y,x);
  for(i=ib;i<(ib+di);i++){s<<setw(15)<<x[i]<<" ";}s<<endl;
  y[j]=(TReal)0.;
 }
 delete [] x;delete [] y;
}
template void pt_matr_t<double>(operat<double>& ma,int ib,int jb,ostream& s,int di,int dj);
template void pt_matr_t<float>(operat<float>& ma,int ib,int jb,ostream& s,int di,int dj);

template <class TReal>
void make_jack(jackd<TReal>* jk,void (*fun)(TReal *a,TReal* e),TReal *b,int dim,TReal eps,TReal tol)
{
 int i,j;
 jk->flush();
 TReal *x0,*x1,*x2,d,err,tmp,tmp1;
 const TReal minreal=TReal(1e-20);
 x0=new TReal[dim];d_mem(x0);
 x1=new TReal[dim];d_mem(x1);
 x2=new TReal[dim];d_mem(x2);
 fun(b,x0);
 for(i=0;i<dim;i++)
 {
  tmp=b[i];
  d=tmp*eps;if(d<minreal&&d>(-minreal)){d=minreal;}
  b[i]-=d;
  fun(b,x1);
  b[i]=tmp+d;
  fun(b,x2);
  b[i]=tmp;
  for(j=0;j<dim;j++)
  {
   tmp=x0[j]-x1[j];
   tmp1=x2[j]-x0[j];
   err=(tmp1-tmp)/(TReal)2.;
   tmp=(tmp+tmp1)/(TReal)2.;
   if(tmp>err||tmp<(-err))
   {
    tmp/=d;
    if(tmp!=0&&(tmp>=tol||tmp<=-tol))
    {
     jk->add(j,i,tmp);
    }
   }
  }
 }
}
template void make_jack(jackd<double>* jk,void (*fun)(double *a,double* e),double *b,int dim,double eps,double tol);
template void make_jack(jackd<float>* jk,void (*fun)(float *a,float* e),float *b,int dim,float eps,float tol);

template <class TReal>
void make_jack(jackd<TReal>* jk,operat<TReal>* ma,TReal *b,TReal eps,TReal tol)
{// analise operator
 int i,j,dim=ma->dim(),dimo=ma->dimo();
 jk->flush();
 TReal *x0,*x1,*x2,d,err,tmp,tmp1;
 const TReal minreal=(TReal)1.e-20;
 x0=new TReal[dimo];d_mem(x0);
 x1=new TReal[dimo];d_mem(x1);
 x2=new TReal[dimo];d_mem(x2);
 ma->mul(b,x0);
 for(i=0;i<dim;i++)
 {
  tmp=b[i];
  d=tmp*eps;if(d<minreal&&d>(-minreal)){d=minreal;}
  b[i]-=d;
  ma->mul(b,x1);
  b[i]=tmp+d;
  ma->mul(b,x2);
  b[i]=tmp;
  for(j=0;j<dimo;j++)
  {
   tmp=x0[j]-x1[j];
   tmp1=x2[j]-x0[j];
   err=(tmp1-tmp)/(TReal)2.;
   tmp=(tmp+tmp1)/(TReal)2.;
   if(tmp>err||tmp<(-err))
   {
    tmp/=d;
    if(tmp!=0&&(tmp>=tol||tmp<=-tol))
    {
     jk->add(j,i,tmp);
    }
   }
  }
 }
 delete [] x0;
 delete [] x1;
 delete [] x2;
}
template void make_jack(jackd<double>* jk,operat<double>* ma,double *b,double eps,double tol);
template void make_jack(jackd<float>* jk,operat<float>* ma,float *b,float eps,float tol);

template <class TReal>
void make_jack(jackd<TReal>* jk,operat<TReal>* ma,TReal *b,TReal *eps,TReal tol)
{// analise operator
 int i,j,dim=ma->dim(),dimo=ma->dimo();
 jk->flush();
 TReal *x0,*x1,*x2,d,err,tmp,tmp1;
 const TReal minreal=(TReal)1e-20;
 x0=new TReal[dimo];d_mem(x0);
 x1=new TReal[dimo];d_mem(x0);
 x2=new TReal[dimo];d_mem(x0);
 ma->mul(b,x0);
 for(i=0;i<dim;i++)
 {
  tmp=b[i];
  d=eps[i];if(d<minreal&&d>(-minreal)){d=minreal;}
  b[i]-=d;
  ma->mul(b,x1);
  b[i]=tmp+d;
  ma->mul(b,x2);
  b[i]=tmp;
  for(j=0;j<dimo;j++)
  {
   tmp=x0[j]-x1[j];
   tmp1=x2[j]-x0[j];
   err=(tmp1-tmp)/(TReal)2.;
   tmp=(tmp+tmp1)/(TReal)2.;
   if(tmp>err||tmp<(-err))
   {
    tmp/=d;
    if(tmp!=0&&(tmp>=tol||tmp<=-tol))
    {
     jk->add(j,i,tmp);
    }
   }
  }
 }
 delete [] x0;
 delete [] x1;
 delete [] x2;
}
template void make_jack(jackd<double>* jk,operat<double>* ma,double *b,double *eps,double tol);
template void make_jack(jackd<float>* jk,operat<float>* ma,float *b,float *eps,float tol);


template <class TReal>
TReal test_mul_lu(operat<TReal>& a,operat<TReal>& alu)
{// return norm of  alu.a.e
 TReal * x,*y,d=0;
 int dim=a.dim(),dimo=alu.dimo(),i;
 if(a.dim()!=alu.dim())return -1;
 x=new TReal[dim];
 y=new TReal[dimo];
 for(i=0;i<dim;i++)
 {
  x[i]=(TReal)1.;
 }
 a.mul(x,y);
 alu.mul(y,x);
 for(i=0;i<dim;i++){x[i]-=(TReal)1.;}
 for(i=0;i<dim;i++){d+=x[i]*x[i];}
 delete [] x;delete [] y;
 return d;
}
template double test_mul_lu(operat<double>& a,operat<double>& alu);
template float test_mul_lu(operat<float>& a,operat<float>& alu);

